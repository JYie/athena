package edu.asu.sefcom.athena.examples;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.InvalidCompositionException;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphFactory;
import edu.asu.sefcom.athena.graph.element.Label;


import java.util.Map;
import java.util.Properties;

/**
 * Created by Jangwon Yie on 10/14/2015.
 * @author Jangwon Yie
 */
public class ExamplesFactory implements GraphFactory {
    private Map<Label, Graph> factory;
    private static final int defaultLower = 5;
    private static final int defaultUpper = 1000;

    private int lower;
    private int upper;

    public ExamplesFactory(){
        this(defaultLower, defaultUpper);
    }

    public ExamplesFactory(int lower, int upper){
        this.lower = lower;
        this.upper = upper;
        this.factory = Maps.newHashMap();
    }


    public ExamplesFactory(Properties properties){

        String lowerString = properties.getProperty("lower");
        String upperString = properties.getProperty("upper");
        int lower_ = new Integer(lowerString).intValue();
        int upper_ = new Integer(upperString).intValue();
        if(lower_ > 1 && upper_ > 1){
            this.lower =lower_;
            this.upper = upper_;
        }else {
            this.lower = defaultLower;
            this.upper = defaultUpper;
        }
        this.factory = Maps.newHashMap();
    }

    public Graph createGraph(Label label){
        Graph graph = factory.get(label);
        if(null != graph)
            return graph;

        Graph numberSystemGraph = null;

        try {
            numberSystemGraph = new NumberSystemGraph(label, null, this.lower, this.upper);
            factory.put(label, numberSystemGraph);
            return numberSystemGraph;
        } catch (InvalidCompositionException e) {
            assert(false);
            return null;
        } catch (InvalidParamException e) {
            assert(false);
            return null;
        }
    }
}
