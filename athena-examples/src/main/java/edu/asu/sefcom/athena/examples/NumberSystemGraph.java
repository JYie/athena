package edu.asu.sefcom.athena.examples;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.ReadOnlyGraph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import edu.asu.sefcom.athena.graph.similarity.BasicSimRank;
import edu.asu.sefcom.athena.graph.similarity.SimRank;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jangwon Yie on 10/14/2015.
 *
 * @author Jangwon Yie
 */
public class NumberSystemGraph extends ReadOnlyGraph {

    private Graph actual;

    private int lower;
    private int upper;

    private Map<Integer, Node> map;

    public NumberSystemGraph(Label label, Graph graph, int lower, int upper) throws InvalidCompositionException, InvalidParamException {
        if (lower < 1 || upper < 1)
            throw new InvalidParamException("delivered numbers must be greater than or equal to 1");

        if (null != graph) {
            if (graph instanceof ReadOnlyGraph)
                throw new InvalidCompositionException("Graph instance must not be readonly");
            this.actual = graph;
            init(lower, upper);
            return;
        }

        if (null == label)
            this.actual = new BlueGraph(new Label(this.getClass().getName()));
        else
            this.actual = new BlueGraph(label);

        init(lower, upper);
    }

    private void init(int lower, int upper) {
        this.lower = lower;
        this.upper = upper;
        this.map = Maps.newHashMap();
    }

    @Override
    public Graph populate() {
        pushNodes();
        pushEdges();
        return this.actual;
    }

    private void pushNodes() {
        for (int i = this.lower; i <= this.upper; i++) {
            Map<String, String> number = Maps.newHashMap();
            number.put("name", new Integer(i).toString());

            Node node = null;
            try {
                node = this.actual.addNode(number);
            } catch (UnsupportedOperationException e) {
                assert (false);
            } catch (InvalidParamException ipe) {
                assert (false);
            } catch (StorageException e) {
                assert (false);
            }
            this.map.put(new Integer(i), node);
        }
    }

    private void pushEdges() {
        for (int i = this.lower; i <= this.upper; i++) {
            int multies = getMultiNum(i);

            Node inNode = this.map.get(new Integer(i));
            for (int j = 2; j <= multies; j++) {
                int k = i * j;
                Node outNode = this.map.get(new Integer(k));
                try {
                    this.actual.addDirectedEdge(inNode.getUid(), outNode.getUid(), null);
                } catch (UnavailabeEdgeException e) {
                    assert (false);
                } catch (UnsupportedOperationException e) {
                    assert (false);
                } catch (InvalidParamException ipe) {
                    assert (false);
                } catch (StorageException e) {
                    assert (false);
                }
            }
        }
        this.map = null;
    }

    private int getMultiNum(int num) {
        return this.upper / num;
    }

    public static void main(String[] args) {
        try {
            Graph graph = new NumberSystemGraph(null, null, 5, 5000);
            int stepNum = 8;
            SimRank simRank = new BasicSimRank(stepNum);
            Iterator<Node> ten = graph.getNodes("name", new Integer(400).toString());
            Iterator<Node> fifteen = graph.getNodes("name", new Integer(1200).toString());

            if (null == ten || null == fifteen)
                return;

            Node node1 = ten.next();
            Node node2 = fifteen.next();

            System.out.println(simRank.measureSimilarity(graph, node1.getUid(), node2.getUid()));
        } catch (InvalidParamException e) {
            e.printStackTrace();
        } catch (InvalidCompositionException e) {
            e.printStackTrace();
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    @Override
    public GraphType getGraphType() {
        return this.actual.getGraphType();
    }
}