package edu.asu.sefcom.athena.examples;

import edu.asu.sefcom.athena.graph.element.Label;

/**
 * Created by Jangwon Yie on 10/14/2015.
 * @author Jangwon Yie
 */
public class NumberSystemLabel extends Label {

    private int lower;
    private int upper;

    public NumberSystemLabel(String label, int lower, int upper) {
        super(label);
        this.lower = lower;
        this.upper = upper;
    }

    public int getLower(){
        return this.lower;
    }

    public int getUpper(){
        return this.upper;
    }


}
