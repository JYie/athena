package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/24/15.
 * @author Jangwon Yie
 */
public interface Graph {

    /**
     * It returns the label of this graph.
     * @return
     */
    public Label getLabel();

    /**
     * It registers a node which contains properties delivered.
     * @param properties
     * @return
     */
    public Node addNode(Map<String, String> properties) throws InvalidParamException,
            edu.asu.sefcom.athena.UnsupportedOperationException, StorageException;

    /**
     * It removes the node which contains the id delivered.
     * @param id
     */
    public void removeNode(String id) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException;

    /**
     * It registers an directed edge which consists of nodes having id delivered.
     * @param inNode
     * @param outNode
     * @param properties
     * @return
     */
    public Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException;

    /**
     * It registers an undirected edge which consists of nodes having id delivered.
     * @param node1
     * @param node2
     * @param properties
     * @return
     */
    public Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException;

    /**
     * It registers an hyper edge which consists of nodes having id delivered.
     * @param nodes
     * @param properties
     * @return
     * @throws edu.asu.sefcom.athena.UnsupportedOperationException
     */
    public Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException, edu.asu.sefcom.athena.UnsupportedOperationException;

    /**
     * It removes the edge which contains the id delivered.
     * @param id
     */
    public void removeEdge(String id) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException;;

    /**
     * It returns the edge which contains the id delivered.
     * @param id
     * @return
     */
    public Edge getEdge(String id) throws StorageException;

    /**
     * It returns all nodes which contains property delivered. If parameters are null, it should return all nodes
     * in the graph.
     * @param key
     * @param value
     * @return
     */
    public Iterator<Node> getNodes(String key, String value) throws StorageException;

    /**
     * It returns the node which contains the id delivered.
     * @param id
     * @return
     */
    public Node getNode(String id) throws StorageException;

    /**
     * It returns all edges which contains property delivered. If parameters are null, it should return all edges
     * in the graph.
     * @param key
     * @param value
     * @return
     */
    public Iterator<Edge> getEdges(String key, String value) throws StorageException;

    /**
     * It returns the neighbor(meets direction) nodes of the node which contains the id delivered.
     * It reflects the current update.
     * @param nodeId
     * @param direction
     * @param key
     * @param value
     * @return
     */
    public Iterator<Node> getNeighbors(String nodeId, Direction direction, String key, String value) throws StorageException;

    /**
     * It returns the edge which contains the node containing the id delivered and meets the property(key/value)
     * delivered. It reflects the current update.
     * @param nodeId
     * @param direction
     * @param key
     * @param value
     * @return
     */
    public Iterator<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value) throws StorageException;

    /**
     * It returns the type of this graph instance.
     * @return
     */
    public GraphType getGraphType();
}
