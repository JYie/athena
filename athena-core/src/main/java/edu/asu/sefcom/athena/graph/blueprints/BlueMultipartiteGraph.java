package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.MultiPartiteGraph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * It supports the multipartite graph as you fix the partition of each node when you add the node. You can add property
 * which has the key BlueMultipartiteGraph.PARTITION and the value you want for the partition name to indicate the
 * partition that contains the node.
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class BlueMultipartiteGraph implements MultiPartiteGraph{


    private Map<String, Set<String>> partitionNodesInfo;
    private Map<String, String> nodePartitionsInfo;
    private Graph graph;

    public BlueMultipartiteGraph(Graph graph){
        this.graph = graph;
        this.partitionNodesInfo = Maps.newHashMap();
        this.nodePartitionsInfo = Maps.newHashMap();
    }


    @Override
    public Label getLabel() {
        return this.graph.getLabel();
    }

    @Override
    public Node addNode(Map<String, String> properties) throws edu.asu.sefcom.athena.UnsupportedOperationException, InvalidParamException, StorageException {
        String partition = properties.get(PARTITION);
        if(null == partition)
            throw new UnsupportedOperationException("Partition Information is required");

        Node node = this.graph.addNode(properties);
        Set<String> current = this.partitionNodesInfo.get(partition);
        if(null == current)
            current = Sets.newHashSet();
        current.add(node.getUid());
        this.partitionNodesInfo.put(partition, current);
        this.nodePartitionsInfo.put(node.getUid(), partition);

        return node;
    }

    private boolean checkCrossPartition(String node1, String node2){
        String partition1 = this.nodePartitionsInfo.get(node1);
        String partition2 = this.nodePartitionsInfo.get(node2);

        if(null == partition1 || null == partition2)
            return false;

        if(partition1.compareToIgnoreCase(partition2) == 0)
            return false;
        return true;
    }

    @Override
    public void removeNode(String id) throws UnsupportedOperationException, StorageException {
        this.nodePartitionsInfo.remove(id);
        this.graph.removeNode(id);
    }

    @Override
    public Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties)
            throws UnavailabeEdgeException, UnsupportedOperationException, InvalidParamException, StorageException {
        if(!checkCrossPartition(inNode, outNode))
            throw new UnavailabeEdgeException("Nodes in every edge must be different partitions");
        return this.graph.addDirectedEdge(inNode, outNode, properties);
    }

    @Override
    public Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties)
            throws UnavailabeEdgeException, UnsupportedOperationException, InvalidParamException, StorageException {
        if(!checkCrossPartition(node1, node2))
            throw new UnavailabeEdgeException("Nodes in every edge must be different partitions");
        return this.graph.addUndirectedEdge(node1, node2, properties);
    }

    @Override
    public Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties)
            throws UnavailabeEdgeException, UnsupportedOperationException, InvalidParamException {
        throw new UnsupportedOperationException("Hypergraph multipartite graph is not defined");
    }

    @Override
    public void removeEdge(String id) throws UnsupportedOperationException, StorageException {
        this.graph.removeEdge(id);
    }

    @Override
    public Edge getEdge(String id) throws StorageException {
        return this.graph.getEdge(id);
    }

    @Override
    public Iterator<Node> getNodes(String key, String value) throws StorageException {
        return this.graph.getNodes(key, value);
    }

    @Override
    public Node getNode(String id) throws StorageException {
        return this.graph.getNode(id);
    }

    @Override
    public Iterator<Edge> getEdges(String key, String value) throws StorageException {
        return this.graph.getEdges(key, value);
    }

    @Override
    public Iterator<Node> getNeighbors(String nodeId, Direction direction, String key, String value) throws StorageException {
        return this.graph.getNeighbors(nodeId, direction, key, value);
    }

    @Override
    public Iterator<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value) throws StorageException {
        return this.graph.getIncidentEdges(nodeId, direction, key, value);
    }

    @Override
    public GraphType getGraphType() {
        return this.graph.getGraphType();
    }

    @Override
    public Set<String> getPartitionKeys() {
        return this.partitionNodesInfo.keySet();
    }

    @Override
    public Set<String> getNodesInPartition(String partitionKey) {
        return this.partitionNodesInfo.get(partitionKey);
    }

    @Override
    public String getPartition(String node) {
        return this.nodePartitionsInfo.get(node);
    }
}
