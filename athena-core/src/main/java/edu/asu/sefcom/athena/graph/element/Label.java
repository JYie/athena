package edu.asu.sefcom.athena.graph.element;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public class Label {

    private String label;

    public Label(String label){
        this.label = label;
    }

    public String getLabel(){
        return this.label;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Label label1 = (Label) o;

        return !(label != null ? !label.equals(label1.label) : label1.label != null);

    }

    @Override
    public int hashCode() {
        return label != null ? label.hashCode() : 0;
    }
}
