package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.graph.element.Element;

import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/25/2015.
 * @author Jangwon Yie
 */
public class BlueElement implements Element {
    
    protected String id;
    protected Map<String, String> properties;
    
    public BlueElement(String id){
        this.id = id;
    }
    
    @Override
    public String getUid() {
        return this.id;
    }

    @Override
    public Object getProperty(String key) {
        if(null == properties)
            return null;
        return properties.get(key);
    }

    @Override
    public Set<String> getPropertyKeys() {
        if(null == properties)
            return null;
        return properties.keySet();
    }

    @Override
    public void removeProperty(String key) {
        if(null != properties)
            return;
        properties.remove(key);
    }

    @Override
    public void setProperty(String key, String value) {
        if(null == properties)
            this.properties = Maps.newHashMap();
        this.properties.put(key,value);
    }

    @Override
    public String toString() {
        return "BlueElement{" +
                "id='" + id + '\'' +
                ", properties=" + properties +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlueElement that = (BlueElement) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


}
