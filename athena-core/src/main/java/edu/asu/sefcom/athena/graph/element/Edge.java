package edu.asu.sefcom.athena.graph.element;

import java.util.Set;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public interface Edge extends Element {

    /**
     * It returns its type.
     * @return
     */
    public EdgeType getType();

    /**
     * It returns the set of nodes which meet the direction delivered.
     * @param direction
     * @return
     */
    public Set<Node> getNodes(Direction direction);
}
