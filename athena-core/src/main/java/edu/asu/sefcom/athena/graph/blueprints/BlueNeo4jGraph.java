package edu.asu.sefcom.athena.graph.blueprints;

import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import edu.asu.sefcom.athena.graph.element.Label;

/**
 * Created by Jangwon Yie on 2015. 11. 3..
 * @author Jangwon Yie
 */
public class BlueNeo4jGraph extends BlueBaseGraph{

    private static String defaultStorageHome = "tmp/Athena/Neo4j/";
    private String location;

    public BlueNeo4jGraph(String baseLocation, Label label){
        if(null == baseLocation)
            generateLocation(defaultStorageHome, label);
        else
            generateLocation(baseLocation, label);
        this.label = label;
    }

    public BlueNeo4jGraph(Label label){
        this(defaultStorageHome, label);
    }

    private void generateLocation(String baseLocation, Label label){
        StringBuffer sb = new StringBuffer(baseLocation);
        sb.append(this.label.getLabel());
        this.location = sb.toString();
    }

    @Override
    public Graph generateBlueInstance() {
        return new Neo4jGraph(this.location);
    }

    @Override
    public void clearUp() {
        if(null != this.blueGraph)
            this.blueGraph.shutdown();
    }
}
