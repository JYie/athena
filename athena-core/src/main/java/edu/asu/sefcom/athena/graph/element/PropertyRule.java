package edu.asu.sefcom.athena.graph.element;

import java.util.Map;

/**
 * Created by Jangwon Yie on 10/29/2015.
 * @author Jangwon Yie
 */
public interface PropertyRule {

    /**
     * This implementation defines the policy that the property has to follow. For example, weight must not be negative
     * rule can be defined through thr implementation of this interface.
     * @return it returns reason why it is not admitted otherwise it returns null(which means admitted)
     */
    public String isAdmitted(Map<String, String> properties);
}
