package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 9/29/15.
 * @author Jangwon Yie
 */
public class UnavailabeEdgeException extends GraphException {
    public UnavailabeEdgeException() {
        super();
    }

    public UnavailabeEdgeException(String message) {
        super(message);
    }

    public UnavailabeEdgeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnavailabeEdgeException(Throwable cause) {
        super(cause);
    }

    protected UnavailabeEdgeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
