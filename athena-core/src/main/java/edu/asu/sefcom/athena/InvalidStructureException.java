package edu.asu.sefcom.athena;

/**
 * Created by Jangwon Yie on 9/11/2015.
 * @author Jangwon Yie
 */
public class InvalidStructureException extends AthenaException {
    public InvalidStructureException() {
        super();
    }

    public InvalidStructureException(String message) {
        super(message);
    }

    public InvalidStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidStructureException(Throwable cause) {
        super(cause);
    }

    protected InvalidStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
