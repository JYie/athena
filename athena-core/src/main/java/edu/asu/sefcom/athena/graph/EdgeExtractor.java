package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.StorageException;

/**
 * Created by Jangwon Yie on 10/7/2015.
 * @author Jangwon Yie
 */
public interface EdgeExtractor {

    public String extractUndirectedEdgeId(String... nodes) throws StorageException;

    public String extractDirectedEdgeId(String head, String tail) throws StorageException;

}
