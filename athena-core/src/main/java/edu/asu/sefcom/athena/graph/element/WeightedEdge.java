package edu.asu.sefcom.athena.graph.element;

/**
 * Created by Jangwon Yie on 11/11/2015.
 * @author Jangwon Yie
 */
public interface WeightedEdge extends Edge{
    public double getWeight();
}
