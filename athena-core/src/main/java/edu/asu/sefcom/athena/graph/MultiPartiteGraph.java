package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Set;

/**
 * It expresses the multi-partite graph. Especially, if the size of getPartitionKeys() is 2 then it means bipartite graph.
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public interface MultiPartiteGraph extends Graph {

    public static final String PARTITION = "Partition";

    public Set<String> getPartitionKeys();

    public Set<String> getNodesInPartition(String partitionKey);

    /**
     * It returns the key of the partition which contains the node having delivered uid. If the graph does not contain
     * the node having delivered id, then it returns -1.
     * @param node
     * @return
     */
    public String getPartition(String node);
}
