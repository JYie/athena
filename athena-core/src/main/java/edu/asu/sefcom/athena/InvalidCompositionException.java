package edu.asu.sefcom.athena;

/**
 * Created by Jangwon Yie on 10/14/2015.
 * @author Jangwon Yie
 */
public class InvalidCompositionException extends AthenaException {

    public InvalidCompositionException() {
        super();
    }

    public InvalidCompositionException(String message) {
        super(message);
    }

    public InvalidCompositionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCompositionException(Throwable cause) {
        super(cause);
    }

    protected InvalidCompositionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

