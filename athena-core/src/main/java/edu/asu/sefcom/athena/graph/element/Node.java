package edu.asu.sefcom.athena.graph.element;

import java.util.Iterator;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public interface Node extends Element{

    /**
     * It returns the edge which is incident to this node and contains the label delivered.
     * @return
     */
    public Iterator<Edge> getIncidentEdges(Direction direction);

    /**
     * It returns the node which is adjacent to this node and meets the direction delivered.
     * @return
     */
    public Iterator<Node> getAdjacentNodes(Direction direction);

}
