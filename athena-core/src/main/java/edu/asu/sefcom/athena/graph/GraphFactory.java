package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.graph.element.Label;

/**
 * Created by Jangwon Yie on 9/24/15.
 * @author Jangwon Yie
 */
public interface GraphFactory {

    /**
     * It returns instance of graph which is distinguished by the label delivered.
     * @param label
     * @return
     */
    public Graph createGraph(Label label);
}
