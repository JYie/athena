package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Arrays;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/25/2015.
 * @author Jangwon Yie
 */
public class BlueEdge implements Edge {

    private BlueElement element;
    private EdgeType type;
    private Node[] nodes;

    public BlueEdge(String id, EdgeType type, Node... nodes) throws InvalidParamException{
        this.element = new BlueElement(id);
        this.type = type;
        checkNodes(nodes);
        storeNodes(nodes);
    }

    public BlueEdge(String id, Node... nodes) throws  InvalidParamException{
        this(id, EdgeType.SimpleUndirected, nodes);
    }

    private void checkNodes(Node... nodes)throws InvalidParamException{
        if(null == nodes || nodes.length < 2)
            throw new InvalidParamException("number of edges must be greater than 1.");
        if(!type.equals(EdgeType.Hyper) && nodes.length != 2)
            throw new InvalidParamException("number of edges must be greater than 1.");
    }

    private void storeNodes(Node... nodes){
        this.nodes = new Node[nodes.length];
        for(int i=0;i<nodes.length;i++)
            this.nodes[i] = nodes[i];
    }

    @Override
    public EdgeType getType() {
        return this.type;
    }

    @Override
    public Set<Node> getNodes(Direction direction) {
        switch(direction){
            case HYPERANY:
            case SIMPLEBOTH:
            case SIMPLEUNDIRECT:
                return Sets.newHashSet(this.nodes);
            case SIMPLEIN:
                return Sets.newHashSet(this.nodes[0]);
            case SIMPLEOUT:
                return Sets.newHashSet(this.nodes[1]);
            default:
                return null;
        }
    }

    @Override
    public String getUid() {
        return this.element.getUid();
    }

    @Override
    public Object getProperty(String key) {
        return this.element.getProperty(key);
    }

    @Override
    public Set<String> getPropertyKeys() {
        return this.element.getPropertyKeys();
    }

    @Override
    public void removeProperty(String key) {
        this.element.removeProperty(key);
    }

    @Override
    public void setProperty(String key, String value) {
        this.element.setProperty(key,value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlueEdge that = (BlueEdge) o;

        if (!element.equals(that.element)) return false;
        if (type != that.type) return false;

        switch(type){
            case Hyper:
            case SimpleUndirected:
                return Sets.difference(Sets.newHashSet(nodes), Sets.newHashSet(that.nodes)).isEmpty();
            case SimpleDirected:
                return nodes[0].equals(that.nodes[0]) && nodes[1].equals(that.nodes[1]);
            default:
                return false;
        }
    }

    @Override
    public int hashCode() {
        int result = element.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + Arrays.hashCode(nodes);
        return result;
    }

    @Override
    public String toString() {
        return "BlueEdge{" +
                "element=" + element +
                ", type=" + type +
                ", nodes=" + Arrays.toString(nodes) +
                '}';
    }
}
