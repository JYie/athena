package edu.asu.sefcom.athena.graph.blueprints;

import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.PropertyRule;

/**
 * Created by Jangwon Yie on 2015. 11. 3..
 *
 * @author Jangwon Yie
 */
public class BlueGraph extends BlueBaseGraph {

    public BlueGraph(Label label, PropertyRule rule) {
        this.label = label;
        this.rule = rule;
    }

    public BlueGraph(Label label) {
        this(label, null);
    }

    @Override
    public Graph generateBlueInstance() {
        return new TinkerGraph();
    }

    @Override
    public void clearUp() {

    }

}
