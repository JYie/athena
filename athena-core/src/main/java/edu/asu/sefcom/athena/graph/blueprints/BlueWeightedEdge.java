package edu.asu.sefcom.athena.graph.blueprints;

import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.Property;
import edu.asu.sefcom.athena.graph.element.WeightedEdge;

/**
 * Created by Jangwon Yie on 11/11/2015.
 * @author Jangwon Yie
 */
public class BlueWeightedEdge extends BlueEdge implements WeightedEdge {
    public BlueWeightedEdge(String id, EdgeType type, Node... nodes) throws InvalidParamException {
        super(id, type, nodes);
    }

    public BlueWeightedEdge(String id, Node... nodes) throws InvalidParamException {
        super(id, nodes);
    }

    @Override
    public double getWeight() {
        String weight = (String)getProperty(Property.WEIGHT.name());
        if(null == weight)
            return 1.0;
        try{
            Double weightValue = new Double(weight);
            return weightValue.doubleValue();
        }catch(NumberFormatException nfe){
            return 1.0;
        }
    }
}
