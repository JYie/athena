package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 11/11/2015.
 * @author Jangwon Yie
 */
public enum GraphType {
    UNDIRECTED, DIRECTED, HYPER, MIXED
}
