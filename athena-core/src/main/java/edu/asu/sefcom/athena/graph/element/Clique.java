package edu.asu.sefcom.athena.graph.element;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import org.apache.commons.math3.util.CombinatoricsUtils;

import java.util.Set;

/**
 * Created by Jangwon Yie on 9/16/15.
 * @author Jangwon Yie
 */
public class Clique {
    private final int hypernumber;
    private Set<Node> nodes;

    public Clique(int hypernumber) throws InvalidParamException{
        if(hypernumber < 2)
            throw new InvalidParamException("hypernumber must be at least 2");
        this.hypernumber = hypernumber;
    }

    public Clique() throws InvalidParamException{
        this(2);
    }

    public int getHypernumber(){
        return this.hypernumber;
    }

    public int getOrder(){
        if(null == nodes)
            return 0;
        else
            return nodes.size();
    }

    public long getSize(){
        if(null == nodes)
            return 0;

        int order = nodes.size();
        return CombinatoricsUtils.binomialCoefficient(order, this.hypernumber);
    }

    public void addNode(Node node){
        if(null == this.nodes)
            this.nodes = Sets.newHashSet();

        this.nodes.add(node);
    }

    public void removeNode(Node node){
        if(null == this.nodes || nodes.size() == 0)
            return;
        nodes.remove(node);
    }

    public Set<Node> getNodes(){
        return Sets.newHashSet(this.nodes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Clique clique = (Clique) o;

        if (hypernumber != clique.hypernumber) return false;
        return !(nodes != null ? !nodes.equals(clique.nodes) : clique.nodes != null);

    }

    @Override
    public int hashCode() {
        int result = hypernumber;
        result = 31 * result + (nodes != null ? nodes.hashCode() : 0);
        return result;
    }
}
