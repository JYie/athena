package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/14/2015.
 *
 * @author Jangwon Yie
 */
public abstract class ReadOnlyGraph implements Graph {

    protected Graph actual;

    @Override
    public Node addNode(Map<String, String> properties) throws edu.asu.sefcom.athena.UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public void removeNode(String id) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties) throws UnavailabeEdgeException, UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties) throws UnavailabeEdgeException, UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties) throws UnavailabeEdgeException, UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public void removeEdge(String id) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("ReadOnlyGraph does not support this api");
    }

    @Override
    public Label getLabel() {
        if (null == this.actual)
            this.actual = populate();

        return this.actual.getLabel();
    }

    @Override
    public Edge getEdge(String id) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getEdge(id);
    }

    @Override
    public Iterator<Node> getNodes(String key, String value) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getNodes(key, value);
    }

    @Override
    public Node getNode(String id) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getNode(id);
    }

    @Override
    public Iterator<Edge> getEdges(String key, String value) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getEdges(key, value);
    }

    @Override
    public Iterator<Node> getNeighbors(String nodeId, Direction direction, String key, String value) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getNeighbors(nodeId, direction, key, value);
    }

    @Override
    public Iterator<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value) throws StorageException {
        if (null == this.actual)
            this.actual = populate();
        return this.actual.getIncidentEdges(nodeId, direction, key, value);
    }

    public abstract Graph populate();
}
