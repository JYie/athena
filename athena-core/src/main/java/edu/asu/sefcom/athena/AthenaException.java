package edu.asu.sefcom.athena;

/**
 * Created by Jangwon Yie on 9/29/15.
 * @author Jangwon Yie
 */
public class AthenaException extends Exception {
    public AthenaException() {
        super();
    }

    public AthenaException(String message) {
        super(message);
    }

    public AthenaException(String message, Throwable cause) {
        super(message, cause);
    }

    public AthenaException(Throwable cause) {
        super(cause);
    }

    protected AthenaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
