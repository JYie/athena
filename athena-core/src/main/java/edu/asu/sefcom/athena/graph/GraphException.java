package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.AthenaException;

/**
 * Created by Jangwon Yie on 9/29/15.
 * @author Jangwon Yie
 */
public class GraphException extends AthenaException {
    public GraphException() {
        super();
    }

    public GraphException(String message) {
        super(message);
    }

    public GraphException(String message, Throwable cause) {
        super(message, cause);
    }

    public GraphException(Throwable cause) {
        super(cause);
    }

    protected GraphException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
