package edu.asu.sefcom.athena.graph.element;

/**
 * Created by Jangwon Yie on 9/24/15.
 */
public enum Direction {
    SIMPLEIN, SIMPLEOUT, SIMPLEBOTH, SIMPLEUNDIRECT, HYPERANY
}
