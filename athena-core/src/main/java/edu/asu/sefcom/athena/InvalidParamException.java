package edu.asu.sefcom.athena;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public class InvalidParamException extends AthenaException{

    public InvalidParamException() {
        super();
    }

    public InvalidParamException(String message) {
        super(message);
    }

    public InvalidParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidParamException(Throwable cause) {
        super(cause);
    }

    protected InvalidParamException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
