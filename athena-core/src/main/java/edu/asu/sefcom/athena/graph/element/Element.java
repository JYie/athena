package edu.asu.sefcom.athena.graph.element;

import java.util.Set;

/**
 * Created by Jangwon Yie on 9/24/15.
 * @author Jangwon Yie
 */
public interface Element {

    /**
     * It returns the unique identifier of the instance which implements element.
     * @return
     */
    public String getUid();

    /**
     * It returns the object value associated with the key delivered.
     * @param key
     * @return
     */
    public Object getProperty(String key);

    /**
     * It returns the set of keys.
     * @return
     */
    public Set<String> getPropertyKeys();

    /**
     * It removes the property key of which is key delivered.
     * @param key
     */
    public void removeProperty(String key);

    /**
     * It sets the key/value pair.
     * @param key
     * @param value
     */
    public void setProperty(String key, String value);
}
