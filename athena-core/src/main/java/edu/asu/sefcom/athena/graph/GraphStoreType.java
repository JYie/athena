package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 9/24/15.
 * @author Jangwon Yie
 */
public enum GraphStoreType {
    Memory, Neo4JEmbed, Neo4j, Rexster
}
