package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphFactory;
import edu.asu.sefcom.athena.graph.element.Label;

import java.util.Map;

/**
 * It only generates a blue graph for its graph instance.
 * Created by Jangwon Yie on 10/7/2015.
 * @author Jangwon Yie
 */
public class BlueFactory implements GraphFactory {
    private Map<Label, Graph> factory;

    public BlueFactory(){
        this.factory = Maps.newHashMap();
    }

    public Graph createGraph(Label label){
        Graph graph = factory.get(label);
        if(null != graph)
            return graph;

        Graph blueGraph = new BlueGraph(label);
        factory.put(label,blueGraph);
        return blueGraph;
    }
}
