package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/25/2015.
 * @author Jangwon Yie
 */
public class BlueNode implements Node {

    private BlueElement element;
    private Set<Edge> inEdge;
    private Set<Edge> outEdge;
    private Set<Edge> undirectedEdge;
    private Set<Edge> hyperEdge;

    private Set<Node> outAdjNodes;
    private Set<Node> inAdjNodes;
    private Set<Node> adjNodes;
    private Set<Set<Node>> adjHyperNodes;

    public BlueNode(String id){
        this.element = new BlueElement(id);
    }

    void putIncidentEdge(Edge edge){
        EdgeType type = edge.getType();
        switch (type) {
            case SimpleUndirected:
                if (null == this.undirectedEdge)
                    this.undirectedEdge = Sets.newHashSet();
                this.undirectedEdge.add(edge);
                break;
            case SimpleDirected:
                Set<Node> inNode = edge.getNodes(Direction.SIMPLEIN);
                if(null != inNode && inNode.contains(this)){
                    if(null == this.outEdge)
                        this.outEdge = Sets.newHashSet();
                    this.outEdge.add(edge);
                }

                Set<Node> outNode = edge.getNodes(Direction.SIMPLEOUT);
                if(null != outNode && outNode.contains(this)){
                    if(null == this.inEdge)
                        this.inEdge = Sets.newHashSet();
                    this.inEdge.add(edge);
                }
                break;
            case Hyper:
                if (null == this.hyperEdge)
                    this.hyperEdge = Sets.newHashSet();
                this.hyperEdge.add(edge);
                break;

            default:
                break;
        }
    }


    @Override
    public Iterator<Edge> getIncidentEdges(Direction direction) {
        Set<Edge> result;
        switch (direction){
            case SIMPLEUNDIRECT:
                result = Sets.newHashSet(this.undirectedEdge);
                break;
            case SIMPLEIN:
                result = Sets.newHashSet(this.inEdge);
                break;
            case SIMPLEOUT:
                result = Sets.newHashSet(this.outEdge);
                break;
            case SIMPLEBOTH:
                result = Sets.newHashSet(this.outEdge);
                result.addAll(this.inEdge);
                break;
            case HYPERANY:
                result = Sets.newHashSet(this.hyperEdge);
                break;
            default:
                result = null;
                break;
        }
        return (null != result)?result.iterator():null;
    }

    @Override
    public Iterator<Node> getAdjacentNodes(Direction direction) {
        Set<Node> result = Sets.newHashSet();
        switch (direction){
            case SIMPLEUNDIRECT:
                for(Edge edge:this.undirectedEdge)
                    result.addAll(edge.getNodes(Direction.SIMPLEUNDIRECT));
                result.remove(this);
                break;
            case SIMPLEIN:
                for(Edge edge:this.inEdge)
                    result.addAll(edge.getNodes(Direction.SIMPLEIN));
                break;
            case SIMPLEOUT:
                for(Edge edge:this.outEdge)
                    result.addAll(edge.getNodes(Direction.SIMPLEOUT));
                break;
            case SIMPLEBOTH:
                for(Edge edge:this.inEdge)
                    result.addAll(edge.getNodes(Direction.SIMPLEIN));
                for(Edge edge:this.outEdge)
                    result.addAll(edge.getNodes(Direction.SIMPLEOUT));
                break;
            case HYPERANY:
                for(Edge edge:this.hyperEdge)
                    result.addAll(edge.getNodes(Direction.HYPERANY));
                result.remove(this);
                break;
            default:
                result = null;
                break;
        }
        return (null != result)?result.iterator():null;
    }

    @Override
    public String getUid() {
        return this.element.getUid();
    }

    @Override
    public Object getProperty(String key) {
        return this.element.getProperty(key);
    }

    @Override
    public Set<String> getPropertyKeys() {
        return this.element.getPropertyKeys();
    }

    @Override
    public void removeProperty(String key) {
        this.element.removeProperty(key);
    }

    @Override
    public void setProperty(String key, String value) {
        this.element.setProperty(key,value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlueNode that = (BlueNode) o;

        return !(element != null ? !element.equals(that.element) : that.element != null);

    }

    @Override
    public int hashCode() {
        return element != null ? element.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "BlueNode{" +
                "element=" + element +
                ", outAdjNodes=" + outAdjNodes +
                ", inAdjNodes=" + inAdjNodes +
                ", adjNodes=" + adjNodes +
                '}';
    }
}
