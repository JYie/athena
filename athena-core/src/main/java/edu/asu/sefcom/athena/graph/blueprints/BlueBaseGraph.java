package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.*;
import com.tinkerpop.blueprints.Direction;

import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.*;
import edu.asu.sefcom.athena.graph.element.*;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Edge;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/25/2015.
 *
 * @author Jangwon Yie
 */
public abstract class BlueBaseGraph implements Graph {

    private static final String directed = "DIRECTED";
    private static final String undirected = "UNDIRECTED";

    protected com.tinkerpop.blueprints.Graph blueGraph;
    protected Label label;
    protected PropertyRule rule;

    private GraphType graphType = null;

    public abstract com.tinkerpop.blueprints.Graph generateBlueInstance();

    public abstract void clearUp();

    private void initEngine() {
        if (null == this.blueGraph)
            this.blueGraph = generateBlueInstance();
    }

    @Override
    public Label getLabel() {
        return this.label;
    }

    @Override
    public Node addNode(Map<String, String> properties) throws InvalidParamException {
        initEngine();
        checkPropertyRule(properties);
        String id = IDGenerator.generateID(properties);

        Vertex vertex = this.blueGraph.addVertex(id);

        Node node = new BlueNode(id);
        if (null == properties)
            return node;

        Set<String> keys = properties.keySet();

        for (String key : keys) {
            vertex.setProperty(key, properties.get(key));
            node.setProperty(key, properties.get(key));
        }

        return node;
    }

    @Override
    public void removeNode(String id) {
        initEngine();
        Vertex vertex = this.blueGraph.getVertex(id);
        if (null != vertex)
            this.blueGraph.removeVertex(vertex);
    }

    @Override
    public edu.asu.sefcom.athena.graph.element.Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException {
        edu.asu.sefcom.athena.graph.element.Edge edge = registerEdge(directed, inNode, outNode, properties);
        if (null == this.graphType)
            this.graphType = GraphType.DIRECTED;
        if (this.graphType != GraphType.DIRECTED)
            this.graphType = GraphType.MIXED;
        return edge;
    }

    @Override
    public edu.asu.sefcom.athena.graph.element.Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException {
        edu.asu.sefcom.athena.graph.element.Edge edge = registerEdge(undirected, node1, node2, properties);
        if (null == this.graphType)
            this.graphType = GraphType.UNDIRECTED;
        if (this.graphType != GraphType.UNDIRECTED)
            this.graphType = GraphType.MIXED;
        return edge;
    }

    private edu.asu.sefcom.athena.graph.element.Edge registerEdge(String label, String inNode, String outNode,
                                                                  Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException {
        initEngine();
        checkPropertyRule(properties);
        Vertex out = this.blueGraph.getVertex(outNode);
        Vertex in = this.blueGraph.getVertex(inNode);
        if (null == in || null == out)
            throw new UnavailabeEdgeException("Given vertex id is not available");
        String edgeId = IDGenerator.generateID(null);
        com.tinkerpop.blueprints.Edge blueEdge = this.blueGraph.addEdge(edgeId, in, out, label);

        if (null != properties) {
            Set<String> keys = properties.keySet();
            for (String key : keys) {
                blueEdge.setProperty(key, properties.get(key));
                blueEdge.setProperty(key, properties.get(key));
            }
        }

        return transferBlueEdgeToEdge(blueEdge);
    }

    @Override
    public edu.asu.sefcom.athena.graph.element.Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties)
            throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException {
        if (null != graphType && graphType != GraphType.HYPER)
            throw new UnavailabeEdgeException("GraphType must be unified");

        return null;
    }

    @Override
    public void removeEdge(String id) {
        initEngine();
        com.tinkerpop.blueprints.Edge edge = this.blueGraph.getEdge(id);
        if (null != edge)
            this.blueGraph.removeEdge(edge);
    }

    @Override
    public Iterator<Node> getNodes(String key, String value) {
        initEngine();
        Set<Node> result = Sets.newHashSet();

        Iterable<Vertex> vertices;
        if (null == key || key.isEmpty() || null == value || value.isEmpty())
            vertices = this.blueGraph.getVertices();
        else
            vertices = this.blueGraph.getVertices(key, value);

        if (null == vertices)
            return result.iterator();

        Iterator<Vertex> iterator = vertices.iterator();
        while (iterator.hasNext())
            result.add(transferVertexToNode(iterator.next()));

        return result.iterator();
    }

    @Override
    public Node getNode(String id) {
        initEngine();
        Iterator<Node> candidates = getNodes("id", id);
        if (null == candidates)
            return null;
        while (candidates.hasNext())
            return candidates.next();
        return null;
    }

    @Override
    public Iterator<edu.asu.sefcom.athena.graph.element.Edge> getEdges(String key, String value) {
        initEngine();
        Set<edu.asu.sefcom.athena.graph.element.Edge> result = Sets.newHashSet();

        Iterable<com.tinkerpop.blueprints.Edge> blueEdges;
        if (null == key || key.isEmpty() || null == value || value.isEmpty())
            blueEdges = this.blueGraph.getEdges();
        else
            blueEdges = this.blueGraph.getEdges(key, value);
        if (null == blueEdges)
            return result.iterator();

        Iterator<com.tinkerpop.blueprints.Edge> iterator = blueEdges.iterator();
        while (iterator.hasNext())
            result.add(transferBlueEdgeToEdge(iterator.next()));

        return result.iterator();
    }

    @Override
    public edu.asu.sefcom.athena.graph.element.Edge getEdge(String id) {
        initEngine();
        Iterator<edu.asu.sefcom.athena.graph.element.Edge> candidates = getEdges("id", id);
        if (null == candidates)
            return null;
        while (candidates.hasNext())
            return candidates.next();
        return null;
    }

    @Override
    public Iterator<Node> getNeighbors(String nodeId, edu.asu.sefcom.athena.graph.element.Direction direction, String key, String value) {
        initEngine();
        Set<Node> result = Sets.newHashSet();

        Vertex vertex = this.blueGraph.getVertex(nodeId);
        if (null == vertex)
            return null;
        Iterable<Vertex> neighbors;
        switch (direction) {
            case SIMPLEIN:
                neighbors = vertex.getVertices(Direction.IN, directed);
                break;
            case SIMPLEOUT:
                neighbors = vertex.getVertices(Direction.OUT, directed);
                break;
            case SIMPLEBOTH:
                neighbors = vertex.getVertices(Direction.BOTH, directed);
                break;
            case SIMPLEUNDIRECT:
                neighbors = vertex.getVertices(Direction.BOTH, undirected);
                break;
            default:
                neighbors = null;
        }
        if (null == neighbors)
            return null;
        Iterator<Vertex> neighbors_ = neighbors.iterator();
        while (neighbors_.hasNext()) {
            Vertex neighbor = neighbors_.next();
            if (null == key || null == value)
                result.add(transferVertexToNode(neighbor));
            else {
                String actual = neighbor.getProperty(key);
                if (null != actual && actual.compareTo(value) == 0)
                    result.add(transferVertexToNode(neighbor));
            }
        }

        return result.iterator();
    }

    @Override
    public Iterator<Edge> getIncidentEdges(String nodeId, edu.asu.sefcom.athena.graph.element.Direction direction, String key, String value) {
        initEngine();
        Set<Edge> result = Sets.newHashSet();

        Vertex vertex = this.blueGraph.getVertex(nodeId);
        if (null == vertex)
            return null;
        Iterable<com.tinkerpop.blueprints.Edge> incidents;
        switch (direction) {
            case SIMPLEIN:
                incidents = vertex.getEdges(Direction.IN, directed);
                break;
            case SIMPLEOUT:
                incidents = vertex.getEdges(Direction.OUT, directed);
                break;
            case SIMPLEBOTH:
                incidents = vertex.getEdges(Direction.BOTH, directed);
                break;
            case SIMPLEUNDIRECT:
                incidents = vertex.getEdges(Direction.BOTH, undirected);
                break;
            default:
                incidents = null;
        }
        if (null == incidents)
            return null;
        Iterator<com.tinkerpop.blueprints.Edge> incidents_ = incidents.iterator();
        while (incidents_.hasNext()) {
            com.tinkerpop.blueprints.Edge edge = incidents_.next();
            if (null == key || null == value)
                result.add(transferBlueEdgeToEdge(edge));
            else {
                String actual = edge.getProperty(key);
                if (null != actual && actual.compareTo(value) == 0)
                    result.add(transferBlueEdgeToEdge(edge));
            }
        }
        return result.iterator();
    }

    protected static Node transferVertexToNode(Vertex vertex) {
        Node node = new BlueNode((String) vertex.getId());
        Set<String> keys = vertex.getPropertyKeys();
        if (null == keys)
            return node;

        for (String key : keys)
            node.setProperty(key, vertex.getProperty(key));

        return node;
    }

    /**
     * Blueprint Edge direction is confusing. It returns tail vertex by calling getVertex(Direction.IN),
     * head vertex by calling getVertex(Direction.OUT).
     *
     * @param blueEdge
     * @return
     */
    protected static Edge transferBlueEdgeToEdge(com.tinkerpop.blueprints.Edge blueEdge) {
        String label = blueEdge.getLabel();
        if (null == label)
            assert false;
        Edge edge = null;

        if (label.compareTo(directed) == 0) {
            Vertex in = blueEdge.getVertex(Direction.OUT);
            Vertex out = blueEdge.getVertex(Direction.IN);
            try {
                edge = new BlueWeightedEdge((String) blueEdge.getId(), EdgeType.SimpleDirected, transferVertexToNode(in), transferVertexToNode(out));
            } catch (InvalidParamException e) {
                assert false;
            }

        } else if (label.compareTo(undirected) == 0) {
            Vertex first = blueEdge.getVertex(Direction.IN);
            Vertex second = blueEdge.getVertex(Direction.OUT);
            try {
                edge = new BlueWeightedEdge((String) blueEdge.getId(), EdgeType.SimpleUndirected, transferVertexToNode(first), transferVertexToNode(second));
            } catch (InvalidParamException e) {
                assert false;
            }
        } else {
            return null;
        }

        if (null == edge)
            assert false;
        Set<String> keys = blueEdge.getPropertyKeys();
        for (String key : keys)
            edge.setProperty(key, blueEdge.getProperty(key));
        return edge;
    }

    @Override
    public GraphType getGraphType() {
        return graphType;
    }

    private void checkPropertyRule(Map<String, String> properties) throws InvalidParamException {
        if (null == this.rule || null == properties || properties.isEmpty())
            return;
        String reason = this.rule.isAdmitted(properties);
        if (null == reason || reason.isEmpty())
            return;
        throw new InvalidParamException(reason);
    }
}
