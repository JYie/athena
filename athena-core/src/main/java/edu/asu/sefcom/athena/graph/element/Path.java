package edu.asu.sefcom.athena.graph.element;

/**
 * It represents a path in the graph. Since a path can have huge data, so implementation of the Path interface depends on
 * how huge data you can presume. For instance, if you assume that the data in each path is not that big then you can
 * implement Path interface based on memory.
 * Created by Jangwon Yie on 9/14/15.
 * @author Jangwon Yie
 */
public interface Path {

    /**
     * It returns the number of edges belong to this path.
     * @return
     */
    public int getLength();

    /**
     * It returns the list of nodes belongs to this path.
     * @return
     */
    public Iterable<Node> getNodes();

    /**
     * It returns the list of edges belongs to this path. Any ith edge must contain the ith,i+1th vertices.
     * @return
     */
    public Iterable<Edge> getEdges();

    /**
     * It returns the list of nodes belongs to this path from the tail.
     * @return
     */
    public Iterable<Node> getNodesReversely();

    /**
     * It returns the list of edges belongs to this path from the tail. Any ith edge must contain the ith,i+1th vertices.
     * @return
     */
    public Iterable<Edge> getEdgesReversely();

    /**
     * It returns the first node of this path.
     * @return
     */
    public Node getHead();

    /**
     * It returns the last node of this path.
     * @return
     */
    public Node getTail();
}
