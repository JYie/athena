package edu.asu.sefcom.athena.graph.blueprints;

import java.util.Map;
import java.util.UUID;

/**
 * Created by Jangwon Yie on 9/26/15.
 */
public class IDGenerator {

    public  static String generateID(Map<String, String> resources){
        return UUID.randomUUID().toString();
    }
}
