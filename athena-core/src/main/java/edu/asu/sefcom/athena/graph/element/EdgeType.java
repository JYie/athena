package edu.asu.sefcom.athena.graph.element;

/**
 * Created by Jangwon Yie on 9/13/15.
 */
public enum EdgeType {

    SimpleDirected, SimpleUndirected, Hyper
}
