package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 11/30/2015.
 *
 * @author Jangwon Yie
 */
public abstract class BasicGraphTestSpec {

    protected Graph graph;

    @Before
    public void setUpBefore() {
        setUpTarget();
    }

    @After
    public void tearDownAfter() {
        tearDownTarget();
    }

    protected abstract void setUpTarget();

    protected abstract void tearDownTarget();

    @Test
    public void testNodeLifecycle() {
        Map<String, String> node1Properties = Maps.newHashMap();
        String expectedLink = "13";
        String expectedSize = "1251";
        node1Properties.put("outlink", expectedLink);
        node1Properties.put("length", expectedSize);
        Node node = null;
        try {
            node = this.graph.addNode(node1Properties);
            Set<String> actualKeys = node.getPropertyKeys();
            Set<String> expectedKeys = Sets.newHashSet("outlink", "length");
            Assert.assertTrue(null != actualKeys && actualKeys.containsAll(expectedKeys));

            String actualLink = (String) node.getProperty("outlink");
            String actualSize = (String) node.getProperty("length");

            Assert.assertTrue(null != actualLink && actualLink.compareTo(expectedLink) == 0);
            Assert.assertTrue(null != actualSize && actualSize.compareTo(expectedSize) == 0);

//            Set<Node> nodes = Sets.newHashSet(this.graph.getNodes("id", node.getUid()));
//            if (null == nodes)
//                Assert.fail();
//
//            Assert.assertTrue(nodes.contains(node));

            Set<Node> nodes = Sets.newHashSet(this.graph.getNodes(null, null));
            if (null == nodes)
                Assert.fail();

            Assert.assertTrue(nodes.contains(node));

            this.graph.removeNode(node.getUid());
            nodes = Sets.newHashSet(this.graph.getNodes("id", node.getUid()));
            if (null == nodes || nodes.size() == 0)
                Assert.assertTrue(true);
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testEdgeLifeCycle() {
        Edge directed12 = null;
        Edge undirected23 = null;

        Node node1 = null;
        Node node2 = null;
        Node node3 = null;
        try {
            node1 = this.graph.addNode(null);
            node2 = this.graph.addNode(null);
            node3 = this.graph.addNode(null);

            Map<String, String> properties = Maps.newHashMap();
            properties.put("name", "testProperty");
            directed12 = this.graph.addDirectedEdge(node1.getUid(), node2.getUid(), properties);
            undirected23 = this.graph.addUndirectedEdge(node2.getUid(), node3.getUid(), null);

            Assert.assertTrue(directed12.getType().equals(EdgeType.SimpleDirected));
            Assert.assertTrue(undirected23.getType().equals(EdgeType.SimpleUndirected));

            Edge directed12_ = this.graph.getEdge(directed12.getUid());
            Edge undirected23_ = this.graph.getEdge(undirected23.getUid());

            Assert.assertTrue(directed12_.getType().equals(EdgeType.SimpleDirected));
            Assert.assertTrue(undirected23_.getType().equals(EdgeType.SimpleUndirected));

            Iterator<Edge> edges = this.graph.getEdges(null, null);
            if (null == edges)
                Assert.fail();

            int expectedSize = 2;
            int actualSize = 0;
            while (edges.hasNext()) {
                edges.next();
                actualSize++;
            }

            Assert.assertTrue(actualSize == expectedSize);

            Set<Node> nodesIn = directed12_.getNodes(Direction.SIMPLEIN);
            if (null == nodesIn || nodesIn.size() == 0)
                Assert.fail();
            Set<Node> expectedIn = Sets.newHashSet(node1);
            Assert.assertTrue(nodesIn.containsAll(expectedIn));

            Set<Node> nodesOut = directed12_.getNodes(Direction.SIMPLEOUT);
            if (null == nodesOut || nodesOut.size() == 0)
                Assert.fail();

            Set<Node> expectedOut = Sets.newHashSet(node2);
            Assert.assertTrue(nodesOut.containsAll(expectedOut));

            Set<Node> nodesUndirected = undirected23_.getNodes(Direction.SIMPLEUNDIRECT);
            if (null == nodesUndirected || nodesUndirected.size() == 0)
                Assert.fail();

            Set<Node> expectedUndirected = Sets.newHashSet(node2, node3);
            Assert.assertTrue(nodesUndirected.containsAll(expectedUndirected));

            String actualName = (String) directed12_.getProperty("name");
            Assert.assertTrue(null != actualName && actualName.compareTo("testProperty") == 0);

            this.graph.removeEdge(directed12_.getUid());
            this.graph.removeEdge(undirected23_.getUid());

            directed12_ = this.graph.getEdge(directed12.getUid());
            undirected23_ = this.graph.getEdge(undirected23.getUid());

            Assert.assertTrue(null == directed12_);
            Assert.assertTrue(null == undirected23_);
        } catch (UnavailabeEdgeException uee) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testLifeCycle() {
        Edge undirected12 = null;
        Edge undirected23 = null;

        Node node1 = null;
        Node node2 = null;
        Node node3 = null;
        try {
            node1 = this.graph.addNode(null);
            node2 = this.graph.addNode(null);
            node3 = this.graph.addNode(null);
            undirected12 = this.graph.addUndirectedEdge(node1.getUid(), node2.getUid(), null);
            undirected23 = this.graph.addUndirectedEdge(node3.getUid(), node2.getUid(), null);

            Iterator<Edge> node1Incidents = this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEUNDIRECT, null, null);
            int expectedIncidents = 1;
            int actualIncidents = 0;
            if (null != node1Incidents) {
                while (node1Incidents.hasNext()) {
                    node1Incidents.next();
                    actualIncidents++;
                }
            }

            System.out.println(actualIncidents);
            Assert.assertTrue(actualIncidents == expectedIncidents);


            this.graph.removeNode(node2.getUid());

            Iterator<Edge> edges = this.graph.getEdges(null, null);
            if (null == edges)
                Assert.fail();

            int expectedSize = 0;
            int actualSize = 0;
            while (edges.hasNext()) {
                edges.next();
                actualSize++;
            }

            Assert.assertTrue(actualSize == expectedSize);

            Iterator<Edge> node1IncidentsAfterRemoving = this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEUNDIRECT, null, null);
            expectedIncidents = 0;
            actualIncidents = 0;
            if (null != node1IncidentsAfterRemoving) {
                while (node1IncidentsAfterRemoving.hasNext()) {
                    node1IncidentsAfterRemoving.next();
                    actualIncidents++;
                }
            }
            Assert.assertTrue(actualIncidents == expectedIncidents);
        } catch (UnavailabeEdgeException uee) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testSearchElement() {

        String type = "type";

        String nodeType1 = "user";
        String nodeType2 = "post";
        String edgeType1 = "authorOf";
        String edgeType2 = "follows";

        Map<String, String> node1Properties = Maps.newHashMap();
        node1Properties.put(type, nodeType1);

        Map<String, String> node2Properties = Maps.newHashMap();
        node2Properties.put(type, nodeType1);

        Map<String, String> node3Properties = Maps.newHashMap();
        node3Properties.put(type, nodeType2);

        Node node1 = null;
        Node node2 = null;
        Node node3 = null;
        try {
            node1 = this.graph.addNode(node1Properties);
            node2 = this.graph.addNode(node2Properties);
            node3 = this.graph.addNode(node3Properties);

            Iterator<Node> nodes1 = this.graph.getNodes(type, nodeType1);
            Iterator<Node> nodes2 = this.graph.getNodes(type, nodeType2);
            Iterator<Node> nodes3 = this.graph.getNodes(null, null);

            Assert.assertTrue(null != nodes1 && null != nodes2 && null != nodes3);

            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            while (nodes1.hasNext()) {
                nodes1.next();
                count1++;
            }
            Assert.assertTrue(count1 == 2);

            while (nodes2.hasNext()) {
                nodes2.next();
                count2++;
            }
            Assert.assertTrue(count2 == 1);

            while (nodes3.hasNext()) {
                nodes3.next();
                count3++;
            }
            Assert.assertTrue(count3 == 3);

            Map<String, String> edge1Properties = Maps.newHashMap();
            edge1Properties.put(type, edgeType1);

            Map<String, String> edge2Properties = Maps.newHashMap();
            edge2Properties.put(type, edgeType2);


            this.graph.addUndirectedEdge(node1.getUid(), node3.getUid(), edge1Properties);
            this.graph.addUndirectedEdge(node2.getUid(), node3.getUid(), edge1Properties);
            this.graph.addDirectedEdge(node1.getUid(), node2.getUid(), edge2Properties);

            Iterator<Edge> edges1 = this.graph.getEdges(type, edgeType1);
            Iterator<Edge> edges2 = this.graph.getEdges(type, edgeType2);
            Iterator<Edge> edges3 = this.graph.getEdges(null, null);

            count1 = 0;
            count2 = 0;
            count3 = 0;
            while (edges1.hasNext()) {
                edges1.next();
                count1++;
            }
            Assert.assertTrue(count1 == 2);

            while (edges2.hasNext()) {
                edges2.next();
                count2++;
            }
            Assert.assertTrue(count2 == 1);

            while (edges3.hasNext()) {
                edges3.next();
                count3++;
            }
            Assert.assertTrue(count3 == 3);
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        }
    }


    @Test(expected = UnavailabeEdgeException.class)
    public void testAddinEdgeException1() throws UnavailabeEdgeException, InvalidParamException {
        try {
            this.graph.addDirectedEdge("undefinedvertexId1", "undefinedvertexId2", null);
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test(expected = UnavailabeEdgeException.class)
    public void testAddinEdgeException2() throws UnavailabeEdgeException, InvalidParamException {
        try {
            this.graph.addUndirectedEdge("undefinedvertexId1", "undefinedvertexId2", null);
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testNeighbors() {
        Node node1 = null;
        Node node2 = null;
        Node node3 = null;
        Node node4 = null;
        try {
            Map<String, String> properties = Maps.newHashMap();
            properties.put("relation", "special");

            node1 = this.graph.addNode(null);
            node2 = this.graph.addNode(null);
            node3 = this.graph.addNode(properties);
            node4 = this.graph.addNode(null);
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        Edge directed12;
        Edge undirected23;
        Edge directed31;
        Edge undirected34;
        try {
            directed12 = this.graph.addDirectedEdge(node1.getUid(), node2.getUid(), null);
            undirected23 = this.graph.addUndirectedEdge(node2.getUid(), node3.getUid(), null);
            directed31 = this.graph.addDirectedEdge(node3.getUid(), node1.getUid(), null);
            undirected34 = this.graph.addUndirectedEdge(node3.getUid(), node4.getUid(), null);
            Set<Node> neighrbors1out = Sets.newHashSet(this.graph.getNeighbors(node1.getUid(), Direction.SIMPLEOUT, null, null));
            Set<Node> neighrbors1in = Sets.newHashSet(this.graph.getNeighbors(node1.getUid(), Direction.SIMPLEIN, null, null));
            Set<Node> neighrbors1in_special = Sets.newHashSet(this.graph.getNeighbors(node1.getUid(), Direction.SIMPLEIN, "relation", "special"));
            Set<Node> neighrbors1in_normal = Sets.newHashSet(this.graph.getNeighbors(node1.getUid(), Direction.SIMPLEIN, "relation", "normal"));

            Set<Node> neighrbors2out = Sets.newHashSet(this.graph.getNeighbors(node2.getUid(), Direction.SIMPLEOUT, null, null));
            Set<Node> neighrbors2in = Sets.newHashSet(this.graph.getNeighbors(node2.getUid(), Direction.SIMPLEIN, null, null));
            Set<Node> neighrbors3 = Sets.newHashSet(this.graph.getNeighbors(node3.getUid(), Direction.SIMPLEUNDIRECT, null, null));

            Set<Node> expected1out = Sets.newHashSet(node2);
            Set<Node> expected1In = Sets.newHashSet(node3);
            Set<Node> expected2In = Sets.newHashSet(node1);
            Set<Node> expected3un = Sets.newHashSet(node2,node4);

            Assert.assertTrue(expected1out.containsAll(neighrbors1out) && neighrbors1out.containsAll(expected1out));
            Assert.assertTrue(expected1In.containsAll(neighrbors1in) && neighrbors1in.containsAll(expected1In));
            Assert.assertTrue(expected1In.containsAll(neighrbors1in_special) && neighrbors1in_special.containsAll(expected1In));
            Assert.assertTrue(null == neighrbors1in_normal || Sets.newHashSet(neighrbors1in_normal).isEmpty());

            Assert.assertTrue(expected2In.containsAll(neighrbors2in) && neighrbors2in.containsAll(expected2In));
            Assert.assertTrue(null == neighrbors2out || Sets.newHashSet(neighrbors2out).isEmpty());
            Assert.assertTrue(expected3un.containsAll(neighrbors3) && neighrbors3.containsAll(expected3un));
        } catch (UnavailabeEdgeException uee) {
            Assert.fail();
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testIncidentEdges() {
        Node node1 = null;
        Node node2 = null;
        Node node3 = null;
        try {
            node1 = this.graph.addNode(null);
            node2 = this.graph.addNode(null);
            node3 = this.graph.addNode(null);
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        Edge directed12;
        Edge undirected23;
        Edge directed31;
        try {
            directed12 = this.graph.addDirectedEdge(node1.getUid(), node2.getUid(), null);
            undirected23 = this.graph.addUndirectedEdge(node2.getUid(), node3.getUid(), null);

            Map<String, String> properties = Maps.newHashMap();
            properties.put("relation", "special");

            directed31 = this.graph.addDirectedEdge(node3.getUid(), node1.getUid(), properties);
            Set<Edge> incident1Out = Sets.newHashSet(this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEOUT, null, null));
            Set<Edge> incident1In = Sets.newHashSet(this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEIN, null, null));
            Set<Edge> incident1In_special = Sets.newHashSet(this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEIN, "relation", "special"));
            Set<Edge> incident1In_normal = Sets.newHashSet(this.graph.getIncidentEdges(node1.getUid(), Direction.SIMPLEIN, "relation", "normal"));

            Set<Edge> incident2out = Sets.newHashSet(this.graph.getIncidentEdges(node2.getUid(), Direction.SIMPLEOUT, null, null));
            Set<Edge> incident2in = Sets.newHashSet(this.graph.getIncidentEdges(node2.getUid(), Direction.SIMPLEIN, null, null));
            Set<Edge> incident3 = Sets.newHashSet(this.graph.getIncidentEdges(node3.getUid(), Direction.SIMPLEUNDIRECT, null, null));
            Set<Edge> incident2 = Sets.newHashSet(this.graph.getIncidentEdges(node2.getUid(), Direction.SIMPLEUNDIRECT, null, null));

            Set<Edge> expected1out = Sets.newHashSet(directed12);
            Set<Edge> expected1In = Sets.newHashSet(directed31);
            Set<Edge> expected2In = Sets.newHashSet(directed12);
            Set<Edge> expected3un = Sets.newHashSet(undirected23);
            Set<Edge> expected2un = Sets.newHashSet(undirected23);

            Assert.assertTrue(expected1out.containsAll(incident1Out) && incident1Out.containsAll(expected1out));
            Assert.assertTrue(expected1In.containsAll(incident1In) && incident1In.containsAll(expected1In));
            Assert.assertTrue(expected1In.containsAll(incident1In_special) && incident1In_special.containsAll(expected1In));

            Assert.assertTrue(null == incident1In_normal || incident1In_normal.isEmpty());
            Assert.assertTrue(expected2In.containsAll(incident2in) && incident2in.containsAll(expected2In));
            Assert.assertTrue(null == incident2out || incident2out.isEmpty());
            Assert.assertTrue(Sets.newHashSet(incident3).containsAll(expected3un) && expected3un.containsAll(incident3));
            Assert.assertTrue(Sets.newHashSet(incident2).containsAll(expected2un) && expected2un.containsAll(incident2));

        } catch (UnavailabeEdgeException uee) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }
}
