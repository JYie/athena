package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.InvalidParamException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public class EdgeTest {

//    @Test(expected = InvalidParamException.class)
//    public void testConstructor1() throws InvalidParamException{
//        long weight = -1;
//        Edge edge = new Edge(-1,new Node[] {new Node(1), new Node(2)});
//    }
//
//    @Test(expected = InvalidParamException.class)
//    public void testConstructor2() throws InvalidParamException{
//        Edge edge = new Edge(new Node[] {new Node(1)});
//    }
//
//    @Test(expected = InvalidParamException.class)
//    public void testConstructor3() throws InvalidParamException{
//        Edge edge = new Edge(EdgeType.SimpleUndirected, new Node[] {new Node(1), new Node(2), new Node(3)});
//    }
//
//    @Test
//    public void testConstructor4() throws InvalidParamException{
//        Edge edge = new Edge(EdgeType.Hyper, new Node[] {new Node(1), new Node(2), new Node(3)});
//        Assert.assertTrue(true);
//    }
//
//    @Test
//    public void testEquality(){
//        try {
//            Edge edge1 = new Edge(EdgeType.Hyper, new Node[] {new Node(1), new Node(2), new Node(3)});
//            Edge edge2 = new Edge(EdgeType.SimpleUndirected, new Node[] {new Node(1), new Node(2)});
//            Assert.assertTrue("test simple cannot be equal to hyper " ,!edge1.equals(edge2));
//
//            edge1 = new Edge(EdgeType.SimpleUndirected, new Node[] {new Node(1), new Node(2)});
//            edge2 = new Edge(EdgeType.SimpleUndirected, new Node[] {new Node(2), new Node(1)});
//            Assert.assertTrue("test undirected edge does not care the order ", edge1.equals(edge2));
//
//            edge1 = new Edge(EdgeType.SimpleDirected, new Node[] {new Node(1), new Node(2)});
//            edge2 = new Edge(EdgeType.SimpleDirected, new Node[] {new Node(2), new Node(1)});
//            Assert.assertTrue("test directed edge care the order",!edge1.equals(edge2));
//
//            edge1 = new Edge(EdgeType.SimpleDirected, new Node[] {new Node(1), new Node(2)});
//            edge2 = new Edge(EdgeType.SimpleDirected, new Node[] {new Node(1), new Node(2)});
//            Assert.assertTrue("test directed edge care the order", edge1.equals(edge2));
//        } catch (InvalidParamException e) {
//            Assert.fail();
//        }
//    }
}
