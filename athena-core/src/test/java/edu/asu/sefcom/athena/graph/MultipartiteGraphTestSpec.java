package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.blueprints.BlueMultipartiteGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 11/30/2015.
 *
 * @author Jangwon Yie
 */
public abstract class MultipartiteGraphTestSpec {
    protected MultiPartiteGraph multipartiteGraph;

    private Node node1;
    private Node node2;
    private Node node3;
    private Node node4;

    private void setUpTestInfo() {
        Map<String, String> forApartition = Maps.newHashMap();
        forApartition.put(MultiPartiteGraph.PARTITION, "A");
        Map<String, String> forBpartition = Maps.newHashMap();
        forBpartition.put(MultiPartiteGraph.PARTITION, "B");
        try {
            node1 = this.multipartiteGraph.addNode(forApartition);
            node2 = this.multipartiteGraph.addNode(forApartition);
            node3 = this.multipartiteGraph.addNode(forBpartition);
            node4 = this.multipartiteGraph.addNode(forBpartition);
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Before
    public void setUpBefore() {
        setUpTarget();
        setUpTestInfo();
    }

    @After
    public void tearDownAfter(){
        tearDownTarget();
    }

    protected abstract void setUpTarget();

    protected abstract void tearDownTarget();

    @Test(expected = UnavailabeEdgeException.class)
    public void testEdgeValidity() throws UnavailabeEdgeException {
        try {
            this.multipartiteGraph.addDirectedEdge(node1.getUid(), node2.getUid(), null);
            this.multipartiteGraph.addUndirectedEdge(node1.getUid(), node2.getUid(), null);
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testPartitionFunction() {
        Set<String> actualPartitionKeys = this.multipartiteGraph.getPartitionKeys();
        int expectedPartitionKeys = 2;
        Assert.assertTrue(null != actualPartitionKeys && actualPartitionKeys.size() == expectedPartitionKeys);

        Set<String> actualNodesInA = this.multipartiteGraph.getNodesInPartition("A");
        Set<String> expectedNodesinA = Sets.newHashSet();
        expectedNodesinA.add(node1.getUid());
        expectedNodesinA.add(node2.getUid());

        Assert.assertTrue(null != actualNodesInA && actualNodesInA.containsAll(expectedNodesinA)
                && expectedNodesinA.containsAll(actualNodesInA));

        String actualParitionForNode3 = this.multipartiteGraph.getPartition(node3.getUid());
        String expectedParitionForNode3 = "B";
        Assert.assertTrue(null != actualParitionForNode3
                && actualParitionForNode3.compareTo(expectedParitionForNode3) == 0);
    }
}
