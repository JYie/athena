package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Jangwon Yie on 9/25/2015.
 * @author Jangwon Yie
 */
public class BlueEdgeTest {

    @Test(expected = InvalidParamException.class)
    public void testConstructor0() throws InvalidParamException{
        Node[] nodes = {new BlueNode("1")};
        BlueEdge edge = new BlueEdge("test", EdgeType.SimpleDirected, nodes);
    }

    @Test(expected = InvalidParamException.class)
    public void testConstructor1() throws InvalidParamException{
        Node[] nodes = {new BlueNode("1"), new BlueNode("2"), new BlueNode("3")};
        BlueEdge edge = new BlueEdge("test", EdgeType.SimpleDirected, nodes);
    }

    @Test(expected = InvalidParamException.class)
    public void testConstructor2() throws InvalidParamException{
        Node[] nodes = {new BlueNode("1"), new BlueNode("2"), new BlueNode("3")};
        BlueEdge edge = new BlueEdge("test", EdgeType.SimpleUndirected, nodes);
    }

    @Test
    public void testConstructor3() {
        Node[] nodes = {new BlueNode("1"), new BlueNode("2")};
        try{
            BlueEdge edge = new BlueEdge("test", EdgeType.SimpleUndirected, nodes);
            Assert.assertTrue(true);
        }catch (InvalidParamException ipe){
            Assert.fail();
        }
    }

    @Test
    public void testConstructor4() {
        Node[] nodes = {new BlueNode("1"), new BlueNode("2"), new BlueNode("3")};
        try{
            BlueEdge edge = new BlueEdge("test", EdgeType.Hyper, nodes);
            Assert.assertTrue(true);
        }catch (InvalidParamException ipe){
            Assert.fail();
        }
    }


    @Test
    public void testEquality(){
        Node[] nodes1 = {new BlueNode("1"), new BlueNode("2")};
        Node[] nodes2 = {new BlueNode("1"), new BlueNode("2"), new BlueNode("3")};
        Node[] nodes3 = {new BlueNode("2"), new BlueNode("1")};
        Node[] nodes4 = {new BlueNode("2"), new BlueNode("1"), new BlueNode("3")};
        try {
            BlueEdge simpleUndirectededge = new BlueEdge("test", EdgeType.SimpleUndirected, nodes1);
            BlueEdge simpleUndirectededge_ = new BlueEdge("test", EdgeType.SimpleUndirected, nodes3);
            BlueEdge simpleDirectedEdge = new BlueEdge("test", EdgeType.SimpleDirected, nodes1);
            BlueEdge simpleDirectedEdge_ = new BlueEdge("test", EdgeType.SimpleDirected, nodes3);
            BlueEdge hyperEdge = new BlueEdge("test", EdgeType.Hyper, nodes2);
            BlueEdge hyperEdge_ = new BlueEdge("test", EdgeType.Hyper, nodes4);

            Assert.assertTrue("test directedEdge cannot be equal to undirectedEdge " ,!simpleUndirectededge.equals(simpleDirectedEdge));
            Assert.assertTrue("test simpleEdge cannot be equal to hyperEdge " ,!simpleUndirectededge.equals(hyperEdge));
            Assert.assertTrue("test simpleEdge cannot be equal to hyperEdge " ,!simpleDirectedEdge.equals(hyperEdge));

            Assert.assertTrue("test order of directed edge", !simpleDirectedEdge.equals(simpleDirectedEdge_));
            Assert.assertTrue("test order of undirected edge", simpleUndirectededge.equals(simpleUndirectededge_));
            Assert.assertTrue("test order of hyper edge", hyperEdge.equals(hyperEdge_));
        } catch (InvalidParamException e) {
            Assert.fail();
        }
    }

    @Test
    public void testDirection(){
        Node[] nodes1 = {new BlueNode("1"), new BlueNode("2")};
        Node[] nodes2 = {new BlueNode("1"), new BlueNode("2"), new BlueNode("3")};
        try {
            BlueEdge simpleUndirectededge = new BlueEdge("test", EdgeType.SimpleUndirected, nodes1);
            BlueEdge simpleDirectedEdge = new BlueEdge("test", EdgeType.SimpleDirected, nodes1);
            BlueEdge hyperEdge = new BlueEdge("test", EdgeType.Hyper, nodes2);

            Assert.assertTrue(simpleDirectedEdge.getNodes(Direction.SIMPLEBOTH).equals(Sets.newHashSet(nodes1)));
            Assert.assertTrue(simpleDirectedEdge.getNodes(Direction.SIMPLEIN).equals(Sets.newHashSet(nodes1[0])));
            Assert.assertTrue(simpleDirectedEdge.getNodes(Direction.SIMPLEOUT).equals(Sets.newHashSet(nodes1[1])));

            Assert.assertTrue(simpleUndirectededge.getNodes(Direction.SIMPLEUNDIRECT).equals(Sets.newHashSet(nodes1)));

            Assert.assertTrue(hyperEdge.getNodes(Direction.HYPERANY).equals(Sets.newHashSet(nodes2)));
        } catch (InvalidParamException e) {
            Assert.fail();
        }
    }
}
