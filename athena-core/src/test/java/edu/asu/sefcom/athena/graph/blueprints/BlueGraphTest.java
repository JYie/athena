package edu.asu.sefcom.athena.graph.blueprints;

import edu.asu.sefcom.athena.graph.*;

import edu.asu.sefcom.athena.graph.element.*;

/**
 * Created by Jangwon Yie on 9/25/15.
 * @author Jangwon Yie
 */
public class BlueGraphTest extends BasicGraphTestSpec {

    @Override
    public void setUpTarget() {
        this.graph = new BlueGraph(new Label("test"));
    }

    @Override
    protected void tearDownTarget() {

    }
}
