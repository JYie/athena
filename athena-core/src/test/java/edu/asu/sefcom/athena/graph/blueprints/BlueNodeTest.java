package edu.asu.sefcom.athena.graph.blueprints;

import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.EdgeType;
import edu.asu.sefcom.athena.graph.element.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

/**
 * Created by Jangwon Yie on 9/25/2015.
 * @author Jangwon Yie
 */
public class BlueNodeTest {

    private BlueNode blueNode;
    private Node outTarget;
    private Node inTarget;

    private Edge edge4out;
    private Edge edge4in;
    private Edge edge4hyper;

    @Before
    public void setUpBefore() {
        this.blueNode = new BlueNode("test");
        this.outTarget = new BlueNode("out1");
        this.inTarget = new BlueNode("in1");
        try {
            this.edge4out = new BlueEdge("edge1", EdgeType.SimpleDirected, this.blueNode, outTarget);
            this.edge4in = new BlueEdge("edge2", EdgeType.SimpleDirected, inTarget, this.blueNode);
            this.edge4hyper = new BlueEdge("hyperEdge", EdgeType.Hyper, this.blueNode, inTarget, outTarget);
            this.blueNode.putIncidentEdge(edge4out);
            this.blueNode.putIncidentEdge(edge4in);
            this.blueNode.putIncidentEdge(edge4hyper);
        } catch (InvalidParamException ipe) {
            Assert.fail();
        }
    }

    @Test
    public void testIncidentEdges1(){
            Iterator<Edge> inEdge = this.blueNode.getIncidentEdges(Direction.SIMPLEIN);
            while(inEdge.hasNext()){
                Assert.assertTrue(inEdge.next().equals(edge4in));
            }
            Iterator<Edge> outEdge = this.blueNode.getIncidentEdges(Direction.SIMPLEOUT);
            while(outEdge.hasNext()){
                Assert.assertTrue(outEdge.next().equals(edge4out));
            }
            Iterator<Edge> hyperEdge = this.blueNode.getIncidentEdges(Direction.HYPERANY);
            while(hyperEdge.hasNext()){
                Assert.assertTrue(hyperEdge.next().equals(edge4hyper));
            }
    }

    @Test
    public void testAdjacentNodes1(){
        Iterator<Node> inNode = this.blueNode.getAdjacentNodes(Direction.SIMPLEIN);
        if(null == inNode)
            Assert.fail();
        while(inNode.hasNext())
            Assert.assertTrue(inNode.next().equals(this.inTarget));

        Iterator<Node> outNode = this.blueNode.getAdjacentNodes(Direction.SIMPLEOUT);
        if(null == outNode)
            Assert.fail();
        while(outNode.hasNext())
            Assert.assertTrue(outNode.next().equals(this.outTarget));

        Iterator<Node> nodes = this.blueNode.getAdjacentNodes(Direction.HYPERANY);
        if(null == nodes)
            Assert.fail();
        boolean result = true;
        while(nodes.hasNext()){
            Node node = nodes.next();
            if(!node.equals(this.inTarget) && !node.equals(this.outTarget))
                result = false;
        }
        Assert.assertTrue(result);
    }
}

