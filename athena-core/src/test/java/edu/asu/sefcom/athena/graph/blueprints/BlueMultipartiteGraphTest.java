package edu.asu.sefcom.athena.graph.blueprints;

import edu.asu.sefcom.athena.graph.MultipartiteGraphTestSpec;
import edu.asu.sefcom.athena.graph.element.Label;

/**
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class BlueMultipartiteGraphTest extends MultipartiteGraphTestSpec{

    @Override
    public void setUpTarget(){
        this.multipartiteGraph = new BlueMultipartiteGraph(new BlueGraph(new Label("test")));
    }

    @Override
    protected void tearDownTarget() {

    }

}
