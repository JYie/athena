package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.InvalidStructureException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/14/15.
 */
public class GraphServiceTest {

    private static Set<Class<? extends GraphService>> serviceClasses;
    private static boolean testable = true;
    private Map<String, GraphService> graphServices;
    private Set<String> serviceKeys;

    private Label label = new Label() {
        @Override
        public String getLabel() {
            return "forTest";
        }
    };


    @BeforeClass
    public static void setUpBeforeClass(){

        Reflections reflections = new Reflections("edu.asu.sefcom.athena.graph");
        serviceClasses = reflections.getSubTypesOf(GraphService.class);
        if(null == serviceClasses || serviceClasses.size() == 0) {
            Assert.assertTrue("There is no GraphService implementation to be tested", true);
            testable = false;
        }
    }

    @Before
    public void setUp(){
        this.graphServices = Maps.newHashMap();
        if(testable) {
            for (Class<? extends GraphService> service : serviceClasses) {
                try {
                    graphServices.put(service.getName(), service.newInstance());
                } catch (IllegalAccessException ie) {
                    continue;
                } catch (InstantiationException e) {
                    continue;
                }
            }
            if (null == graphServices || graphServices.size() == 0) {
                Assert.assertTrue("No GraphService implementation is instantiated to be tested", true);
                testable = false;
            }
            serviceKeys = graphServices.keySet();
            setUpData();
        }
    }

    @Test
    public void testBasicStat(){
        if(!testable)
            return;
        for(String key:serviceKeys){
            GraphService service = graphServices.get(key);
            if(null != service) {
                Set<Label> labels = service.getLabels();
                Assert.assertTrue(null != labels && labels.size() == 1);
                int expectedHyperNum = 2;
                int actualHyperNum = service.getHyperNumber(label);
                Assert.assertTrue(expectedHyperNum == actualHyperNum);
                int expectedOrder = 3;
                int actualOrder = service.getOrder(label);
                Assert.assertTrue("orderTest at " + key,expectedOrder == actualOrder);
                int expectedSize = 1;
                int actualSize = service.getSize(label);
                Assert.assertTrue("sizeTest at " + key,expectedSize == actualSize);

                Set<Edge> actualNeighbor =  service.getNeighbors(new Node(2));
                try {
                    Edge expectedNeighbor = new Edge(EdgeType.SimpleUndirected, new Node(2), new Node(3));
                    Assert.assertTrue("neighborTest at " + key, null != actualNeighbor && actualNeighbor.size() == 1 && actualNeighbor.contains(expectedNeighbor));
                } catch (InvalidParamException e) {
                    Assert.fail("Invalid ParameterException occured at : " + key);
                }
            }
        }
    }

    @Test
    public void testRemovingEdge(){
        if(!testable)
            return;
        for(String key:serviceKeys) {
            GraphService service = graphServices.get(key);
            if (null != service) {
                try {
                    Edge edge = new Edge(EdgeType.SimpleUndirected, new Node(2), new Node(3));
                    service.removeEdge(edge);

                    int expectedOrder = 1;
                    int actualOrder = service.getOrder(label);
                    Assert.assertTrue("orderTest at " + key,expectedOrder == actualOrder);
                    int expectedSize = 0;
                    int actualSize = service.getSize(label);
                    Assert.assertTrue("sizeTest at " + key,expectedSize == actualSize);

                    Set<Edge> actualNeighbor =  service.getNeighbors(new Node(2));
                    Assert.assertTrue("neighborTest at " + key, null == actualNeighbor || actualNeighbor.size() == 0);
                }catch (InvalidParamException e) {
                    Assert.fail("Invalid ParameterException occured at : " + key);
                }
            }
        }
    }

    private void setUpData(){
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        for(String key:serviceKeys){
            GraphService service = graphServices.get(key);
            if(null != service) {
                service.createNode(label, node1);
                try {
                    Edge edge = new Edge(EdgeType.SimpleUndirected, node2, node3);
                    service.addEdge(edge);
                } catch (InvalidParamException e) {
                    Assert.fail("Invalid ParameterException occured at : " + key);
                } catch (InvalidStructureException e) {
                    Assert.fail("InvalidStructureException occured at : " + key);
                }
            }
        }
    }
}

