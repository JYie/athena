package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * Created by Jangwon Yie on 9/16/15.
 * @author Jangwon Yie
 */
public class CliqueTest {

    @Test(expected = InvalidParamException.class)
    public void testConstructor1() throws InvalidParamException {
        int hyperNum = 1;
        Clique clique = new Clique(hyperNum);
    }

    @Test
    public void testConstructor2() throws InvalidParamException {
        int hyperNum = 3;
        Clique clique = new Clique(hyperNum);
        Assert.assertTrue(true);
    }

    @Test
    public void testEdgeNum1() throws  InvalidParamException{
        int n = 10;
        Clique clique = new Clique();
        for(int i=0;i<n;i++)
            clique.addNode(new Node(i));

        Assert.assertTrue(n == clique.getOrder());
        Assert.assertTrue(45 == clique.getSize()); //45 comes from 10 C 2
    }

    @Test
    public void testEdgeNum2() throws InvalidParamException{
        int n = 8;
        Clique clique = new Clique(3);
        for(int i=0;i<n;i++)
            clique.addNode(new Node(i));

        int expectedSize = 56; //56 comes from 8 C 3
        System.out.println(clique.getSize());
        Assert.assertTrue(expectedSize == clique.getSize());
    }

    @Test
    public void testEquality1() throws InvalidParamException{
        Clique clique1 = new Clique();
        for(int i=0;i<3;i++)
            clique1.addNode(new Node(i));

        Clique clique2 = new Clique(3);
        for(int i=0;i<3;i++)
            clique2.addNode(new Node(i));

        Assert.assertTrue(!clique1.equals(clique2));
    }


    @Test
    public void testEquality2() throws InvalidParamException{
        Clique clique1 = new Clique();
        for(int i=0;i<3;i++)
            clique1.addNode(new Node(i));

        Clique clique2 = new Clique();
        for(int i=0;i<2;i++)
            clique2.addNode(new Node(i));
        clique2.addNode(new Node(3));

        Assert.assertTrue(!clique1.equals(clique2));
    }


    @Test
    public void testEquality3() throws InvalidParamException{
        Clique clique1 = new Clique();
        for(int i=0;i<4;i++)
            clique1.addNode(new Node(i));

        Clique clique2 = new Clique();
        for(int i=0;i<3;i++)
            clique2.addNode(new Node(i));

        Assert.assertTrue(!clique1.equals(clique2));
    }

    @Test
    public void testEquality4() throws InvalidParamException{
        Clique clique1 = new Clique(3);
        for(int i=0;i<10;i++)
            clique1.addNode(new Node(i));

        Clique clique2 = new Clique(3);
        for(int i=0;i<10;i++)
            clique2.addNode(new Node(i));

        Assert.assertTrue(clique1.equals(clique2));
    }
}
