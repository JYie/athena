package edu.asu.sefcom.athena;

/**
 * It is throwned if the id which should be unique is duplicated.
 * Created by jyie1 on 9/11/2015.
 */
public class DuplicateUIDException extends Exception {

    public DuplicateUIDException() {
        super();
    }

    public DuplicateUIDException(String message) {
        super(message);
    }

    public DuplicateUIDException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateUIDException(Throwable cause) {
        super(cause);
    }

    protected DuplicateUIDException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
