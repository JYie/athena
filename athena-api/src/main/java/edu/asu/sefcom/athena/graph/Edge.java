package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;

import java.util.Arrays;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public class Edge {

    private EdgeType type;
    private long weight;
    private Node[] nodes;

    /**
     * It constructs the edge.
     * @param type
     * @param weight
     */
    public Edge(EdgeType type, long weight, Node... nodes) throws InvalidParamException{
       if(weight < 0)
           throw new InvalidParamException("weigh must be positive");
        if(nodes.length <= 0)
            throw new InvalidParamException("the size of nodes must be greater than 1");
        switch(type){
            case SimpleDirected:
            case SimpleUndirected:
                if(2 != nodes.length)
                    throw new InvalidParamException("An edge of simple graph must contain 2 nodes");
        }
        int size = nodes.length;
        this.nodes = new Node[nodes.length];
        for(int i=0;i<size;i++){
            this.nodes[i] = nodes[i];
        }
        this.type = type;
    }

    public Edge(long weight, Node... nodes) throws InvalidParamException{
        this(EdgeType.SimpleUndirected, weight, nodes);
    }

    public Edge(EdgeType type, Node... nodes) throws InvalidParamException{
        this(type, 1, nodes);
    }

    public Edge(Node... nodes) throws InvalidParamException{
        this(EdgeType.SimpleUndirected, 1, nodes);
    }


    /**
     * It returns the weight of this edge.
     * @return
     */
    public long getWeight(){
        return this.weight;
    }


    /**
     * It returns the type of this edge.
     * @return
     */
    public EdgeType getType(){
        return this.type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (weight != edge.weight) return false;
        if (type != edge.type) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals

        if(type == EdgeType.SimpleDirected){
            return nodes[0].equals(edge.nodes[0]) && nodes[1].equals(edge.nodes[1]);
        }else {
            Set<Node> this_ = Sets.newHashSet(nodes);
            Set<Node> that = Sets.newHashSet(edge.nodes);

            return this_.containsAll(that) && that.containsAll(this_);
        }
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (int) (weight ^ (weight >>> 32));
        result = 31 * result + (nodes != null ? Arrays.hashCode(nodes) : 0);
        return result;
    }
}
