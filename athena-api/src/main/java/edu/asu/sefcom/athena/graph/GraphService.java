package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;

import java.util.Set;

/**
 * @author Jangwon Yie
 */
public interface GraphService {


    /**
     * It creates the node which containing the label delivered. Basically, a label can be used to distinguish graphs.
     * @param node
     */
    public void createNode(Label label, Node node);

    /**
     * It removes the node. If there exists edge which contains this node then it also removes all such edges.
     * @param node
     */
    public void removeNode(Node node);

    /**
     * It creates the edge. It also creates the nodes delivered if the node in the edge are not registered.
     * If this store does not support the edge type delivered, then it throws the InvalidStructureException.
     * @throws InvalidStructureException
     */
    public void addEdge(Edge edge) throws InvalidStructureException;

    /**
     * It removes the edge.
     * @param edge
     */
    public void removeEdge(Edge edge);

    /**
     * It returns the number of vertices of the graph having the label delivered.
     * @return
     */
    public int getOrder(Label label);

    /**
     * It returns the number of edges of the graph having the label delivered.
     * @return
     */
    public int getSize(Label label);

    /**
     * The hyper number means the number of nodes belongs to the single edge, i.e the hyper number of simple edge is 2.
     * It returns the hyper number of edge of the graph having the label delivered.
     * If the graph is not uniform, it returns the hyper number of edges which are majority of the graph having the
     * label delivered.
     * @param label
     * @return
     */
    public int getHyperNumber(Label label);

    /**
     * It returns edge which contains the node delivered.
     * @param node
     * @return
     */
    public Set<Edge> getNeighbors(Node node);

    /**
     * It returns the number of edges which contains the node delivered.
     * @param node
     * @return
     */
    public int degree(Node node);

    /**
     * It returns the set of labels.
     */
    public Set<Label> getLabels();

    /**
     * It returns the label of the graph which contains the node delivered.
     * @param node
     * @return
     */
    public Label getLabel(Node node);

}
