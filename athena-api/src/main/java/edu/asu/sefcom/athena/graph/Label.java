package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public interface Label {

    public String getLabel();

}
