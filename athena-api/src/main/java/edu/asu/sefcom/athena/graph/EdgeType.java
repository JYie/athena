package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 9/13/15.
 */
public enum EdgeType {

    SimpleDirected, SimpleUndirected, Hyper
}
