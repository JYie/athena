package edu.asu.sefcom.athena.similarity;

import edu.asu.sefcom.athena.graph.Label;

/**
 * @author - Jangwon Yie
 */
public interface GraphSimilarity {

    /**
     * It returns the similarity between two graphs delivered. The value is ranged from 0 to 1.
     * @param graph1
     * @param graph2
     * @return
     */
    public double measureSimilarity(Label graph1, Label graph2);
}
