package edu.asu.sefcom.athena.similarity;


import edu.asu.sefcom.athena.graph.Label;

/**
 * SimRank algorithm(http://www-cs-students.stanford.edu/~glenj/simrank.pdf) is the algorithm which is written by
 * Glen.Jeh who was the researcher in Stanford University.
 *
 * @author - Jangwon Yie
 */
public class SimRank implements GraphSimilarity {

    @Override
    public double measureSimilarity(Label graph1, Label graph2) {
        return 0;
    }
}
