package edu.asu.sefcom.athena.similarity;

/**
 * @author - Jangwon Yie
 */
public interface VertexSimilarity {

    /**
     * It returns the similarity between two vertices in the graph devliered. The number is ranged from 0 to 1.
     * @param node1
     * @param node2
     * @return
     */
    public double measureSimilarity(long node1, long node2);
}
