package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;

/**
 * Created by Jangwon Yie on 9/14/15.
 */
public interface GraphPropertyService {

    /**
     * It returns the density of the graph having the label delivered.
     * @param label
     * @return
     */
    public float getDensity(Label label);

    /**
     * It returns true if the graph having the label delivered is acyclic, returns false otherwise.
     * @return
     * @throws edu.asu.sefcom.athena.UnsupportedOperationException
     */
    public boolean isForest(Label label) throws UnsupportedOperationException;

    /**
     * It returns the minimum number of vertices removal of which makes the graph disconnect. For example, it returns 0
     * if the graph already disconnects.
     * @return
     * @throws UnsupportedOperationException
     */
    public int getConnectivity(Label label) throws UnsupportedOperationException;

    /**
     * It returns the number of components belongs to the graph having the label delivered. For instance, if the graph
     * is connected then it returns 1.
     * @param lable
     * @return
     * @throws UnsupportedOperationException
     */
    public int getComponents(Label lable) throws UnsupportedOperationException;


}
