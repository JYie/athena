package edu.asu.sefcom.athena.graph;

/**
 * Created by Jangwon Yie on 9/13/15.
 * @author Jangwon Yie
 */
public class Node {

    private long uid;

    public Node(long uid){
        this.uid = uid;
    }

    /**
     * It returns the unique id of this node.
     * @return
     */
    public long getId(){
        return this.uid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return uid == node.uid;
    }

    @Override
    public int hashCode() {
        return (int) (uid ^ (uid >>> 32));
    }
}
