package edu.asu.sefcom.athena.restful;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;
import com.google.api.client.util.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.asu.sefcom.athena.restful.response.StatusCode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 9/30/15.
 *
 * @author Jangwon Yie
 */
public class MainTest {

    private static int port = 8888;
    private static String serviceName = "Athena";
    private static String version = "1.0";
    private static String serviceUrl = "http://localhost:" + port + "/" + serviceName + "/" + version + "/";

    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private HttpRequestFactory requestFactory;

    @BeforeClass
    public static void beforeClass() {
        Main.start("-m", "test", "-s", serviceName, "-p", new Integer(port).toString());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUpBefore() {
        this.requestFactory =
                HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });
    }

    @Test
    public void testNodeLifecycle() {
        String id_request = "node1";
        AthenaUrl url = new AthenaUrl(serviceUrl + "test/node/" + id_request);
        try {
            Map<String, String> node1Properties = Maps.newHashMap();
            String expectedLink = "13";
            String expectedSize = "1251";
            node1Properties.put("outlink", expectedLink);
            node1Properties.put("length", expectedSize);
            HttpContent node1Content = new JsonHttpContent(new JacksonFactory(), node1Properties);
            HttpRequest request4Adding = requestFactory.buildPostRequest(url.setLength(expectedSize).setOutLink(expectedLink), node1Content);
            HttpResponse responseAdding = request4Adding.execute();
            int okAdding = responseAdding.getStatusCode();
            Assert.assertTrue(okAdding == StatusCode.getCode(StatusCode.Created));
            Node nodeFeed = responseAdding.parseAs(Node.class);
            Assert.assertTrue(null != nodeFeed && null != nodeFeed.name && nodeFeed.name.compareTo(id_request) == 0);
            Assert.assertTrue(null != nodeFeed.length && nodeFeed.length.compareTo(expectedSize) == 0);
            Assert.assertTrue(null != nodeFeed.outlink && nodeFeed.outlink.compareTo(expectedLink) == 0);

            HttpRequest request4AddingAgain = requestFactory.buildPostRequest(url, node1Content);
            HttpResponse responseAddingAgain = request4AddingAgain.execute();
            int conflictAdding = responseAddingAgain.getStatusCode();
            Assert.assertTrue(conflictAdding == StatusCode.getCode(StatusCode.Reset));

            HttpRequest request4Removing = requestFactory.buildDeleteRequest(url);
            HttpResponse responseRemoving = request4Removing.execute();
            int okRemoving = responseRemoving.getStatusCode();
            Assert.assertTrue(okRemoving == StatusCode.getCode(StatusCode.OK));

            HttpRequest request4RemovingAgain = requestFactory.buildDeleteRequest(url);
            HttpResponse responseRemovingAgain = request4RemovingAgain.execute();
            int againRemoving = responseRemovingAgain.getStatusCode();
            Assert.assertTrue(againRemoving == StatusCode.getCode(StatusCode.NoContent));

        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testEdgeLifecycle() {
        String id_node1 = "node11";
        String id_node2 = "node12";
        String id_node3 = "node13";

        AthenaUrl url4Node1 = new AthenaUrl(serviceUrl + "test/node/" + id_node1);
        AthenaUrl url4Node2 = new AthenaUrl(serviceUrl + "test/node/" + id_node2);
        AthenaUrl url4Node3 = new AthenaUrl(serviceUrl + "test/node/" + id_node3);

        AthenaUrl url4diEdge = new AthenaUrl(serviceUrl + "test/edge/directed/" + id_node1 + "/" + id_node2);
        AthenaUrl url4undiEdge = new AthenaUrl(serviceUrl + "test/edge/undirected/" + id_node2 + "/" + id_node3);

        AthenaUrl url4outNeighbors = new AthenaUrl(serviceUrl + "test/node/" + id_node1 + "/neighbors/OUT");
        AthenaUrl url4inNeighbors = new AthenaUrl(serviceUrl + "test/node/" + id_node2 + "/neighbors/IN");
        AthenaUrl url4undirectedNeighbors = new AthenaUrl(serviceUrl + "test/node/" + id_node2 + "/neighbors/UNDIRECTED");

        try {
            Map<String, String> empty = Maps.newHashMap();
            HttpContent emptyContent = new UrlEncodedContent(empty);
            HttpRequest request1 = requestFactory.buildPostRequest(url4Node1, emptyContent);
            HttpRequest request2 = requestFactory.buildPostRequest(url4Node2, emptyContent);
            HttpRequest request3 = requestFactory.buildPostRequest(url4Node3, emptyContent);

            Node node1 = request1.execute().parseAs(Node.class);
            Node node2 = request2.execute().parseAs(Node.class);
            Node node3 = request3.execute().parseAs(Node.class);

            Map<String, String> edge1Properties = Maps.newHashMap();
            Map<String, String> edge2Properties = Maps.newHashMap();

            HttpContent edge1Content = new UrlEncodedContent(edge1Properties);
            HttpContent edge2Content = new UrlEncodedContent(edge2Properties);

            HttpRequest requestEdge1 = requestFactory.buildPostRequest(url4diEdge, edge1Content);
            HttpRequest requestEdge2 = requestFactory.buildPostRequest(url4undiEdge, edge2Content);

            DirectedEdge edge1 = requestEdge1.execute().parseAs(DirectedEdge.class);
            UndirectedSimpleEdge edge2 = requestEdge2.execute().parseAs(UndirectedSimpleEdge.class);

            Assert.assertTrue(null != edge1 && null != edge1.head && edge1.head.compareTo(id_node1) == 0);
            Assert.assertTrue(null != edge1 && null != edge1.tail && edge1.tail.compareTo(id_node2) == 0);

            Set<String> expected = Sets.newHashSet(id_node2, id_node3);
            Set<String> actual = Sets.newHashSet(edge2.node1, edge2.node2);
            Assert.assertTrue(Sets.difference(expected, actual).isEmpty());

            HttpRequest request4 = requestFactory.buildGetRequest(url4outNeighbors);
            HttpRequest request5 = requestFactory.buildGetRequest(url4inNeighbors);
            HttpRequest request6 = requestFactory.buildGetRequest(url4undirectedNeighbors);

            String outString = request4.execute().parseAsString();
            String inString = request5.execute().parseAsString();
            String undirectedString = request6.execute().parseAsString();
            Gson gson = new Gson();
            List<Node> nodesOut = gson.fromJson(outString, new TypeToken<List<Node>>() {
            }.getType());
            List<Node> nodesIn = gson.fromJson(inString, new TypeToken<List<Node>>() {
            }.getType());
            List<Node> nodesUndirected = gson.fromJson(undirectedString, new TypeToken<List<Node>>() {
            }.getType());

            Assert.assertTrue(null != nodesOut && nodesOut.size() == 1 && nodesOut.get(0).name.compareTo(node2.name) == 0);
            Assert.assertTrue(null != nodesIn && nodesIn.size() == 1 && nodesIn.get(0).name.compareTo(node1.name) == 0);
            Assert.assertTrue(null != nodesUndirected && nodesUndirected.size() == 1 && nodesUndirected.get(0).name.compareTo(node3.name) == 0);

            HttpRequest request4Removing = requestFactory.buildDeleteRequest(url4diEdge);
            HttpResponse responseRemoving = request4Removing.execute();
            int okRemoving = responseRemoving.getStatusCode();
            Assert.assertTrue(okRemoving == StatusCode.getCode(StatusCode.OK));

            HttpRequest request4RemovingAgain = requestFactory.buildDeleteRequest(url4diEdge);
            HttpResponse responseRemovingAgain = request4RemovingAgain.execute();
            int againRemoving = responseRemovingAgain.getStatusCode();
            Assert.assertTrue(againRemoving == StatusCode.getCode(StatusCode.NoContent));

            outString = request4.execute().parseAsString();
            inString = request5.execute().parseAsString();

            gson = new Gson();
            nodesOut = gson.fromJson(outString, new TypeToken<List<Node>>() {
            }.getType());
            nodesIn = gson.fromJson(inString, new TypeToken<List<Node>>() {
            }.getType());

            Assert.assertTrue(null == nodesOut || nodesOut.isEmpty());
            Assert.assertTrue(null == nodesIn || nodesIn.isEmpty());

        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testSpectralFunctions(){
        String id_node1 = "node111";
        String id_node2 = "node112";
        String id_node3 = "node113";

        AthenaUrl url4Node1 = new AthenaUrl(serviceUrl + "test/node/" + id_node1);
        AthenaUrl url4Node2 = new AthenaUrl(serviceUrl + "test/node/" + id_node2);
        AthenaUrl url4Node3 = new AthenaUrl(serviceUrl + "test/node/" + id_node3);

        AthenaUrl url4diEdge12 = new AthenaUrl(serviceUrl + "test/edge/directed/" + id_node1 + "/" + id_node2);
        AthenaUrl url4diEdge23 = new AthenaUrl(serviceUrl + "test/edge/directed/" + id_node2 + "/" + id_node3);

        AthenaUrl url4AdjacenyMatrix = new AthenaUrl(serviceUrl + "test/spectral/adjacencyMatrix");
        AthenaUrl url4DegreeMatrix = new AthenaUrl(serviceUrl + "test/spectral/diagonalMatrix");
        AthenaUrl url4LaplacianMatrix = new AthenaUrl(serviceUrl + "test/spectral/laplacianMatrix");

        try {
            Map<String, String> empty = Maps.newHashMap();
            HttpContent emptyContent = new UrlEncodedContent(empty);
            HttpRequest request1 = requestFactory.buildPostRequest(url4Node1, emptyContent);
            HttpRequest request2 = requestFactory.buildPostRequest(url4Node2, emptyContent);
            HttpRequest request3 = requestFactory.buildPostRequest(url4Node3, emptyContent);

            Node node1 = request1.execute().parseAs(Node.class);
            Node node2 = request2.execute().parseAs(Node.class);
            Node node3 = request3.execute().parseAs(Node.class);

            Map<String, String> edge1Properties = Maps.newHashMap();
            Map<String, String> edge2Properties = Maps.newHashMap();

            HttpContent edge1Content = new UrlEncodedContent(edge1Properties);
            HttpContent edge2Content = new UrlEncodedContent(edge2Properties);

            HttpRequest requestEdge1 = requestFactory.buildPostRequest(url4diEdge12, edge1Content);
            HttpRequest requestEdge2 = requestFactory.buildPostRequest(url4diEdge23, edge2Content);

            requestEdge1.execute();
            requestEdge2.execute();

            HttpRequest request4 = requestFactory.buildGetRequest(url4AdjacenyMatrix);
            HttpRequest request5 = requestFactory.buildGetRequest(url4DegreeMatrix);
            HttpRequest request6 = requestFactory.buildGetRequest(url4LaplacianMatrix);

            String aMatrix = request4.execute().parseAsString();
            String dMatrix = request5.execute().parseAsString();
            String lMatrix = request6.execute().parseAsString();

            System.out.println("Adjacency Matrix : " + aMatrix);
            System.out.println("Diagonal Matrix : " + dMatrix);
            System.out.println("Laplacian Matrix : " + lMatrix);

        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    private static class AthenaUrl extends GenericUrl {
        public AthenaUrl(String encodedUrl) {
            super(encodedUrl);
        }

        @Key("outlink")
        private String outLink;
        @Key("length")
        private String length;


        public AthenaUrl setOutLink(String outLink) {
            this.outLink = outLink;
            return this;
        }

        public AthenaUrl setLength(String length) {
            this.length = length;
            return this;
        }
    }

    public static class Node {
        @Key
        public String name;
        @Key
        public String outlink;
        @Key
        public String length;
    }

    public static class DirectedEdge {

        @Key
        public String head;
        @Key
        public String tail;
    }

    public static class UndirectedSimpleEdge {

        @Key
        public String node1;
        @Key
        public String node2;
    }
}
