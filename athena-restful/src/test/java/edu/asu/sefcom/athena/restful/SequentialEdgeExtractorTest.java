package edu.asu.sefcom.athena.restful;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 10/7/2015.
 *
 * @author Jangwon Yie
 */
public class SequentialEdgeExtractorTest {

    private SequentialEdgeExtractor sequentialEdgeExtractor;
    private Graph graph;

    private Node node1;
    private Node node2;
    private Node node3;

    private Edge directed12;
    private Edge undirected23;

    @Before
    public void setUpBefore() {
        this.graph = new BlueGraph(new Label("test"));
        this.sequentialEdgeExtractor = new SequentialEdgeExtractor(this.graph);

        try {
            this.node1 = this.graph.addNode(null);
            this.node2 = this.graph.addNode(null);
            this.node3 = this.graph.addNode(null);

            Map<String, String> properties = Maps.newHashMap();
            properties.put("name", "testProperty");
            this.directed12 = this.graph.addDirectedEdge(node1.getUid(), node2.getUid(), properties);
            this.undirected23 = this.graph.addUndirectedEdge(node2.getUid(), node3.getUid(), null);
        } catch (UnavailabeEdgeException uee) {
            Assert.fail();
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (InvalidParamException ipe) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testFindingUndirectedEdgeId() {
        try {
            String undirected = this.sequentialEdgeExtractor.extractUndirectedEdgeId(this.node2.getUid(), this.node3.getUid());
            Assert.assertTrue(null != undirected && undirected.compareTo(this.undirected23.getUid()) == 0);

            String reversed = this.sequentialEdgeExtractor.extractUndirectedEdgeId(this.node3.getUid(), this.node2.getUid());
            Assert.assertTrue(null != reversed && reversed.compareTo(this.undirected23.getUid()) == 0);
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testFindingDirectedEdgeId() {
        try{
            String directed = this.sequentialEdgeExtractor.extractDirectedEdgeId(this.node1.getUid(), this.node2.getUid());
            Assert.assertTrue(null != directed && directed.compareTo(this.directed12.getUid()) == 0);

            String reversed = this.sequentialEdgeExtractor.extractDirectedEdgeId(this.node2.getUid(), this.node1.getUid());
            Assert.assertTrue(null == reversed || reversed.isEmpty());
        }catch (StorageException e){
            Assert.fail();
        }
    }
}
