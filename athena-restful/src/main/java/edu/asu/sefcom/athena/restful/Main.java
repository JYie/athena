package edu.asu.sefcom.athena.restful;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


import static spark.Spark.*;

/**
 * Created by Jangwon Yie on 9/30/15.
 *
 * @author Jangwon Yie
 */
public class Main {

    private static String confFile = "service.conf";

    private static final String defaultServiceName = "Athena";
    private static final String defaultVersion = "1.0";
    private static final int defaultPort = 7890;
    private static final Mode defaultMode = Mode.TEST;
    private static final String helpMessage;
    public static void main(String[] args) {
        start(args);
    }

    static{
        StringBuffer sb = new StringBuffer();
        sb.append("Usage: Main.class [-options] ");
        sb.append("\n\t (This is a Java Main class, so you should know how you execute Java Main class)");
        sb.append("\nwhere options include: ");
        sb.append("\n\t -s \t\t service name that this service contains");
        sb.append("\n\t -m \t\t mode, it could be either test, example, service");
        sb.append("\n\t -p \t\t port number that this service opens to listen to requests )");
        sb.append("\n\t -? -help -h \t print this help message");
        helpMessage = sb.toString();
    }

    static void start(String... args) {
        System.out.println("Athena Restful Service is being ignited");

        if(args.length != 0 && !handleArguments(args))
            return;

        loadConfiguration(null);
        initialize();
        System.out.println("Athena is successfully ignited at " + Config.getPort());
        port(Config.getPort());
        Service.startService(Config.getHost() + "/" + Config.getVersion());
        ExtendedService.startService(Config.getHost() + "/" + Config.getVersion());
    }

    private static boolean handleArguments(String[] args) {
        for (int i = 0; i < args.length; i = i + 2) {
            String key = args[i];
            String value;
            if(args.length > i+1)
                value = args[i + 1];
            else
                value = "";
            if (!key.startsWith("-")) {
                System.err.println("Invalid Arguments");
                return true;
            }
            key = key.substring(1);
            if (key.compareToIgnoreCase("p") == 0) {
                try {
                    int port_ = new Integer(value).intValue();
                    Config.setPort(port_);
                } catch (NumberFormatException nfe) {
                    System.err.println("port must be number");
                    System.err.println("default port(7890) is used.");
                }
            } else if (key.compareToIgnoreCase("m") == 0) {
                if (value.compareToIgnoreCase("test") == 0) {
                    Config.setMode(Mode.TEST);
                } else if (value.compareToIgnoreCase("service") == 0) {
                    Config.setMode(Mode.SERVICE);
                } else if (value.compareToIgnoreCase("example") == 0) {
                    Config.setExampleMode();
                } else {
                    System.err.println("Invalid Arguments");
                    return true;
                }
            } else if (key.compareToIgnoreCase("s") == 0) {
                Config.setHost(value);
            } else if (key.compareToIgnoreCase("h") == 0 || key.compareToIgnoreCase("?") == 0 ||
                    key.compareToIgnoreCase("help") == 0) {
                System.out.println(generateHelp());
                return false;
            } else {
                System.err.println("Unknown Arguments");
                return true;
            }
        }
        return true;
    }

    private static String generateHelp() {
        return helpMessage;
    }

    private static void loadConfiguration(String file) {
        Properties conf = new Properties();
        if (null == file || file.isEmpty())
            file = "service.conf";
        InputStream input = Main.class.getClassLoader().getResourceAsStream(file);
        if (null == input) {
            System.err.println("failure to load configuration");
            System.err.println("default configuration is loaded");
            loadDefaultConfiguration();
            return;
        }

        try {
            conf.load(input);
            input.close();
            Config.inherits(conf);
        } catch (IOException e) {
            System.err.println("failure to load configuration");
            System.err.println("default configuration is loaded");
            loadDefaultConfiguration();
        }
    }

    private static void loadDefaultConfiguration() {
        Config.setMode(Mode.TEST);
    }

    private static void initialize() {
        Mode current = Config.getServiceMode();
        if (null == current) {
            Config.setMode(defaultMode);
            current = defaultMode;
        }
        switch (current) {
            case SERVICE:
                System.out.println("SERVICE Mode is activated");
                Config.initStorage();
                break;
            case TEST:
                System.out.println("TEST Mode is activated");
                break;
            case EXAMPLE:
                System.out.println("EXAMLPLE Mode is activated");
                break;
            default:
                break;
        }

        if (null == Config.getHost() || Config.getHost().isEmpty())
            Config.setHost(defaultServiceName);
        if (null == Config.getVersion() || Config.getVersion().isEmpty())
            Config.setVersion(defaultVersion);
        if (0 == Config.getPort())
            Config.setPort(defaultPort);
    }
}
