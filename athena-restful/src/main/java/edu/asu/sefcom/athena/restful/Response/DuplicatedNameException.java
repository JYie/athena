package edu.asu.sefcom.athena.restful.response;

import edu.asu.sefcom.athena.restful.AthenaRestException;

/**
 * Created by Jangwon Yie on 10/7/15.
 */
public class DuplicatedNameException extends AthenaRestException {
    public DuplicatedNameException() {
        super();
    }

    public DuplicatedNameException(String message) {
        super(message);
    }

    public DuplicatedNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatedNameException(Throwable cause) {
        super(cause);
    }

    protected DuplicatedNameException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
