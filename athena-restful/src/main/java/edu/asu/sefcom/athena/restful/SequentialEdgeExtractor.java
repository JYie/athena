package edu.asu.sefcom.athena.restful;

import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.graph.EdgeExtractor;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/7/2015.
 * @author Jangwon Yie
 */
public class SequentialEdgeExtractor implements EdgeExtractor {

    private final Graph graph;

    public SequentialEdgeExtractor(Graph graph){
        this.graph = graph;
    }

    @Override
    public String extractUndirectedEdgeId(final String... nodes) throws StorageException {
        if(null == nodes || nodes.length < 2)
            return null;
        String node = nodes[0];
        Iterator<Edge> candidates = graph.getIncidentEdges(node, Direction.SIMPLEUNDIRECT, null, null);
        if(null == candidates)
            return null;

        while(candidates.hasNext()){
            Edge edge = candidates.next();
            Set<Node> undirects =edge.getNodes(Direction.SIMPLEUNDIRECT);
            if(null == undirects || undirects.isEmpty())
                continue;
            for(Node neighbor:undirects){
                if(neighbor.getUid().compareTo(nodes[1]) == 0)
                    return edge.getUid();
            }
        }
        return null;
    }

    @Override
    public String extractDirectedEdgeId(final String head, final String tail) throws StorageException {
        if(null == head || null == tail)
            return null;
        Iterator<Edge> candidates = graph.getIncidentEdges(head, Direction.SIMPLEOUT, null, null);
        if(null == candidates)
            return null;

        while(candidates.hasNext()){
            Edge edge = candidates.next();
            Set<Node> outs =edge.getNodes(Direction.SIMPLEOUT);
            if(null == outs || outs.isEmpty())
                continue;
            for(Node out:outs){
                if(out.getUid().compareTo(tail) == 0)
                    return edge.getUid();
            }
        }
        return null;
    }
}
