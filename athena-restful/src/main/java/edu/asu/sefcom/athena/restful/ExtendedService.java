package edu.asu.sefcom.athena.restful;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.mongodb.util.JSON;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.similarity.BasicSimRank;
import edu.asu.sefcom.athena.graph.similarity.MiniMaxSimRank;
import edu.asu.sefcom.athena.graph.similarity.SimRank;
import edu.asu.sefcom.athena.graph.similarity.SimRankResult;
import edu.asu.sefcom.athena.graph.spectral.ComposedSpectralGraph;
import edu.asu.sefcom.athena.graph.spectral.Matrix;
import edu.asu.sefcom.athena.restful.request.Parameter;
import edu.asu.sefcom.athena.restful.request.RequestParamHandler;
import edu.asu.sefcom.athena.restful.response.*;

import java.util.*;

import static spark.Spark.get;

/**
 * Created by Jangwon Yie on 10/14/2015.
 *
 * @author Jangwon Yie
 */
public class ExtendedService {

    private static int simrankStep;
    private static float simrankConstant;

    private static SimRank basicSimRank;
    private static SimRank miniMaxSimRank;

    private final static String simRankGeneral = "general";
    private final static String simRankMiniMax = "miniMax";

    static {
        Properties properties = Config.getExtraConfiguration();
        if (null != properties) {
            String step = properties.getProperty("simrankstep");
            try {
                simrankStep = new Integer(step).intValue();
            } catch (NumberFormatException ne) {
                simrankStep = 0;
            }
            String constant = properties.getProperty("simrankconstant");
            try {
                simrankConstant = new Float(constant).floatValue();
            } catch (NumberFormatException ne) {
                simrankConstant = 0;
            }
            if (simrankStep < 2)
                simrankStep = 0;
            if (simrankConstant <= 0 || simrankConstant >= 1)
                simrankConstant = 0;
        }
        init();
    }

    static void init() {
        basicSimRank = new BasicSimRank(simrankStep, simrankConstant, SimRank.Direction.PointedTo);
        miniMaxSimRank = new MiniMaxSimRank(simrankStep, simrankConstant, SimRank.Direction.PointedTo);
    }

    static void startService(String host) {
        get("/" + host + "/:label/simrank/:node1/:node2", (request, response) -> {

            String node1 = request.params(":node1");
            String node2 = request.params(":node2");

            Graph graph = Bridge.generateGraph(request);

            String id1 = Bridge.getIdfromName(graph, node1);
            String id2 = Bridge.getIdfromName(graph, node2);
            if (null == id1 || id1.isEmpty() || null == id2 || id2.isEmpty())
                throw new NoContentException("There is no node having delivered name");
            try {
                String type = RequestParamHandler.getQueryValue(request, Parameter.simranktype.name());
                double similarityValue = 0;
                if (type != null && type.compareToIgnoreCase(simRankMiniMax) == 0)
                    similarityValue = miniMaxSimRank.measureSimilarity(graph, id1, id2);
                else
                    similarityValue = basicSimRank.measureSimilarity(graph, id1, id2);

                SimRankResult simRankResult = new SimRankResult(similarityValue, node1, node2);
                if (type != null && type.compareToIgnoreCase(simRankMiniMax) == 0)
                    simRankResult.setStepMethod(simRankMiniMax);
                else
                    simRankResult.setStepMethod(simRankGeneral);

                response.status(StatusCode.getCode(StatusCode.OK));
                Gson gson = new GsonBuilder().serializeNulls().create();
                return gson.toJson(simRankResult);
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException use) {
                throw new InvalidModeException(use);
            }
        });

        get("/" + host + "/:label/simrank/:node", (request, response) -> {

            String nodeName = request.params(":node");
            Graph graph = Bridge.generateGraph(request);
            Set<String> params = RequestParamHandler.refinedQueryParams(request);

            Iterator<Node> nodes = null;
            if (null == params || params.size() == 0)
                nodes = graph.getNodes("", "");
            else {
                for (String key : params) {
                    String value = RequestParamHandler.getQueryValue(request, key);
                    nodes = graph.getNodes(key, value);
                }
            }

            List<SimRankResult> result = Lists.newArrayList();
            Gson gson = new GsonBuilder().serializeNulls().create();
            if (null == nodes) {
                return gson.toJson(result);
            }

            String id = Bridge.getIdfromName(graph, nodeName);

            if (null == id || id.isEmpty())
                throw new NoContentException("There is no node having delivered name");
            try {
                String type = RequestParamHandler.getQueryValue(request, Parameter.simranktype.name());
                while (nodes.hasNext()) {
                    Node node = nodes.next();
                    double similarityValue = 0;
                    if (type != null && type.compareToIgnoreCase(simRankMiniMax) == 0)
                        similarityValue = miniMaxSimRank.measureSimilarity(graph, id, node.getUid());
                    else
                        similarityValue = basicSimRank.measureSimilarity(graph, id, node.getUid());

                    if (similarityValue > 0 && similarityValue < 1) {
                        String name = (String) node.getProperty("name");
                        SimRankResult simRankResult = new SimRankResult(similarityValue, nodeName, name);
                        if (type != null && type.compareToIgnoreCase(simRankMiniMax) == 0)
                            simRankResult.setStepMethod(simRankMiniMax);
                        else
                            simRankResult.setStepMethod(simRankGeneral);
                        result.add(simRankResult);
                    }
                }

                Ordering<SimRankResult> ordering = new Ordering<SimRankResult>() {
                    @Override
                    public int compare(SimRankResult simRankResult, SimRankResult other) {
                        return simRankResult.getSimilarity() > other.getSimilarity() ? 1 : -1;
                    }
                };

                int direction = RequestParamHandler.getSortingDirection(request);
                if (direction > 0)
                    Collections.sort(result, ordering.reverse().nullsLast());
                else
                    Collections.sort(result, ordering.nullsLast());

                return gson.toJson(result);
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException use) {
                throw new InvalidModeException(use);
            }
        });

        get("/" + host + "/:label/spectral/adjacencyMatrix", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);

            if (null == graph)
                throw new NoContentException("Unregistered label");

            ComposedSpectralGraph csg = new ComposedSpectralGraph(graph);
            try{
                Matrix matrix = csg.generateAdjacencyMatrix();
                if (null == matrix)
                    throw new NoContentException("there is no vertex in the graph " + graph.getLabel().toString());

                Gson gson = new GsonBuilder().serializeNulls().create();
                return gson.toJson(matrix);
            }catch(UnsupportedOperationException use){
                throw new InvalidGraphException("It is not possible to generate adjacency matrix for the graph containing mixed type of edges ");
            }catch(StorageException se){
                throw new InternalException("Internal error is occured");
            }
        });

        get("/" + host + "/:label/spectral/laplacianMatrix", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);

            if (null == graph)
                throw new NoContentException("Unregistered label");

            ComposedSpectralGraph csg = new ComposedSpectralGraph(graph);
            Matrix matrix = csg.generateLaplacianMatrix();
            Gson gson = new GsonBuilder().serializeNulls().create();
            return gson.toJson(matrix);
        });

        get("/" + host + "/:label/spectral/diagonalMatrix", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);

            if (null == graph)
                throw new NoContentException("Unregistered label");

            ComposedSpectralGraph csg = new ComposedSpectralGraph(graph);
            Matrix matrix = csg.generateDiagonalDegreeMatrix();
            Gson gson = new GsonBuilder().serializeNulls().create();
            return gson.toJson(matrix);
        });
        ExceptionHandler.register();
    }
}
