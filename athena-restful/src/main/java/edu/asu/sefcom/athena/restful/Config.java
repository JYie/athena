package edu.asu.sefcom.athena.restful;

import java.util.Properties;

/**
 * Created by Jangwon Yie on 10/7/2015.
 * @author Jangwon Yie
 */
public class Config {

    private static Mode currentMode;
    private static String host;
    private static String version;
    private static int port = 0;
    private static Properties extras;

    static void setMode(Mode mode){
        currentMode = mode;
    }

    static void setExampleMode(){
        currentMode = Mode.EXAMPLE;
    }

    static void setHost(String name){
        host = name;
    }

    static void setVersion(String name){
        version = name;
    }

    static void setPort(int port_){
        port = port_;
    }

    static void initStorage(){

    }

    static Mode getServiceMode(){
        return currentMode;
    }

    static String getHost(){
        return host;
    }

    static String getVersion(){
        return version;
    }

    static int getPort(){
        return port;
    }

    static void inherits(Properties properties){
        extras = properties;
    }

    static Properties getExtraConfiguration(){
        return extras;
    }
}
