package edu.asu.sefcom.athena.restful;

/**
 * Created by Jangwon Yie on 9/30/15.
 * @author Jangwon Yie
 */
public enum Mode {
    TEST, SERVICE, EXAMPLE
}
