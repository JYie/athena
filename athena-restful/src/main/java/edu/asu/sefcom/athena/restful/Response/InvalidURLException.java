package edu.asu.sefcom.athena.restful.response;

import edu.asu.sefcom.athena.restful.AthenaRestException;

/**
 * Created by Jangwon Yie on 15. 10. 2..
 * @author Jangwon Yie
 */
public class InvalidURLException extends AthenaRestException {

    public InvalidURLException() {
        super();
    }

    public InvalidURLException(String message) {
        super(message);
    }

    public InvalidURLException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidURLException(Throwable cause) {
        super(cause);
    }

    protected InvalidURLException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
