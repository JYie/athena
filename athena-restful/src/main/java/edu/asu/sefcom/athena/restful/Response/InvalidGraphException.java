package edu.asu.sefcom.athena.restful.response;

import edu.asu.sefcom.athena.restful.AthenaRestException;

/**
 * Created by Jangwon Yie on 12/10/15.
 * @author Jangwon Yie
 */
public class InvalidGraphException extends AthenaRestException {

    public InvalidGraphException() {
        super();
    }

    public InvalidGraphException(String message) {
        super(message);
    }

    public InvalidGraphException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidGraphException(Throwable cause) {
        super(cause);
    }

    protected InvalidGraphException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
