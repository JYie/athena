package edu.asu.sefcom.athena.restful;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.*;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Node;

import edu.asu.sefcom.athena.restful.request.RequestParamHandler;
import edu.asu.sefcom.athena.restful.response.*;

import java.util.Map;
import java.util.Set;

import static edu.asu.sefcom.athena.restful.Transformer.*;
import static spark.Spark.*;

/**
 * Created by Jangwon Yie on 10/1/15.
 *
 * @author Jangwon Yie
 */
public class Service {

    static void startService(String host) {
        post("/" + host + "/:label/node/:name", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String name = RequestParamHandler.getValue(request, ":name");
            String id = Bridge.getIdfromName(graph, name);
            if (null != id && !id.isEmpty())
                throw new DuplicatedNameException("name is an unique identifier");

            Set<String> params = RequestParamHandler.refinedQueryParams(request);
            Map<String, String> properties = Maps.newHashMap();
            properties.put("name", name);
            for (String param : params) {
                String value = RequestParamHandler.getQueryValue(request, param);
                properties.put(RequestParamHandler.handleKeycaseSensitivity(param), value);
            }

            try {
                Node node = graph.addNode(properties);
                Gson gson = new GsonBuilder().serializeNulls().create();
                response.status(StatusCode.getCode(StatusCode.Created));
                return gson.toJson(transformElement(node));
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            } catch (edu.asu.sefcom.athena.InvalidParamException ipe){
                throw new InvalidPropertyException("there is an invalid property");
            }
        });


        delete("/" + host + "/:label/node/:name", (request, response) -> {
            String name = RequestParamHandler.getValue(request, ":name");

            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String id = Bridge.getIdfromName(graph, name);
            if (null == id || id.isEmpty())
                throw new NoContentException("There is no node having the name you delivered");
            try {
                graph.removeNode(id);
                response.status(StatusCode.getCode(StatusCode.OK));
                return "requested node is successfully deleted";
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            }
        });


        post("/" + host + "/:label/edge/undirected/:name1/:name2", (request, response) -> {
            String name1 = RequestParamHandler.getValue(request, ":name1");
            String name2 = RequestParamHandler.getValue(request, ":name2");

            if (null == name1 || null == name2)
                throw new InvalidParamException();

            Set<String> params = RequestParamHandler.refinedQueryParams(request);

            Map<String, String> properties = Maps.newHashMap();
            properties.put("node1", name1);
            properties.put("node2", name2);
            for (String param : params) {
                String value = RequestParamHandler.getQueryValue(request, param);
                properties.put(RequestParamHandler.handleKeycaseSensitivity(param), value);
            }

            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String node1Uid = Bridge.getIdfromName(graph, name1);
            String node2Uid = Bridge.getIdfromName(graph, name2);

            if (null == node1Uid || null == node2Uid)
                throw new InvalidParamException("two node ids are necessary to create an undirected edge");

            try {
                Edge edge = graph.addUndirectedEdge(node1Uid, node2Uid, properties);

                Gson gson = new GsonBuilder().serializeNulls().create();
                response.status(StatusCode.getCode(StatusCode.Created));
                return gson.toJson(transformEdge(edge));
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            } catch (edu.asu.sefcom.athena.InvalidParamException ipe){
                throw new InvalidPropertyException("there is an invalid property");
            }
        });

        delete("/" + host + "/:label/edge/undirected/:name1/:name2", (request, response) -> {
            String name1 = RequestParamHandler.getValue(request, ":name1");
            String name2 = RequestParamHandler.getValue(request, ":name2");

            if (null == name1 || null == name2)
                throw new InvalidParamException();

            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String node1Uid = Bridge.getIdfromName(graph, name1);
            String node2Uid = Bridge.getIdfromName(graph, name2);

            if (null == node1Uid || null == node2Uid)
                throw new InvalidParamException("two node ids are necessary to delete an undirected edge");

            EdgeExtractor edgeExtractor = new SequentialEdgeExtractor(graph);
            String edgeId = edgeExtractor.extractUndirectedEdgeId(node1Uid, node2Uid);

            if (null == edgeId || edgeId.isEmpty())
                throw new NoContentException("There is no edge containing nodes you delivered");
            try {
                graph.removeEdge(edgeId);
                response.status(StatusCode.getCode(StatusCode.OK));
                return "requested edge is successfully deleted";
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            }
        });

        post("/" + host + "/:label/edge/directed/:head/:tail", (request, response) -> {
            String head_name = RequestParamHandler.getValue(request, ":head");
            String tail_name = RequestParamHandler.getValue(request, ":tail");

            if (null == head_name || null == tail_name)
                throw new InvalidParamException();

            Set<String> params = RequestParamHandler.refinedQueryParams(request);

            Map<String, String> properties = Maps.newHashMap();
            for (String param : params) {
                String value = RequestParamHandler.getQueryValue(request, param);
                properties.put(RequestParamHandler.handleKeycaseSensitivity(param), value);
            }

            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String headUid = Bridge.getIdfromName(graph, head_name);
            String tailUid = Bridge.getIdfromName(graph, tail_name);

            if (null == headUid || null == tailUid)
                throw new InvalidParamException("head and tail ids are necessary to create a directed edge");

            try {
                Edge edge = graph.addDirectedEdge(headUid, tailUid, properties);
                if (null == edge)
                    throw new InternalException("Internal Error Occured");

                Gson gson = new GsonBuilder().serializeNulls().create();
                response.status(StatusCode.getCode(StatusCode.Created));
                return gson.toJson(transformEdge(edge));
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            } catch (edu.asu.sefcom.athena.InvalidParamException ipe){
                throw new InvalidPropertyException("there is an invalid property");
            }
        });

        delete("/" + host + "/:label/edge/directed/:head/:tail", (request, response) -> {
            String head_name = RequestParamHandler.getValue(request, ":head");
            String tail_name = RequestParamHandler.getValue(request, ":tail");

            if (null == head_name || null == tail_name)
                throw new InvalidParamException();

            Graph graph = Bridge.generateGraph(request);
            if (null == graph)
                throw new InternalException("Fails to instantiate the graph ");

            String headUid = Bridge.getIdfromName(graph, head_name);
            String tailUid = Bridge.getIdfromName(graph, tail_name);

            if (null == headUid || null == tailUid)
                throw new InvalidParamException("head, tail ids are necessary to delete an directed edge");

            EdgeExtractor edgeExtractor = new SequentialEdgeExtractor(graph);
            String edgeId = edgeExtractor.extractDirectedEdgeId(headUid, tailUid);

            if (null == edgeId || edgeId.isEmpty())
                throw new NoContentException("There is no edge containing nodes you delivered");
            try {
                graph.removeEdge(edgeId);
                response.status(StatusCode.getCode(StatusCode.OK));
                return "requested edge is successfully deleted";
            } catch (edu.asu.sefcom.athena.UnsupportedOperationException uoe) {
                throw new InvalidModeException(host + " is activated as readonly mode");
            }

        });

        get("/" + host + "/:label/node/:node/neighbors/OUT", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getNeighbors(graph, id, Direction.SIMPLEOUT);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        get("/" + host + "/:label/node/:node/neighbors/IN", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getNeighbors(graph, id, Direction.SIMPLEIN);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        get("/" + host + "/:label/node/:node/neighbors/BOTH", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getNeighbors(graph, id, Direction.SIMPLEBOTH);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;

        });

        get("/" + host + "/:label/node/:node/neighbors/UNDIRECTED", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getNeighbors(graph, id, Direction.SIMPLEUNDIRECT);

            response.status(StatusCode.getCode(StatusCode.OK));
            return result;

        });

        get("/" + host + "/:label/node/:node/incident/OUT", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getIncidentEdges(graph, node, Direction.SIMPLEOUT);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;

        });

        get("/" + host + "/:label/node/:node/incident/IN", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getIncidentEdges(graph, node, Direction.SIMPLEIN);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;

        });

        get("/" + host + "/:label/node/:node/incident/both", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getIncidentEdges(graph, node, Direction.SIMPLEBOTH);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        get("/" + host + "/:label/node/:node/incident/undirected", (request, response) -> {
            String node = RequestParamHandler.getValue(request, ":node");
            if (null == node)
                throw new InvalidParamException(":node is essential parameter");

            Graph graph = Bridge.generateGraph(request);
            String id = Bridge.getIdfromName(graph, node);
            if (null == id)
                throw new NoContentException("There is no node having delivered name");

            String result = Bridge.getIncidentEdges(graph, node, Direction.SIMPLEUNDIRECT);
            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        get("/" + host + "/:label/nodes", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);
            Set<String> params = RequestParamHandler.refinedQueryParams(request);

            String result = "";
            if (null == params || params.size() == 0)
                result = Bridge.getNodes(graph, "", "");

            for (String key : params) {
                String value = RequestParamHandler.getQueryValue(request, key);
                result = Bridge.getNodes(graph, RequestParamHandler.handleKeycaseSensitivity(key), value);
            }

            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        get("/" + host + "/:label/edges", (request, response) -> {
            Graph graph = Bridge.generateGraph(request);
            Set<String> params = RequestParamHandler.refinedQueryParams(request);

            String result = "";
            if (null == params || params.size() == 0)
                result = Bridge.getEdges(graph, "", "");

            for (String key : params) {
                String value = RequestParamHandler.getQueryValue(request, key);
                result = Bridge.getEdges(graph, RequestParamHandler.handleKeycaseSensitivity(key), value);
            }

            response.status(StatusCode.getCode(StatusCode.OK));
            return result;
        });

        ExceptionHandler.register();
    }
}
