package edu.asu.sefcom.athena.restful;

import edu.asu.sefcom.athena.AthenaException;

/**
 * Created by Jangwon Yie on 10/7/15.
 * @author Jangwon Yie
 */
public class AthenaRestException extends AthenaException {
    public AthenaRestException() {
        super();
    }

    public AthenaRestException(String message) {
        super(message);
    }

    public AthenaRestException(String message, Throwable cause) {
        super(message, cause);
    }

    public AthenaRestException(Throwable cause) {
        super(cause);
    }

    protected AthenaRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
