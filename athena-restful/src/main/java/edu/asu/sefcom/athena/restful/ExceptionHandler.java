package edu.asu.sefcom.athena.restful;

import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.restful.response.*;

import static spark.Spark.exception;

/**
 * Created by Jangwon Yie on 10/14/2015.
 * @author Jangwon Yie
 */
public class ExceptionHandler {

    static void register(){
        exception(InvalidModeException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.ServiceUnavailable));
            response.body(e.getMessage());
        });

        exception(InternalException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.InternalError));
            response.body(e.getMessage());
        });

        exception(InvalidParamException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.BadRequest));
            response.body(e.getMessage());
        });

        exception(NoContentException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.NoContent));
            response.body(e.getMessage());
        });

        exception(DuplicatedNameException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.Reset));
            response.body(e.getMessage());
        });

        exception(InvalidPropertyException.class, (e, request, response) -> {
            response.status(StatusCode.getCode(StatusCode.BadRequest));
            response.body(e.getMessage());
        });

    }
}
