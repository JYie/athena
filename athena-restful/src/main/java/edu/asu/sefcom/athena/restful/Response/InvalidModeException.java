package edu.asu.sefcom.athena.restful.response;

import edu.asu.sefcom.athena.restful.AthenaRestException;

/**
 * Created by Jangwon Yie on 10/14/2015.
 * @author Jangwon Yie
 */
public class InvalidModeException extends AthenaRestException {
    public InvalidModeException() {
        super();
    }

    public InvalidModeException(String message) {
        super(message);
    }

    public InvalidModeException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidModeException(Throwable cause) {
        super(cause);
    }

    protected InvalidModeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
