package edu.asu.sefcom.athena.restful;

import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.examples.ExamplesFactory;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphFactory;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueFactory;
import edu.asu.sefcom.athena.graph.mongo.MongoFactory;
import edu.asu.sefcom.athena.restful.request.RequestParamHandler;
import edu.asu.sefcom.athena.restful.response.InternalException;
import edu.asu.sefcom.athena.restful.response.InvalidURLException;
import spark.Request;

import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static edu.asu.sefcom.athena.restful.Transformer.transformEdge;
import static edu.asu.sefcom.athena.restful.Transformer.transformElement;

/**
 * Created by Jangwon Yie on 10/14/2015.
 *
 * @author Jangwon Yie
 */
public class Bridge {

    private static GraphFactory graphFactory;

    /**
     * as of now, only implementation of the graphfactory is bluefactory.
     */
    static {
        Properties extraConf = Config.getExtraConfiguration();

        if (Config.getServiceMode() == Mode.EXAMPLE)
            initFactoryForExampleMode(extraConf);
        else if (Config.getServiceMode() == Mode.TEST)
            initFactoryForTestMode();
        else if (Config.getServiceMode() == Mode.SERVICE)
            initFactoryForServiceMode(extraConf);
        else
            initFactoryForTestMode();
    }

    static void initFactoryForExampleMode(Properties properties) {
        if (null != properties) {
            graphFactory = new ExamplesFactory(properties);
        } else
            graphFactory = new ExamplesFactory();
    }

    static void initFactoryForTestMode() {
        graphFactory = new BlueFactory();
    }

    static void initFactoryForServiceMode(Properties properties) {
        try {
            graphFactory = new MongoFactory(properties);
        } catch (UnknownHostException e) {
            graphFactory = new BlueFactory();
        }
    }

    static Graph generateGraph(Request request) throws InvalidURLException {
        Label label = RequestParamHandler.getLabel(request);
        return graphFactory.createGraph(label);
    }

    static String getNeighbors(Graph graph, String nodeId, Direction direction) throws AthenaRestException {
        Iterator<Node> neighbors = null;
        try {
            neighbors = graph.getNeighbors(nodeId, direction, null, null);
        } catch (StorageException e) {
            throw new InternalException("Storage does not work");
        }

        Gson gson = new GsonBuilder().serializeNulls().create();
        Set<Map<String, String>> result = Sets.newHashSet();

        if (null == neighbors)
            return gson.toJson(result);

        while (neighbors.hasNext())
            result.add(transformElement(neighbors.next()));

        return gson.toJson(result);
    }

    static String getIncidentEdges(Graph graph, String nodeId, Direction direction) throws AthenaRestException {
        Iterator<Edge> incidentEdges = null;
        try {
            incidentEdges = graph.getIncidentEdges(nodeId, direction, null, null);
        } catch (StorageException e) {
            throw new InternalException("Storage does not work");
        }

        Gson gson = new GsonBuilder().serializeNulls().create();
        Set<Map<String, String>> result = Sets.newHashSet();

        if (null == incidentEdges)
            return gson.toJson(result);

        while (incidentEdges.hasNext()) {
            switch (direction) {
                case SIMPLEBOTH:
                case SIMPLEIN:
                case SIMPLEOUT:
                    result.add(transformEdge(incidentEdges.next()));
                    continue;
                case SIMPLEUNDIRECT:
                    result.add(transformEdge(incidentEdges.next()));
                    continue;
            }
            result.add(transformElement(incidentEdges.next()));
        }

        return gson.toJson(result);
    }

    static String getNodes(Graph graph, String key, String value) throws AthenaRestException {
        Iterator<Node> nodes = null;
        try {
            nodes = graph.getNodes(key, value);
        } catch (StorageException e) {
            throw new InternalException("Storage does not work");
        }

        Gson gson = new GsonBuilder().serializeNulls().create();
        Set<Map<String, String>> result = Sets.newHashSet();

        if (null == nodes)
            return gson.toJson(result);

        while (nodes.hasNext())
            result.add(transformElement(nodes.next()));

        return gson.toJson(result);
    }

    static String getEdges(Graph graph, String key, String value) throws AthenaRestException {
        Iterator<Edge> edges = null;
        try {
            edges = graph.getEdges(key, value);
        } catch (StorageException e) {
            throw new InternalException("Storage does not work");
        }

        Gson gson = new GsonBuilder().serializeNulls().create();
        Set<Map<String, String>> result = Sets.newHashSet();

        if (null == edges)
            return gson.toJson(result);

        while (edges.hasNext())
            result.add(transformEdge(edges.next()));

        return gson.toJson(result);
    }

    static String getIdfromName(Graph graph, String name) throws AthenaRestException {
        Iterator<Node> nodes = null;
        try {
            nodes = graph.getNodes("name", name);
        } catch (StorageException e) {
            throw new InternalException("Storage does not work");
        }

        if (null == nodes)
            return null;

        Node node = null;
        while (nodes.hasNext())
            node = nodes.next();

        if (null == node)
            return null;
        return node.getUid();
    }
}