package edu.asu.sefcom.athena.restful.response;

import edu.asu.sefcom.athena.restful.AthenaRestException;

/**
 * Created by Jangwon Yie on 10/29/2015.
 * @author Jangwon Yie
 */
public class InvalidPropertyException extends AthenaRestException {
    public InvalidPropertyException() {
    }

    public InvalidPropertyException(String message) {
        super(message);
    }

    public InvalidPropertyException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidPropertyException(Throwable cause) {
        super(cause);
    }

    public InvalidPropertyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
