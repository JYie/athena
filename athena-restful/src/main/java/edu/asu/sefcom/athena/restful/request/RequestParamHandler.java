package edu.asu.sefcom.athena.restful.request;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.graph.GraphStoreType;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.restful.response.InvalidURLException;
import spark.Request;

import java.util.Set;

/**
 * Created by Jangwon Yie on 10/15/2015.
 * @author Jangwon Yie
 */
public class RequestParamHandler {

    private static Set<String> reserved;

    static{
        reserved = Sets.newLinkedHashSet();
        reserved.add(Parameter.limit.name());
        reserved.add(Parameter.offSet.name());
        reserved.add(Parameter.sortingOrder.name());
        reserved.add(Parameter.type.name());
        reserved.add(Parameter.simranktype.name());
    }

    public static GraphStoreType getType(Request request) {
        String[] value = request.queryParamsValues(Parameter.type.name());
        if (null == value || value.length == 0)
            return GraphStoreType.Memory;
        if (value[0].compareToIgnoreCase("Memory") == 0)
            return GraphStoreType.Memory;
        if (value[0].compareToIgnoreCase("Neo4j") == 0)
            return GraphStoreType.Neo4j;
        if (value[0].compareToIgnoreCase("Neo4jEmbed") == 0)
            return GraphStoreType.Neo4JEmbed;
        if (value[0].compareToIgnoreCase("Rexster") == 0)
            return GraphStoreType.Rexster;
        return GraphStoreType.Memory;
    }

    /**
     * descening order is default, so it returns positive number when the request does contain such information. Only
     * the case that it returns negative is that the request contains parameter value "asecending".
     * @param request
     * @return
     */
    public static int getSortingDirection(Request request){
        String[] value = request.queryParamsValues(Parameter.sortingOrder.name());
        if(null == value || value.length == 0)
            return 1;
        if(value[0].compareToIgnoreCase("ascending") == 0)
            return -1;
        return 1;
    }

    public static Label getLabel(Request request) throws InvalidURLException {
        String label = request.params(":label");
        if (null == label)
            throw new InvalidURLException("label is essential");
        else
            return new Label(label);
    }

    public static String getValue(Request request, String key){
        return handleValuecaseSensitivity(request.params(key));
    }

    public static String getQueryValue(Request request, String key){
        return handleValuecaseSensitivity(request.queryParams(key));
    }

    public static Set<String> refinedQueryParams(Request request){
        Set<String> refined = Sets.newHashSet(request.queryParams());
        refined.removeAll(reserved);
        return refined;
    }

    public static Set<String> rawQueryParams(Request request){
        return Sets.newHashSet(request.queryParams());
    }

    public static Set<String> getReserved(){
        return reserved;
    }

    public static String handleKeycaseSensitivity(String origin){
        return null!=origin?origin.toLowerCase():"";
    }

    public static String handleValuecaseSensitivity(String origin){
        return origin;
    }
}
