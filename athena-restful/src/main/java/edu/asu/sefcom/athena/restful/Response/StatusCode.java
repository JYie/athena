package edu.asu.sefcom.athena.restful.response;

/**
 * Created by Jangwon Yie on 10/7/15.
 * @author Jangwon Yie
 */
public enum StatusCode {
    OK, Created, NoContent, NotModified,BadRequest, Unauthorized, Forbidden, NotFound, NotAllowedMethod, Reset,
    InternalError, ServiceUnavailable;


    public static int getCode(StatusCode status){
        switch (status){
            case OK:
                return 200;
            case Created:
                return 201;
            case NoContent:
                return 204;
            case Reset:
                return 205;
            case BadRequest:
                return 400;
            case NotModified:
                return 304;
            case Unauthorized:
                return 401;
            case Forbidden:
                return 403;
            case NotFound:
                return 404;
            case NotAllowedMethod:
                return 405;
            case InternalError:
                return 500;
            case ServiceUnavailable:
                return 503;
            default:
                return 500;
        }
    }
}

