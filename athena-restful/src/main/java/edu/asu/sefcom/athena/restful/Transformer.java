package edu.asu.sefcom.athena.restful;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.graph.element.*;

import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 15. 10. 2..
 * @author Jangwon Yie
 */
public class Transformer {

    static Map<String, String> transformElement(Element element){
        Map<String, String> map = Maps.newHashMap();
//        map.put("id", element.getUid());
        Set<String> properties = element.getPropertyKeys();
        if(null == properties || properties.size() == 0)
            return map;
        for(String property:properties)
            map.put(property, (String)element.getProperty(property));
        return  map;
    }

    static Map<String, String> transformEdge(Edge edge){
        EdgeType type = edge.getType();
        switch (type){
            case SimpleUndirected:
                return transformUndirected(edge);
            case SimpleDirected:
                return transformDirected(edge);
            case Hyper:
            default:
                return null;
        }
    }

    private static Map<String, String> transformDirected(Edge edge){
        Map<String, String> properties = transformElement(edge);
        Set<Node> head = edge.getNodes(Direction.SIMPLEIN);
        if(null != head){
            for(Node node:head)
                properties.put("head",(String)node.getProperty("name"));
        }
        Set<Node> tail = edge.getNodes(Direction.SIMPLEOUT);
        if(null != tail){
            for(Node node:tail)
                properties.put("tail",(String)node.getProperty("name"));
        }
        return properties;
    }

    private static Map<String, String> transformUndirected(Edge edge){
        Map<String, String> properties = transformElement(edge);
        Set<Node> nodes = edge.getNodes(Direction.SIMPLEUNDIRECT);
        if(null != nodes){
            int i=1;
            for(Node node:nodes) {
                properties.put("node"+i, (String)node.getProperty("name"));
                i++;
            }
        }
        return properties;
    }

}
