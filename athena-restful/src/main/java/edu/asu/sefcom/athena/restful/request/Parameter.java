package edu.asu.sefcom.athena.restful.request;

/**
 * Created by Jangwon Yie on 10/15/2015.
 * @author Jangwon Yie
 */
public enum Parameter {
    sortingOrder, offSet, limit, type, simranktype
}
