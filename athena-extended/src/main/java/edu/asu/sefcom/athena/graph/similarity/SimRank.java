package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/13/15.
 * @author Jangwon Yie
 */
public abstract class SimRank implements NodeSimilarity {

    public static enum Direction{
        PointsTo, PointedTo, bothWay, Undirected
    }

    private static final float defaultConstant = (float) 0.8;
    private static final int defaultSteps = 10;

    final double constant;
    final int steps;

    SimRank(int steps, float constant){
        if(constant > 0)
            this.constant = constant;
        else
            this.constant = defaultConstant;

        if(steps > 0)
            this.steps = steps;
        else
            this.steps = defaultSteps;


    }

    SimRank(float constant){
        this(defaultSteps, constant);
    }

    SimRank(int steps){
        this(steps, defaultConstant);
    }

    SimRank(){
        this(defaultSteps, defaultConstant);
    }

    @Override
    public double measureSimilarity(Graph graph, String... ids) throws InvalidParamException, edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {
        if(ids.length < 2)
            throw new InvalidParamException("ids must be greater than 1");

        if(ids.length > 2)
            throw new UnsupportedOperationException("Simrank can only measure the similarity between two nodes");

        return calculateSimrankSimilarity(graph, ids[0], ids[1]);
    }

    double stepFunction(Graph graph, int step, String a, String b) throws UnsupportedOperationException, StorageException {
        if (step == 0)
            return zeroStep(a, b);

        if (null == a || null == b || a.isEmpty() || b.isEmpty())
            return 0;

        if (a.compareTo(b) == 0)
            return 1;

        Set<Node> inA = getNexts(graph, a);
        Set<Node> inB = getNexts(graph, b);

        if (null == inA || inA.size() == 0 || null == inB || inB.size() == 0)
            return 0;

        double denominator = (double) (this.constant / (inA.size() * inB.size()));
        double nominator = (double) 0;
        for (Node nodeA : inA) {
            for (Node nodeB : inB) {
                nominator += stepFunction(graph, step - 1, nodeA.getUid(), nodeB.getUid());
            }
        }

        return nominator * denominator;
    }

    double zeroStep(String a, String b){
        if(a.compareTo(b) == 0)
            return 1;
        else
            return 0;
    }

    abstract double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException;

    abstract Set<Node> getNexts(Graph graph, String node) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException;

    abstract Direction getDirection();
}
