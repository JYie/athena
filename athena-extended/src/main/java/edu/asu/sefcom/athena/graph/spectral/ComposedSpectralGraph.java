package edu.asu.sefcom.athena.graph.spectral;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.element.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 12/10/15.
 *
 * @author Jangwon Yie
 */
public class ComposedSpectralGraph implements SpectralGraph {

    final private Graph graph;
    private Map<Node, Integer> nodeIndex;

    public ComposedSpectralGraph(Graph graph) {
        this.graph = graph;
    }

    @Override
    public Matrix generateAdjacencyMatrix() throws UnsupportedOperationException, StorageException {
        GraphType type = checkType();
        Iterator<Node> nodes = getNodes(null, null);
        if (null == nodes)
            return null;

        if (null == this.nodeIndex)
            this.nodeIndex = generateIndex(nodes);
        int size = this.nodeIndex.size();

        double[][] rawMatrix = new double[size][size];

        Iterator<Node> nodes_ = getNodes(null, null);
        while (nodes_.hasNext()) {
            Node node = nodes_.next();
            Iterator<Edge> incidentEdges = null;
            switch (type) {
                case UNDIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEUNDIRECT, null, null);
                    break;
                case DIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEOUT, null, null);
                    break;
            }

            if (null == incidentEdges)
                continue;

            int index = this.nodeIndex.get(node);
            while (incidentEdges.hasNext()) {
                Edge incidentEdge = incidentEdges.next();
                WeightedEdge weightedEdge = null;
                if (incidentEdge instanceof WeightedEdge)
                    weightedEdge = (WeightedEdge) incidentEdge;

                Set<Node> neighbors = incidentEdge.getNodes(Direction.SIMPLEUNDIRECT);
                if (null == neighbors)
                    continue;
                for (Node neighbor : neighbors) {
                    if (neighbor.equals(node))
                        continue;
                    int neighborIndex = this.nodeIndex.get(neighbor);
                    rawMatrix[index][neighborIndex] = null != weightedEdge ? weightedEdge.getWeight() : 1.0;
                }
            }
        }

        try {
            if(null == rawMatrix || rawMatrix.length == 0)
                return null;
            return new JBLASMatrix(rawMatrix);
        } catch (InvalidParamException e) {
            assert false;
            return null;
        }
    }

    @Override
    public Matrix generateDiagonalDegreeMatrix() throws UnsupportedOperationException, StorageException {
        GraphType type = checkType();
        Iterator<Node> nodes = getNodes(null, null);
        if (null == nodes)
            return null;

        if (null == this.nodeIndex)
            this.nodeIndex = generateIndex(nodes);
        int size = this.nodeIndex.size();

        double[][] rawMatrix = new double[size][size];

        Iterator<Node> nodes_ = getNodes(null, null);
        while (nodes_.hasNext()) {
            Node node = nodes_.next();
            Iterator<Edge> incidentEdges = null;
            switch (type) {
                case UNDIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEUNDIRECT, null, null);
                    break;
                case DIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEOUT, null, null);
                    break;
            }

            if (null == incidentEdges)
                continue;
            double degree = 0;
            int index = this.nodeIndex.get(node);
            while (incidentEdges.hasNext()) {
                Edge edge = incidentEdges.next();
                if (!(edge instanceof WeightedEdge)) {
                    degree = degree + 1.0;
                    continue;
                }
                WeightedEdge weightedEdge = (WeightedEdge) edge;
                degree = degree + weightedEdge.getWeight();
            }
            rawMatrix[index][index] = degree;
        }

        try {
            return new JBLASMatrix(rawMatrix);
        } catch (InvalidParamException e) {
            assert false;
            return null;
        }

    }

    @Override
    public Matrix generateLaplacianMatrix() throws UnsupportedOperationException, StorageException {
        Matrix W = generateAdjacencyMatrix();
        W.negative();
        Matrix D = generateDiagonalDegreeMatrix();
        try {
            D.add(W);
            return D;
        } catch (MatrixException e) {
            assert false;
            return null;
        }
    }

    @Override
    public Label getLabel() {
        return this.graph.getLabel();
    }

    @Override
    public Node addNode(Map<String, String> properties) throws InvalidParamException, UnsupportedOperationException, StorageException {
        return this.graph.addNode(properties);
    }

    @Override
    public void removeNode(String id) throws UnsupportedOperationException, StorageException {
        this.graph.removeNode(id);
    }

    @Override
    public Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException {
        return this.graph.addDirectedEdge(inNode, outNode, properties);
    }

    @Override
    public Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException {
        return this.graph.addUndirectedEdge(node1, node2, properties);
    }

    @Override
    public Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException {
        return this.graph.addHyperEdge(nodes, properties);
    }

    @Override
    public void removeEdge(String id) throws UnsupportedOperationException, StorageException {
        this.graph.removeEdge(id);
    }

    @Override
    public Edge getEdge(String id) throws StorageException {
        return this.graph.getEdge(id);
    }

    @Override
    public Iterator<Node> getNodes(String key, String value) throws StorageException {
        return this.graph.getNodes(key, value);
    }

    @Override
    public Node getNode(String id) throws StorageException {
        return this.graph.getNode(id);
    }

    @Override
    public Iterator<Edge> getEdges(String key, String value) throws StorageException {
        return this.graph.getEdges(key, value);
    }

    @Override
    public Iterator<Node> getNeighbors(String nodeId, Direction direction, String key, String value) throws StorageException {
        return this.graph.getNeighbors(nodeId, direction, key, value);
    }

    @Override
    public Iterator<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value) throws StorageException {
        return this.graph.getIncidentEdges(nodeId, direction, key, value);
    }

    @Override
    public GraphType getGraphType() {
        return this.graph.getGraphType();
    }

    private Map<Node, Integer> generateIndex(Iterator<Node> nodes) {
        Map<Node, Integer> indexedNodes = Maps.newHashMap();

        int i = 0;
        while (nodes.hasNext()) {
            Node node = nodes.next();
            indexedNodes.put(node, i);
            i++;
        }
        return indexedNodes;
    }

    private GraphType checkType() throws UnsupportedOperationException {
        GraphType type = getGraphType();
        if (null == type)
            return GraphType.UNDIRECTED;

        if (type == GraphType.MIXED || type == GraphType.HYPER)
            throw new UnsupportedOperationException("Requested matrix can be defined on simple undirected or directed graph only");

        return type;
    }
}
