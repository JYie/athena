package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Lists;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import edu.asu.sefcom.athena.graph.OffsetLimitHandler;
import edu.asu.sefcom.athena.graph.PaginatedGraph;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.PropertyRule;

import java.util.List;

/**
 * Actually, tinkerPop graph only supports iterable type as the return of search functions such as getNodes, getEdges,
 * etc. So it is not efficient to implement paginated function by using tinkerpop graph.
 * Created by Jangwon Yie on 10/9/15.
 * @author Jangwon Yie
 */
public class BluePaginatedGraph extends BlueGraph implements PaginatedGraph  {

    public BluePaginatedGraph(Label label, PropertyRule rule) {
        super(label, rule);
    }

    public BluePaginatedGraph(Label label) {
        super(label);
    }

    @Override
    public List<Node> getNodes(String key, String value, int offset, int limit) {
        List<Node> result = Lists.newLinkedList();

        if(!isValid(offset))
            return result;

        Iterable<Vertex> vertices;
        if(null == key || key.isEmpty() || null == value || value.isEmpty())
            vertices = this.blueGraph.getVertices();
        else
            vertices= this.blueGraph.getVertices(key,value);

        if(null == vertices)
            return result;

        OffsetLimitHandler<Vertex, Node> offsetLimitHandler = new OffsetLimitHandler<Vertex, Node>() {
            @Override
            public Node transform(Vertex vertex) {
                return transferVertexToNode(vertex);
            }
        };


        offsetLimitHandler.handle(vertices.iterator(), result, offset, limit);
        return result;
    }

    @Override
    public List<Edge> getEdges(String key, String value, int offset, int limit) {
        List<Edge> result = Lists.newLinkedList();

        if(!isValid(offset))
            return result;

        Iterable<com.tinkerpop.blueprints.Edge> blueEdges;
        if(null == key || key.isEmpty() || null == value || value.isEmpty())
            blueEdges = this.blueGraph.getEdges();
        else
            blueEdges= this.blueGraph.getEdges(key, value);

        if(null == blueEdges)
            return result;

        OffsetLimitHandler<com.tinkerpop.blueprints.Edge, Edge> offsetLimitHandler =
                new OffsetLimitHandler<com.tinkerpop.blueprints.Edge, Edge>() {
            @Override
            public Edge transform(com.tinkerpop.blueprints.Edge edge) {
                return transferBlueEdgeToEdge(edge);
            }
        };

        offsetLimitHandler.handle(blueEdges.iterator(), result, offset, limit);
        return result;
    }

    private boolean isValid(int offset){
        return offset >= 0;
    }

    private boolean isAdded(int offset, int cursor){
        if(cursor >= offset)
            return true;
        else
            return false;
    }



}
