package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.Property;
import org.apache.commons.math3.analysis.function.Exp;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

import java.util.Set;

/**
 * Created by Jangwon Yie on 10/28/15.
 * @author Jangwon Yie
 */
public class WeightedSimRank extends EvidenceBasedSimRank{

    public WeightedSimRank(SimRank simRank) {
        super(simRank);
    }

    @Override
    double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException {
        return stepFunction(graph, steps, id1, id2);
    }


    @Override
    double stepFunction(Graph graph, int step, String a, String b) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {
        if (step == 0)
            return zeroStep(a, b);

        if (null == a || null == b || a.isEmpty() || b.isEmpty())
            return 0;

        if (a.compareTo(b) == 0)
            return 1;

        Set<Edge> inA = getWeightedEdges(graph, a);
        Set<Edge> inB = getWeightedEdges(graph, b);

        if (null == inA || inA.size() == 0 || null == inB || inB.size() == 0)
            return 0;

        double denominator = this.constant * measureEvidence(graph, a, b);
        double nominator = (double) 0;
        for (Edge edgeA : inA) {
            Node i = getCorrespondNode(a, edgeA);
            for (Edge edgeB : inB) {
                Node j = getCorrespondNode(b, edgeB);
                nominator += getWeights(graph, a, edgeA) * getWeights(graph, b, edgeB)
                        * stepFunction(graph, step - 1, i.getUid(), j.getUid());
            }
        }

        return nominator * denominator;

    }

    double getWeights(Graph graph, String me, Edge edge) throws StorageException {
        Node i = getCorrespondNode(me, edge);
        return getSpread(graph, i.getUid())*getNormalizedWeight(graph, me, edge);
    }

    double getVariance(Graph graph, String i) throws StorageException {
        try {
            Set<Edge> edges = getWeightedEdges(graph, i);
            if(null == edges || edges.size() == 0)
                return 0;
            int size = edges.size();
            double[] doubles = new double[size];
            int j=0;
            for(Edge edge:edges){
                doubles[j] = extractWeight(edge);
                j++;
            }

            Variance variance = new Variance();
            return variance.evaluate(doubles);
        } catch (UnsupportedOperationException e) {
            return 0;
        }
    }

    double getSpread(Graph graph, String node) throws StorageException {
        Exp exp = new Exp();
        double variance = getVariance(graph, node);
        return exp.value((0 - variance));
    }

    double getNormalizedWeight(Graph graph, String me, Edge edge) throws StorageException {
        double nominator = extractWeight(edge);
        try {
            Set<Edge> another = getWeightedEdges(graph, me);
            double denominator = 0;
            for(Edge neighbor:another){
                denominator += extractWeight(neighbor);
            }
            return nominator/denominator;
        } catch (UnsupportedOperationException e) {
            return 0;
        }

    }

    Set<Edge> getWeightedEdges(Graph graph, String node) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {
        Direction direction = getDirection();
        Set<Edge> result = null;
        Set<Edge> temp;
        switch (direction){
            case PointedTo:
                temp = Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, null, null ));
                result = Sets.difference(temp, Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, Property.WEIGHT.name(), "0")));
                break;
            case PointsTo:
                temp = Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, null, null ));
                result = Sets.difference(temp, Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, Property.WEIGHT.name(), "0")));
                break;
            case bothWay:
                temp = Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, null, null));
                temp.addAll(Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, null, null)));
                Set<Edge> temp_ = Sets.difference(temp, Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, Property.WEIGHT.name(), "0")));
                result = Sets.difference(temp_, Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, Property.WEIGHT.name(), "0")));
                break;
            case Undirected:
                temp = Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT, null, null ));
                result = Sets.difference(temp, Sets.newHashSet(graph.getIncidentEdges(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT, Property.WEIGHT.name(), "0")));

                break;
        }
        return result;
    }

    private double extractWeight(Edge edge){
        String value = (String)edge.getProperty(Property.WEIGHT.name());
        if(null == value || value.isEmpty())
            return 0;
        try{
            return new Double(value).doubleValue();
        }catch (NumberFormatException nfe){
            return 0;
        }
    }

    private Node getCorrespondNode(String me, Edge edge){
        Direction direction = getDirection();
        Set<Node> result = null;
        switch (direction){
            case PointedTo:
                result = edge.getNodes(edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN);
                break;
            case PointsTo:
                result = edge.getNodes(edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT);
                break;
            case bothWay:
                result = edge.getNodes(edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN);
                result.addAll(edge.getNodes(edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT));
                break;
            case Undirected:
                result = edge.getNodes(edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT);
                break;
        }
        if(null == result)
            return null;

        for(Node node:result){
            if(node.getUid().compareTo(me) != 0)
                return node;
        }
        return null;
    }
}
