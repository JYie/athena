package edu.asu.sefcom.athena.graph.global;

import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.element.Clique;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Set;

/**
 * This interface defines the function that the function should be implemented through looking into the graph globally.
 * Created by Jangwon Yie on 9/14/15.
 * @author Jangwon Yie
 */
public interface GloballyAccessibleGraph extends Graph {

    /**
     * It returns the density of the graph.
     * @return
     */
    public float getDensity();

    /**
     * It returns true if the graph having the label delivered is acyclic, returns false otherwise.
     * @return
     * @throws edu.asu.sefcom.athena.UnsupportedOperationException
     */
    public boolean isForest() throws UnsupportedOperationException;

    /**
     * It returns the minimum number of vertices removal of which makes the graph disconnect. For example, it returns 0
     * if the graph already disconnects.
     * @return
     * @throws UnsupportedOperationException
     */
    public int getConnectivity() throws UnsupportedOperationException;

    /**
     * It returns the number of components belongs to the graph having the label delivered. For instance, if the graph
     * is connected then it returns 1.
     * @return
     * @throws UnsupportedOperationException
     */
    public int getComponents() throws UnsupportedOperationException;

    /**
     * It returns the maximum clique which contains the node delivered.
     * @param node
     * @return
     * @throws UnsupportedOperationException
     */
    public Clique getMaximumClique(Node node) throws UnsupportedOperationException;

    /**
     * It returns the maximum independent set which contains the node delivered.
     * @param node
     * @return
     * @throws UnsupportedOperationException
     */
    public Set<Node> getMaximumIndependentSet(Node node) throws UnsupportedOperationException;

    /**
     * It returns the SimRank(http://www-cs-students.stanford.edu/~glenj/simrank.pdf) value between two nodes delivered.
     * It must be called after calling computeSimrank first.
     * @param node1
     * @param node2
     * @return
     * @throws UnsupportedOperationException
     */
    public float getSimRank(String node1, String node2) throws UnsupportedOperationException;

    /**
     * It computes the A = (a_{ij}) = n*n matrix where V(G) = {a_1,...,a_n} and a_{ij} indicates the simrank value
     * between a_i and a_j.
     * @throws UnsupportedOperationException
     */
    public void computeSimrank() throws UnsupportedOperationException;
}
