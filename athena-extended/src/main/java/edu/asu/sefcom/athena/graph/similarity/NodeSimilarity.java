package edu.asu.sefcom.athena.graph.similarity;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.graph.Graph;

/**
 * Created by Jangwon Yie on 10/7/2015.
 * @author Jangwon Yie
 */
public interface NodeSimilarity {

    public double measureSimilarity(Graph graph, String... ids) throws InvalidParamException, edu.asu.sefcom.athena.UnsupportedOperationException, StorageException;

}
