package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * It supports evidence based simrank algorithm. This algorithm is described in the paper
 * "Simrank++: Query Rewriting through Link Analysis of the Click Graph" in the section
 * "REVISING SIMRANK"
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class EvidenceBasedSimRank extends SimRank{

    private SimRank simRank;

    public EvidenceBasedSimRank(SimRank simRank) {
        this.simRank = simRank;
    }

    @Override
    double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException {
        double originSimilarity = this.simRank.calculateSimrankSimilarity(graph, id1, id2);
        double evidence = measureEvidence(graph, id1, id2);
        return evidence * originSimilarity;
    }

    @Override
    Set<Node> getNexts(Graph graph, String node) throws UnsupportedOperationException, StorageException {
        return this.simRank.getNexts(graph, node);
    }

    @Override
    Direction getDirection() {
        return this.simRank.getDirection();
    }

    double measureEvidence(Graph graph, String id1, String id2) throws StorageException {
        Iterator<Node> neighbors1 = null;
        Iterator<Node> neighbors2 = null;

        SimRank.Direction simrankDirection = this.simRank.getDirection();
        switch (simrankDirection){
            case PointedTo:
                neighbors1 = graph.getNeighbors(id1, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, null, null);
                neighbors2 = graph.getNeighbors(id2, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, null, null);
                break;
            case PointsTo:
                neighbors1 = graph.getNeighbors(id1, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, null, null);
                neighbors2 = graph.getNeighbors(id2, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, null, null);
                break;
            case bothWay:
                neighbors1 = graph.getNeighbors(id1, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEBOTH, null, null);
                neighbors2 = graph.getNeighbors(id2, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEBOTH, null, null);
                break;
            case Undirected:
                neighbors1 = graph.getNeighbors(id1, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT, null, null);
                neighbors2 = graph.getNeighbors(id2, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT, null, null);
                break;
        }
        if(null == neighbors1 || null == neighbors2)
            return 0;

        return getEvidence(Sets.intersection(Sets.newHashSet(neighbors1), Sets.newHashSet(neighbors2)).size());
    }

    private double getEvidence(int k){
        if(k <= 0)
            return 0;
        double evidence = 0;
        for(int i=1;i<=k;i++)
            evidence += Math.pow(0.5, i);

        return evidence;
    }
}
