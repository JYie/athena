package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.MultiPartiteGraph;
import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class BipartiteSimRank extends SimRank {

    private Direction direction;
    public static final Direction defaultDirection = Direction.Undirected;

    public BipartiteSimRank(int steps, float constant, Direction direction) {
        super(steps, constant);
        this.direction = direction;
    }

    public BipartiteSimRank(float constant, Direction direction) {
        super(constant);
        this.direction = direction;
    }

    public BipartiteSimRank(int steps, Direction direction) {
        super(steps);
        this.direction = direction;
    }

    public BipartiteSimRank(float constant) {
        super(constant);
        this.direction = defaultDirection;
    }

    public BipartiteSimRank(int steps) {
        super(steps);
        this.direction = defaultDirection;
    }

    public BipartiteSimRank() {
        super();
        this.direction = defaultDirection;
    }

    @Override
    double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException {
        checkBipartiteness(graph);
        MultiPartiteGraph mpg = (MultiPartiteGraph)graph;

        String partitionofA = mpg.getPartition(id1);
        String partitionofB = mpg.getPartition(id2);
        if(null == partitionofA || partitionofA.isEmpty() || null == partitionofB || partitionofB.isEmpty())
            return 0;

        if(partitionofA.compareTo(partitionofB) != 0)
            throw new UnsupportedOperationException("two nodes must be included in same partition");

        return stepFunction(graph, this.steps, id1, id2);
    }

    @Override
    Set<Node> getNexts(Graph graph, String node) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {

        Iterator<Node> neighbors;
        if(this.direction == Direction.bothWay)
            neighbors = graph.getNeighbors(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEBOTH, null, null);
        else if(this.direction == Direction.Undirected)
            neighbors = graph.getNeighbors(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEUNDIRECT, null, null);
        else
            throw new UnsupportedOperationException("Unknown direction for Simrank");

        if (null == neighbors)
            return null;

        Set<Node> result = Sets.newHashSet();
        while (neighbors.hasNext())
            result.add(neighbors.next());

        return result;
    }

    @Override
    Direction getDirection() {
        return this.direction;
    }

    private void checkBipartiteness(Graph graph) throws edu.asu.sefcom.athena.UnsupportedOperationException {
        if(!(graph instanceof MultiPartiteGraph))
            throw new UnsupportedOperationException("the graph must be multipartite graph.");

        MultiPartiteGraph mpg = (MultiPartiteGraph)graph;
        Set<String> partitions = mpg.getPartitionKeys();
        if(null == partitions || partitions.size() != 2)
            throw new UnsupportedOperationException("it only works for a bipartite graph.");
    }
}
