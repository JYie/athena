package edu.asu.sefcom.athena.graph.spectral;

import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;

/**
 * Created by Jangwon Yie on 11/4/2015.
 * @author Jangwon Yie
 */
public interface SpectralGraph extends Graph {

    public Matrix generateAdjacencyMatrix() throws UnsupportedOperationException, StorageException;

    public Matrix generateDiagonalDegreeMatrix() throws UnsupportedOperationException, StorageException;

    public Matrix generateLaplacianMatrix() throws UnsupportedOperationException, StorageException;
}
