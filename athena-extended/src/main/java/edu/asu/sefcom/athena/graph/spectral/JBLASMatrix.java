package edu.asu.sefcom.athena.graph.spectral;

import edu.asu.sefcom.athena.InvalidParamException;
import org.jblas.*;

/**
 * Created by Jangwon Yie on 11/4/2015.
 *
 * @author Jangwon Yie
 */
public class JBLASMatrix implements Matrix {

    private DoubleMatrix matrix;

    public JBLASMatrix(double[][] data) throws InvalidParamException {
        checkParam(data);
        this.matrix = new DoubleMatrix(data);
    }

    void checkParam(double[][] data) throws InvalidParamException {
        if(null == data)
            throw new InvalidParamException("data must not be null");
        int length = data.length;
        if(length == 0)
            throw new InvalidParamException("data must not be empty");
        int colLength = data[0].length;
        for (int i = 0; i < length; i++) {
            if (colLength != data[i].length)
                throw new InvalidParamException("length of each row must be consistent");
        }
    }

    @Override
    public double[][] getData() {
        return null != this.matrix ? this.matrix.toArray2() : null;
    }

    @Override
    public ComplexNumber<Double>[] getEigenValues() {
        if (null == this.matrix)
            return null;

        ComplexDoubleMatrix cdm = Eigen.eigenvalues(this.matrix);
        if (null == cdm || cdm.isEmpty())
            return null;

        int length = cdm.length;
        ComplexNumber<Double>[] result = new ComplexNumber[length];
        for (int i = 0; i < length; i++) {
            result[i] = new ComplexNumber<Double>(cdm.getReal(i), cdm.getImag(i));
        }

        return result;
    }

    @Override
    public void add(Matrix matrix) throws MatrixException {
        try {
            JBLASMatrix jblasMatrix = transform(matrix);
            if (this.matrix.rows != jblasMatrix.matrix.rows || this.matrix.columns != jblasMatrix.matrix.columns)
                throw new MatrixException("(i*j) + (i*j) is only possible.");
            this.matrix = this.matrix.add(jblasMatrix.matrix);
        } catch (InvalidParamException e) {
            throw new MatrixException(e.getMessage());
        }
    }

    @Override
    public void negative() {
        this.matrix = this.matrix.neg();
    }

    @Override
    public void multiply(Matrix matrix) throws MatrixException {
        try {
            JBLASMatrix jblasMatrix = transform(matrix);

            if (this.matrix.columns != jblasMatrix.matrix.rows)
                throw new MatrixException("only (i*j) * (j*k) is only possible");
            this.matrix = this.matrix.mmul(jblasMatrix.matrix);


        } catch (InvalidParamException e) {
            throw new MatrixException(e.getMessage());
        }
    }

    private JBLASMatrix transform(Matrix matrix) throws InvalidParamException {
        if (null == matrix)
            return null;

        if (matrix instanceof JBLASMatrix)
            return (JBLASMatrix) matrix;
        else {
            double[][] raw = matrix.getData();
            if (null == raw)
                return null;
            return new JBLASMatrix(raw);
        }
    }


    @Override
    public String toString() {
        return "JBLASMatrix{" + "matrix=" + matrix + '}';
    }
}
