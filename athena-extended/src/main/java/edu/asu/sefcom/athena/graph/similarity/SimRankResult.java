package edu.asu.sefcom.athena.graph.similarity;

/**
 * Created by Jangwon Yie on 2015. 10. 14..
 * @author Jangwon Yie
 */
public class SimRankResult{

    private String[] elements;
    private double similarity;
    private String stepMethod;

    public SimRankResult(double similarity, String... elements){
        this.similarity = similarity;
        this.elements = new String[elements.length];
        for(int i=0;i<elements.length;i++)
            this.elements[i] = elements[i];

    }

    public String[] getElements() {
        return elements;
    }

    public double getSimilarity() {
        return similarity;
    }

    public String getStepMethod() {
        return this.stepMethod;
    }

    public void setStepMethod(String stepMethod) {
        this.stepMethod = stepMethod;
    }
}
