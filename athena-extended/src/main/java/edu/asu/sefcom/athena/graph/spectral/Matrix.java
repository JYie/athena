package edu.asu.sefcom.athena.graph.spectral;

/**
 * Created by Jangwon Yie on 11/4/2015.
 * @author Jangwon Yie
 */
public interface Matrix {

    public double[][] getData();

    public ComplexNumber<Double>[] getEigenValues();

    public void add(Matrix matrix) throws MatrixException;

    public void negative();

    public void multiply(Matrix matrix) throws MatrixException;
}
