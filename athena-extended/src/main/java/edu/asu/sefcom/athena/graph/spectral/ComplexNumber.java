package edu.asu.sefcom.athena.graph.spectral;

/**
 * Created by Jangwon Yie on 11/4/2015.
 * @author Jangwon Yie
 */
public class ComplexNumber<T extends Number > {

    private T real;
    private T imaginary;

    public ComplexNumber(T real, T imaginary){
       this.real = real;
        this.imaginary = imaginary;
    }

    public T getReal() {
        return real;
    }

    public T getImaginary() {
        return imaginary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComplexNumber<?> that = (ComplexNumber<?>) o;

        if (!real.equals(that.real)) return false;
        return imaginary.equals(that.imaginary);

    }

    @Override
    public int hashCode() {
        int result = real.hashCode();
        result = 31 * result + imaginary.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ComplexNumber{" +
                "real=" + real +
                ", imaginary=" + imaginary +
                '}';
    }
}
