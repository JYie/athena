package edu.asu.sefcom.athena.graph;

import edu.asu.sefcom.athena.graph.element.Direction;
import edu.asu.sefcom.athena.graph.element.Edge;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.List;

/**
 * PaginatedGraph supports the paginated version of functions return of which has a form of iterator in the
 * graph interface.
 * Created by Jangwon Yie on 10/9/15.
 * @author Jangwon Yie
 */
public interface PaginatedGraph extends Graph {

    /**
     * It returns all nodes which contains property delivered. If parameters are null, it should return all nodes
     * in the graph.
     * @param key
     * @param value
     * @param offset : Specifies how many results to skip for the first returned result. Defaults to 0.
     * @param limit : Specifies how many resources should be returned.
     * @return
     */
    public List<Node> getNodes(String key, String value, int offset, int limit);

    /**
     * It returns all edges which contains property delivered. If parameters are null, it should return all edges
     * in the graph.
     * @param key
     * @param value
     * @param offset : Specifies how many results to skip for the first returned result. Defaults to 0.
     * @param limit : Specifies how many resources should be returned.
     * @return
     */
    public List<Edge> getEdges(String key, String value, int offset, int limit);

    /**
     * It returns the neighbor(meets direction) nodes of the node which contains the id delivered.
     * It reflects the current update.
     * @param nodeId
     * @param direction
     * @param key
     * @param value
     * @param offset : Specifies how many results to skip for the first returned result. Defaults to 0.
     * @param limit : Specifies how many resources should be returned.
     * @return
     */
//    public List<Node> getNeighbors(String nodeId, Direction direction, String key, String value, int offset, int limit);

    /**
     * It returns the edge which contains the node containing the id delivered and meets the property(key/value)
     * delivered. It reflects the current update.
     * @param nodeId
     * @param direction
     * @param key
     * @param value
     * @param offset : Specifies how many results to skip for the first returned result. Defaults to 0.
     * @param limit : Specifies how many resources should be returned.
     * @return
     */
//    public List<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value, int offset, int limit);
}
