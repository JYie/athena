package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.List;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/19/2015.
 *
 * @author Jangwon Yie
 */
public class MiniMaxSimRank extends BasicSimRank {

    public MiniMaxSimRank(int steps, float constant, Direction direction) {
        super(steps, constant, direction);
    }

    public MiniMaxSimRank(float constant, Direction direction) {
        super(constant, direction);
    }

    public MiniMaxSimRank(int steps, Direction direction) {
        super(steps, direction);
    }

    public MiniMaxSimRank(float constant) {
        super(constant);
    }

    public MiniMaxSimRank(int steps) {
        super(steps);
    }

    public MiniMaxSimRank() {
        super();
    }

    @Override
    double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException {
        double a = stepFunction(graph, this.steps, id1, id2);
        double b = stepFunction(graph, this.steps, id2, id1);

        return a<b?a:b;
    }

    @Override
    double stepFunction(Graph graph, int step, String a, String b) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {

        if (step == 0)
            return zeroStep(a, b);

        if (null == a || null == b || a.isEmpty() || b.isEmpty())
            return 0;

        if (a.compareTo(b) == 0)
            return 1;

        Set<Node> inA = getNexts(graph, a);
        Set<Node> inB = getNexts(graph, b);

        if (null == inA || inA.size() == 0 || null == inB || inB.size() == 0)
            return 0;

        double denominator = (double) (this.constant / (inA.size()));
        double nominator = (double) 0;
        List<Double> list = Lists.newLinkedList();
        Ordering<Double> ordering = new Ordering<Double>() {
            @Override
            public int compare(Double t0, Double t1) {
                if (null == t0 || null == t1)
                    return 1;
                if (t0.doubleValue() >= t1.doubleValue())
                    return 1;
                else
                    return -1;
            }
        };
        for (Node nodeA : inA) {
            for (Node nodeB : inB) {
                list.add(stepFunction(graph, step - 1, nodeA.getUid(), nodeB.getUid()));
            }
            nominator = ordering.max(list);
        }

        return nominator * denominator;
    }
}
