package edu.asu.sefcom.athena.graph;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Jangwon Yie on 10/10/15.
 * @author Jangwon Yie
 */
public abstract class OffsetLimitHandler<S,T>{

    public OffsetLimitHandler(){
    }

    public abstract T transform(S s);

    public void handle(Iterator<S> iterator,List<T> result, int offset, int limit){
        if(offset < 0)
            return;
        int cursor = 0;
        int added = 0;
        while(iterator.hasNext()) {
            S s = iterator.next();
            if(added >= limit)
                break;
            if(isAdded(offset, cursor)) {
                result.add(transform(s));
                added++;
            }
            cursor++;
        }
    }

    private boolean isValid(int offset){
        return offset >= 0;
    }

    private boolean isAdded(int offset, int cursor){
        if(cursor >= offset)
            return true;
        else
            return false;
    }
}
