package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.element.Node;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/19/2015.
 *
 * @author Jangwon Yie
 */
public class BasicSimRank extends SimRank {

    public static final Direction defaultDirection = Direction.PointedTo;

    private Direction direction;

    public BasicSimRank(int steps, float constant, Direction direction) {
        super(steps, constant);
        this.direction = direction;
    }

    public BasicSimRank(float constant, Direction direction) {
        super(constant);
        this.direction = direction;
    }

    public BasicSimRank(int steps, Direction direction) {
        super(steps);
        this.direction = direction;
    }

    public BasicSimRank(float constant) {
        super(constant);
        this.direction = defaultDirection;

    }

    public BasicSimRank(int steps) {
        super(steps);
        this.direction = defaultDirection;
    }

    public BasicSimRank() {
        super();
        this.direction = defaultDirection;
    }

    @Override
    double calculateSimrankSimilarity(Graph graph, String id1, String id2) throws UnsupportedOperationException, StorageException {
        return stepFunction(graph, this.steps, id1, id2);
    }

    @Override
    Set<Node> getNexts(Graph graph, String node) throws edu.asu.sefcom.athena.UnsupportedOperationException, StorageException {
        Iterator<Node> neighbors;
        if(this.direction == Direction.PointedTo)
            neighbors = graph.getNeighbors(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEIN, null, null);
        else if(this.direction == Direction.PointsTo)
            neighbors = graph.getNeighbors(node, edu.asu.sefcom.athena.graph.element.Direction.SIMPLEOUT, null, null);
        else
            throw new edu.asu.sefcom.athena.UnsupportedOperationException("Unknown direction for Simrank");

        if (null == neighbors)
            return null;

        Set<Node> result = Sets.newHashSet();
        while (neighbors.hasNext())
            result.add(neighbors.next());

        return result;
    }

    @Override
    Direction getDirection() {
        return this.direction;
    }
}