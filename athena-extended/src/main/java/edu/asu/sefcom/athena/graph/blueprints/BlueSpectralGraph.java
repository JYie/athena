package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.element.*;
import edu.asu.sefcom.athena.graph.spectral.JBLASMatrix;
import edu.asu.sefcom.athena.graph.spectral.Matrix;
import edu.asu.sefcom.athena.graph.spectral.MatrixException;
import edu.asu.sefcom.athena.graph.spectral.SpectralGraph;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 11/5/2015.
 * @author Jangwon Yie
 */
public class BlueSpectralGraph extends BlueGraph implements SpectralGraph {

    public BlueSpectralGraph(Label label, PropertyRule rule) {
        super(label, rule);
    }

    public BlueSpectralGraph(Label label) {
        super(label);
    }

    private  Map<Node, Integer> nodeIndex;

    @Override
    public Matrix generateAdjacencyMatrix() throws UnsupportedOperationException{
       GraphType type = checkType();
       Iterator<Node> nodes = getNodes(null,null);
        if(null == nodes)
            return null;

        if(null == this.nodeIndex)
            this.nodeIndex = generateIndex(nodes);
        int size = this.nodeIndex.size();

        double[][] rawMatrix = new double[size][size];

        Iterator<Node> nodes_ = getNodes(null, null);
        while(nodes_.hasNext()){
            Node node = nodes_.next();
            Iterator<Edge> incidentEdges = null;
            switch (type){
                case UNDIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEUNDIRECT, null, null);
                    break;
                case DIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEOUT, null, null);
                    break;
            }

            if(null == incidentEdges)
                continue;

            int index = this.nodeIndex.get(node);
            while(incidentEdges.hasNext()){
                Edge incidentEdge = incidentEdges.next();
                WeightedEdge weightedEdge = null;
                if(incidentEdge instanceof WeightedEdge)
                   weightedEdge = (WeightedEdge)incidentEdge;

                Set<Node> neighbors = incidentEdge.getNodes(Direction.SIMPLEUNDIRECT);
                if(null == neighbors)
                    continue;
                for(Node neighbor:neighbors){
                    if(neighbor.equals(node))
                        continue;
                    int neighborIndex = this.nodeIndex.get(neighbor);
                    rawMatrix[index][neighborIndex] = null != weightedEdge?weightedEdge.getWeight():1.0;
                }
            }
        }

        try {
            return new JBLASMatrix(rawMatrix);
        } catch (InvalidParamException e) {
            assert false;
            return null;
        }
    }

    @Override
    public Matrix generateDiagonalDegreeMatrix() throws UnsupportedOperationException{
        GraphType type = checkType();
         Iterator<Node> nodes = getNodes(null,null);
        if(null == nodes)
            return null;

        if(null == this.nodeIndex)
            this.nodeIndex = generateIndex(nodes);
        int size = this.nodeIndex.size();

        double[][] rawMatrix = new double[size][size];

        Iterator<Node> nodes_ = getNodes(null, null);
        while(nodes_.hasNext()){
            Node node = nodes_.next();
            Iterator<Edge> incidentEdges = null;
            switch (type){
                case UNDIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEUNDIRECT, null, null);
                    break;
                case DIRECTED:
                    incidentEdges = getIncidentEdges(node.getUid(), Direction.SIMPLEOUT, null, null);
                    break;
            }

            if(null == incidentEdges)
                continue;
            double degree = 0;
            int index = this.nodeIndex.get(node);
            while(incidentEdges.hasNext()){
                Edge edge = incidentEdges.next();
                if(!(edge instanceof WeightedEdge)) {
                    degree = degree + 1.0;
                    continue;
                }
                WeightedEdge weightedEdge = (WeightedEdge)edge;
                degree = degree + weightedEdge.getWeight();
            }
            rawMatrix[index][index] = degree;
        }

        try {
            return new JBLASMatrix(rawMatrix);
        } catch (InvalidParamException e) {
            assert false;
            return null;
        }
    }

    @Override
    public Matrix generateLaplacianMatrix() throws UnsupportedOperationException {
        Matrix W = generateAdjacencyMatrix();
        W.negative();
        Matrix D = generateDiagonalDegreeMatrix();
        try {
            D.add(W);
            return D;
        } catch (MatrixException e) {
            assert false;
            return null;
        }
    }

    private Map<Node, Integer> generateIndex(Iterator<Node> nodes){
        Map<Node, Integer> indexedNodes = Maps.newHashMap();

        int i = 0;
        while(nodes.hasNext()){
            Node node = nodes.next();
            indexedNodes.put(node, i);
            i++;
        }
        return indexedNodes;
    }

    private GraphType checkType() throws UnsupportedOperationException{
        GraphType type = getGraphType();
        if(null == type)
           return GraphType.UNDIRECTED;

        if(type == GraphType.MIXED || type == GraphType.HYPER)
            throw new UnsupportedOperationException("Requested matrix can be defined on simple undirected or directed graph only");

        return type;
    }
}
