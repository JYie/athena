package edu.asu.sefcom.athena.graph.spectral;

import edu.asu.sefcom.athena.AthenaException;

/**
 * Created by Jangwon Yie on 11/6/2015.
 * @author Jangwon Yie
 */
public class MatrixException extends AthenaException{
    public MatrixException() {
    }

    public MatrixException(String message) {
        super(message);
    }

    public MatrixException(String message, Throwable cause) {
        super(message, cause);
    }

    public MatrixException(Throwable cause) {
        super(cause);
    }

    public MatrixException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
