package edu.asu.sefcom.athena.graph.spectral;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Jangwon Yie on 11/4/2015.
 *
 * @author Jangwon Yie
 */
public class JBLASMatrixTest {

    @Test(expected = InvalidParamException.class)
    public void testEmpty() throws InvalidParamException {
        double[][] raw = new double[][]{};

        JBLASMatrix matrix = new JBLASMatrix(raw);
        Assert.fail();
    }

    @Test(expected = InvalidParamException.class)
    public void testDataValidation() throws InvalidParamException {

        double[][] raw1 = new double[][]{{2, 0, 0}, {0, 3}, {0, 4, 9}};
        JBLASMatrix matrix = new JBLASMatrix(raw1);

        Assert.fail();
    }

    @Test
    public void testEigenValue() {
        double[][] raw1 = new double[][]{{2, 0, 0}, {0, 3, 4}, {0, 4, 9}};

        JBLASMatrix matrix = null;
        try {
            matrix = new JBLASMatrix(raw1);
        } catch (InvalidParamException e) {
            Assert.fail();
        }

        ComplexNumber<Double> a = new ComplexNumber<Double>(11.0, 0.0);
        ComplexNumber<Double> b = new ComplexNumber<Double>(1.0, 0.0);
        ComplexNumber<Double> c = new ComplexNumber<Double>(2.0, 0.0);
        ComplexNumber<Double>[] expected = new ComplexNumber[]{a, b, c};

        ComplexNumber<Double>[] actual = matrix.getEigenValues();

        Assert.assertTrue(Sets.symmetricDifference(Sets.newHashSet(expected), Sets.newHashSet(actual)).isEmpty());
    }

    @Test
    public void testBasicOperation() {
        double[][] raw1 = new double[][]{{2, 0, 0}, {0, 2, 0}, {0, 0, 2}};
        double[][] raw2 = new double[][]{{1, 0, 0}, {1, 0, 0}, {0, 1, 0}};
        JBLASMatrix matrix1 = null;
        JBLASMatrix matrix2 = null;
        try {
            matrix1 = new JBLASMatrix(raw1);
            matrix2 = new JBLASMatrix(raw2);

            matrix1.add(matrix2);
            double[][] actualAddition = matrix1.getData();

            matrix1 = new JBLASMatrix(raw1);
            matrix1.multiply(matrix2);
            double[][] actualMultiplication = matrix1.getData();

            matrix1 = new JBLASMatrix(raw1);
            matrix1.negative();
            double[][] actualNegation = matrix1.getData();

            double[][] expected4Addition = new double[][]{{3, 0, 0}, {1, 2, 0}, {0, 1, 2}};
            double[][] expected4Multiplication = new double[][]{{2, 0, 0}, {2, 0, 0}, {0, 2, 0}};
            double[][] expected4Negation = new double[][]{{-2, 0, 0}, {0, -2, 0}, {0, 0, -2}};

            boolean test4Addition = true;
            boolean test4Multiplication = true;
            boolean test4Negation = true;
            for (int i = 0; i < actualAddition.length; i++) {
                for (int j = 0; j < actualAddition.length; j++) {
                    if (actualAddition[i][j] != expected4Addition[i][j])
                        test4Addition = false;
                    if (actualMultiplication[i][j] != expected4Multiplication[i][j])
                        test4Multiplication = false;
                    if (actualNegation[i][j] != expected4Negation[i][j])
                        test4Negation = false;
                }
            }
            Assert.assertTrue(test4Addition);
            Assert.assertTrue(test4Multiplication);
            Assert.assertTrue(test4Negation);
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (MatrixException e) {
            Assert.fail();
        }
    }

    @Test(expected = MatrixException.class)
    public void testAdditionException() throws MatrixException {
        double[][] raw1 = new double[][]{{2, 0, 0}, {0, 2, 0}, {0, 0, 2}};
        double[][] raw2 = new double[][]{{1, 0}, {1, 0}, {0, 2}};
        JBLASMatrix matrix1 = null;
        JBLASMatrix matrix2 = null;
        try {
            matrix1 = new JBLASMatrix(raw1);
            matrix2 = new JBLASMatrix(raw2);
            matrix1.add(matrix2);
        } catch (InvalidParamException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = MatrixException.class)
    public void testMultiplicationException() throws MatrixException {
        double[][] raw1 = new double[][]{{2, 0, 0}, {0, 2, 0}, {0, 0, 2}};
        double[][] raw2 = new double[][]{{1, 0, 2}, {1, 0, 3}};
        JBLASMatrix matrix1 = null;
        JBLASMatrix matrix2 = null;
        try {
            matrix1 = new JBLASMatrix(raw1);
            matrix2 = new JBLASMatrix(raw2);
            matrix1.multiply(matrix2);
        } catch (InvalidParamException e) {
            e.printStackTrace();
        }

        try {
            matrix2.multiply(matrix1);
        } catch (MatrixException me) {
            Assert.fail();
        }
    }
}
