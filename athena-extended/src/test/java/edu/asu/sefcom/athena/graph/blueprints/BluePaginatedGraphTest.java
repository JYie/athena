package edu.asu.sefcom.athena.graph.blueprints;

import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

/**
 * Created by Jangwon Yie on 10/9/15.
 */
public class BluePaginatedGraphTest {

    private BluePaginatedGraph bpg;
    private Node[] nodes;
    private int size;

    @Before
    public void setUpBefore() throws InvalidParamException {
        this.bpg = new BluePaginatedGraph(new Label("test"));
        this.size = 200;
        this.nodes = new Node[this.size];
        for(int i=0;i<this.size;i++)
            nodes[i] = this.bpg.addNode(null);
    }

    @Test
    public void testLimit(){
        int expectedLimit = 20;

        List<Node> list = this.bpg.getNodes(null, null, 10, expectedLimit);
        Assert.assertTrue(null != list && list.size() == expectedLimit);
    }

    @Test
    public void testOffsetException(){
        int offset = - 3;
        List<Node> list = this.bpg.getNodes(null, null, offset, 1);
        Assert.assertTrue(null == list || list.size() == 0);

        offset = this.size;
        list = this.bpg.getNodes(null, null, offset, 1);
        Assert.assertTrue(null == list || list.size() == 0);
    }


    @Test
    public void testCompleteness(){
        int expectedLimit = 10;
        int start = 0;
        Set<Node> actual = Sets.newHashSet();
        int repeat = this.size/expectedLimit;

        for(int j=0;j<repeat;j++){
            int offset = start + j*expectedLimit;
            List<Node> list = this.bpg.getNodes(null, null, offset, expectedLimit);
            if(null == list)
                continue;
            for(Node node:list){
                if(!actual.add(node))
                    Assert.fail();
            }
        }

        Assert.assertTrue(actual.size() == this.size);
    }

}
