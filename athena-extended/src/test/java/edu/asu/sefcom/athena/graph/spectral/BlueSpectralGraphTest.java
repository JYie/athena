package edu.asu.sefcom.athena.graph.spectral;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueSpectralGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.Property;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 11/5/2015.
 *
 * @author Jangwon Yie
 */
public class BlueSpectralGraphTest {

    private BlueSpectralGraph bsgForSimpleUndirected;
    private BlueSpectralGraph bsgForWeightedUndirected;
    private BlueSpectralGraph bsgForWeightedDirected;

    private Node node1;
    private Node node2;
    private Node node3;
    private Node node4;
    private Node node5;
    private Node node6;
    private Node node7;
    private Node node8;
    private Node node9;

    @Before
    public void setUpBefore() {
        bsgForSimpleUndirected = new BlueSpectralGraph(new Label("bsgTest"));
        bsgForWeightedUndirected = new BlueSpectralGraph(new Label("bsgTest2"));
        bsgForWeightedDirected = new BlueSpectralGraph(new Label("bsgTest3"));
        try {
            node1 = bsgForSimpleUndirected.addNode(null);
            node2 = bsgForSimpleUndirected.addNode(null);
            node3 = bsgForSimpleUndirected.addNode(null);
            bsgForSimpleUndirected.addUndirectedEdge(node1.getUid(), node2.getUid(), null);
            bsgForSimpleUndirected.addUndirectedEdge(node1.getUid(), node3.getUid(), null);

            node4 = bsgForWeightedUndirected.addNode(null);
            node5 = bsgForWeightedUndirected.addNode(null);
            node6 = bsgForWeightedUndirected.addNode(null);
            Map<String, String> weight_45 = Maps.newHashMap();
            weight_45.put(Property.WEIGHT.name(), "0.5");
            Map<String, String> weight_46 = Maps.newHashMap();
            weight_46.put(Property.WEIGHT.name(), "0.3");
            bsgForWeightedUndirected.addUndirectedEdge(node4.getUid(), node5.getUid(), weight_45);
            bsgForWeightedUndirected.addUndirectedEdge(node4.getUid(), node6.getUid(), weight_46);

            node7 = bsgForWeightedDirected.addNode(null);
            node8 = bsgForWeightedDirected.addNode(null);
            node9 = bsgForWeightedDirected.addNode(null);
            Map<String, String> weight_78 = Maps.newHashMap();
            weight_78.put(Property.WEIGHT.name(), "0.5");
            Map<String, String> weight_79 = Maps.newHashMap();
            weight_79.put(Property.WEIGHT.name(), "0.3");
            bsgForWeightedDirected.addDirectedEdge(node7.getUid(), node8.getUid(), weight_78);
            bsgForWeightedDirected.addDirectedEdge(node7.getUid(), node9.getUid(), weight_79);

        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAdjacencyMatrixException() throws UnsupportedOperationException {
        try {
            bsgForSimpleUndirected.addDirectedEdge(node2.getUid(), node3.getUid(), null);
            bsgForSimpleUndirected.generateAdjacencyMatrix();
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (InvalidParamException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAdjacencyMatrix1() {
        Matrix adjacency = null;
        try {
            adjacency = bsgForSimpleUndirected.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isSymmetry(actual));

        ComplexNumber<Double>[] eigenvalues = adjacency.getEigenValues();
        Assert.assertTrue(null != eigenvalues && eigenvalues.length == 3);
        System.out.println("AdjacencyMatrixForSimpleUndirected : " + adjacency);
    }

    @Test
    public void testAdjacencyMatrix2() {
        Matrix adjacency = null;
        try {
            adjacency = bsgForWeightedUndirected.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isSymmetry(actual));

        ComplexNumber<Double>[] eigenvalues = adjacency.getEigenValues();
        Assert.assertTrue(null != eigenvalues && eigenvalues.length == 3);
        System.out.println("AdjacencyMatrixForWeightedUndirected : " + adjacency);
    }

    @Test
    public void testAdjacencyMatrix3() {
        Matrix adjacency = null;
        try {
            adjacency = bsgForWeightedDirected.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(!isSymmetry(actual));

        System.out.println("AdjacencyMatrixForWeightedDirected : " + adjacency);
    }

    @Test
    public void testDiagonalDegreeMatrix1() {
        Matrix degreeDiagonal = null;
        try {
            degreeDiagonal = bsgForSimpleUndirected.generateDiagonalDegreeMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }

        double[][] actual = degreeDiagonal.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isDiagonal(actual));

        ComplexNumber<Double>[] eigenValues = degreeDiagonal.getEigenValues();
        ComplexNumber<Double>[] expected = new ComplexNumber[]{new ComplexNumber(2.0, 0.0), new ComplexNumber(1.0, 0.0)};

        Assert.assertTrue(Sets.difference(Sets.newHashSet(eigenValues), Sets.newHashSet(expected)).isEmpty());
        System.out.println("Diagonal : " + degreeDiagonal);
    }

    @Test
    public void testDiagonalDegreeMatrix2() {
        Matrix degreeDiagonal = null;
        try {
            degreeDiagonal = bsgForWeightedDirected.generateDiagonalDegreeMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }

        double[][] actual = degreeDiagonal.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isDiagonal(actual));

        ComplexNumber<Double>[] eigenValues = degreeDiagonal.getEigenValues();
        ComplexNumber<Double>[] expected = new ComplexNumber[]{new ComplexNumber(0.8, 0.0), new ComplexNumber(0.0, 0.0)};

        Assert.assertTrue(Sets.difference(Sets.newHashSet(eigenValues), Sets.newHashSet(expected)).isEmpty());
        System.out.println("Diagonal : " + degreeDiagonal);
    }

    @Test
    public void testLaplacianMatrix() {
        Matrix laplacian = null;
        try {
            laplacian = bsgForSimpleUndirected.generateLaplacianMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        }
        double[][] actual = laplacian.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
    }

    private boolean isSymmetry(double[][] matrix) {
        boolean isSymmetry = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    isSymmetry = false;
                    continue;
                }
            }
        }
        return isSymmetry;
    }

    private boolean isDiagonal(double[][] matrix) {
        boolean isDiagonal = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i != j && matrix[i][j] != 0) {
                    isDiagonal = false;
                    continue;
                }
            }
        }
        return isDiagonal;
    }
}
