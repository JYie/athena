package edu.asu.sefcom.athena.graph.similarity;

import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Jangwon Yie on 10/13/15.
 * @author Jangwon Yie
 */
public class SimRankTest {

    private SimRank basicSimRank;
    private SimRank miniMaxSimRank;
    private static int stepNum = 20;
    private static float constant = (float)0.8;

    private Graph graph;

    private Node univ;
    private Node profA;
    private Node profB;
    private Node studentA;
    private Node studentB;

    private double univ_studentB_general;
    private double profB_studentA_general;
    private double profA_studentB_general;
    private double profA_profB_general;
    private double studentA_studentB_general;
    private double univ_profB_general;
    private double profB_studentB_general;

    private double univ_studentB_minimax;
    private double profB_studentA_minimax;
    private double profA_studentB_minimax;
    private double profA_profB_minimax;
    private double studentA_studentB_minimax;
    private double univ_profB_minimax;
    private double profB_studentB_minimax;

    /**
     * Here we use 20 as the stepNumber to get accurate value, but it has been experimentally known that
     * 8 is enough good as the step number in a general situation.
     */
    @Before
    public void setUp(){
        this.basicSimRank = new BasicSimRank(stepNum, constant, SimRank.Direction.PointedTo);
        this.miniMaxSimRank = new MiniMaxSimRank(stepNum, constant, SimRank.Direction.PointedTo);
        this.graph = new BlueGraph(new Label("SimRankTest"));
        loadSample();
        loadExpectedResult();
    }

    @Test
    public void testStepfunction0(){
        String a = "A";
        String b = "B";

        Assert.assertTrue(this.basicSimRank.zeroStep(a, b) == 0);
        Assert.assertTrue(this.basicSimRank.zeroStep(a, a) == 1);
    }

    @Test
    public void testStepfunction(){
        String a = "A";

        try {
            Assert.assertTrue(this.basicSimRank.stepFunction(graph, stepNum, a, a) == 1);
            Assert.assertTrue(this.basicSimRank.stepFunction(graph, stepNum, null, null) == 0);

            Assert.assertTrue(this.miniMaxSimRank.stepFunction(graph, stepNum, a, a) == 1);
            Assert.assertTrue(this.miniMaxSimRank.stepFunction(graph, stepNum, null, null) == 0);
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test(expected = InvalidParamException.class)
    public void testInvalidParamException1() throws InvalidParamException, UnsupportedOperationException {
        String a = "A";
        try {
            this.basicSimRank.measureSimilarity(graph, a);
        } catch (StorageException e) {
            Assert.fail();
        }
    }


    @Test(expected = InvalidParamException.class)
    public void testInvalidParamException2() throws InvalidParamException, UnsupportedOperationException {
        String a = "A";
        try {
            this.miniMaxSimRank.measureSimilarity(graph, a);
        } catch (StorageException e) {
            Assert.fail();
        }
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedOperationException1() throws InvalidParamException, UnsupportedOperationException {
        String a = "A";

        try {
            this.basicSimRank.measureSimilarity(graph, a, a, a);
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedOperationException2() throws InvalidParamException, UnsupportedOperationException {
        String a = "A";

        try {
            this.miniMaxSimRank.measureSimilarity(graph, a, a, a);
        } catch (StorageException e) {
            Assert.fail();
        }
    }


    @Test
    public void testSymmetry(){
        try {
            double ub1 = this.basicSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid());
            double bu1 = this.basicSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.univ.getUid());
            Assert.assertTrue(ub1 == bu1);

            double ub2 = this.miniMaxSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid());
            double bu2 = this.miniMaxSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.univ.getUid());
            Assert.assertTrue(ub2 == bu2);

        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testSimilarity(){
        try {
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid()) - this.univ_studentB_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.profB.getUid(), this.studentA.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.profB.getUid(), this.studentA.getUid()) - this.profB_studentA_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.studentB.getUid()) - this.profA_studentB_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.profB.getUid()) - this.profA_profB_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.studentA.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.studentA.getUid(), this.studentB.getUid()) - this.studentA_studentB_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.profB.getUid()) - this.univ_profB_general) < 0.001);
            System.out.println(this.basicSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.basicSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.profB.getUid()) - this.profB_studentB_general) < 0.001);

            System.out.println("Minimax Variation");

            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.studentB.getUid()) - this.univ_studentB_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.profB.getUid(), this.studentA.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.profB.getUid(), this.studentA.getUid()) - this.profB_studentA_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.studentB.getUid()) - this.profA_studentB_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.profA.getUid(), this.profB.getUid()) - this.profA_profB_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.studentA.getUid(), this.studentB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.studentA.getUid(), this.studentB.getUid()) - this.studentA_studentB_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.univ.getUid(), this.profB.getUid()) - this.univ_profB_minimax) < 0.001);
            System.out.println(this.miniMaxSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.profB.getUid()));
            Assert.assertTrue(Math.abs(this.miniMaxSimRank.measureSimilarity(this.graph, this.studentB.getUid(), this.profB.getUid()) - this.profB_studentB_minimax) < 0.001);

        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    /**
     * this sample is appeared in the original paper of SimRank
     */
    private void loadSample(){
        try {
            this.univ = this.graph.addNode(null);
            this.profA = this.graph.addNode(null);
            this.profB = this.graph.addNode(null);
            this.studentA = this.graph.addNode(null);
            this.studentB = this.graph.addNode(null);


            graph.addDirectedEdge(univ.getUid(), profA.getUid(), null);
            graph.addDirectedEdge(univ.getUid(), profB.getUid(), null);
            graph.addDirectedEdge(profA.getUid(), studentA.getUid(), null);
            graph.addDirectedEdge(studentA.getUid(), univ.getUid(), null);
            graph.addDirectedEdge(profB.getUid(), studentB.getUid(), null);
            graph.addDirectedEdge(studentB.getUid(), profB.getUid(), null);
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch(InvalidParamException ipe){
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    private void loadExpectedResult(){
        this.univ_studentB_general = 0.034;
        this.profB_studentA_general = 0.042;
        this.profA_studentB_general = 0.106;
        this.profA_profB_general = 0.414;
        this.studentA_studentB_general = 0.331;
        this.univ_profB_general = 0.132;
        this.profB_studentB_general = 0.088;

        this.univ_studentB_minimax = 0.0328;
        this.profB_studentA_minimax = 0.041;
        this.profA_studentB_minimax = 0.102;
        this.profA_profB_minimax = 0.400;
        this.studentA_studentB_minimax = 0.320;
        this.univ_profB_minimax = 0.128;
        this.profB_studentB_minimax = 0.164;
    }
}
