package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.MultiPartiteGraph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import edu.asu.sefcom.athena.graph.blueprints.BlueMultipartiteGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class EvidenceBasedSimRankTest {

    private EvidenceBasedSimRank evidenceBasedSimRank1;
    private EvidenceBasedSimRank evidenceBasedSimRank2;
    private EvidenceBasedSimRank evidenceBasedSimRank3;

    private static int stepNum1 = 5;
    private static int stepNum2 = 6;
    private static int stepNum3 = 7;
    private static float constant = (float)0.8;

    private MultiPartiteGraph graph;

    private Node camera;
    private Node digitalCamera;
    private Node HP;
    private Node BestBuy;

    private double camera_digitalCamera_5;
    private double camera_digitalCamera_6;
    private double camera_digitalCamera_7;

    @Before
    public void setUp(){
        this.evidenceBasedSimRank1 = new EvidenceBasedSimRank(new BipartiteSimRank(stepNum1, constant, SimRank.Direction.Undirected));
        this.evidenceBasedSimRank2 = new EvidenceBasedSimRank(new BipartiteSimRank(stepNum2, constant, SimRank.Direction.Undirected));
        this.evidenceBasedSimRank3 = new EvidenceBasedSimRank(new BipartiteSimRank(stepNum3, constant, SimRank.Direction.Undirected));
        this.graph = new BlueMultipartiteGraph(new BlueGraph(new Label("BipartiteSimRankTest")));
        loadSample();
        loadExpectedResult();
    }

    @Test
    public void testSimilarity(){
        try {
            double actualSimrank_5 = this.evidenceBasedSimRank1.calculateSimrankSimilarity(this.graph, this.camera.getUid(), this.digitalCamera.getUid());
            double actualSimrank_6 = this.evidenceBasedSimRank2.calculateSimrankSimilarity(this.graph, this.camera.getUid(), this.digitalCamera.getUid());
            double actualSimrank_7 = this.evidenceBasedSimRank3.calculateSimrankSimilarity(this.graph, this.camera.getUid(), this.digitalCamera.getUid());

            Assert.assertTrue(Math.abs(actualSimrank_5 - this.camera_digitalCamera_5) < 0.000001);
            Assert.assertTrue(Math.abs(actualSimrank_6 - this.camera_digitalCamera_6) < 0.000001);
            Assert.assertTrue(Math.abs(actualSimrank_7 - this.camera_digitalCamera_7) < 0.000001);

        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    /**
     * this sample is appeared in the paper "Simrank++: Query Rewritting through Link Anaylysis of the Click Graph"
     */
    private void loadSample(){
        try {
            Map<String, String> forCameraPartition = Maps.newHashMap();
            forCameraPartition.put(BlueMultipartiteGraph.PARTITION, "Camera");
            Map<String, String> forSitePartition = Maps.newHashMap();
            forSitePartition.put(BlueMultipartiteGraph.PARTITION, "Site");

            this.camera = this.graph.addNode(forCameraPartition);
            this.digitalCamera = this.graph.addNode(forCameraPartition);
            this.HP = this.graph.addNode(forSitePartition);
            this.BestBuy = this.graph.addNode(forSitePartition);

            graph.addUndirectedEdge(camera.getUid(), HP.getUid(), null);
            graph.addUndirectedEdge(camera.getUid(), BestBuy.getUid(), null);
            graph.addUndirectedEdge(digitalCamera.getUid(), HP.getUid(), null);
            graph.addUndirectedEdge(digitalCamera.getUid(), BestBuy.getUid(), null);
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch( InvalidParamException ipe){
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    private void loadExpectedResult(){
        this.camera_digitalCamera_5 = 0.49488;
        this.camera_digitalCamera_6 = 0.497952;
        this.camera_digitalCamera_7 = 0.4991808;
    }
}
