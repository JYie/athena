package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.*;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import edu.asu.sefcom.athena.graph.blueprints.BlueMultipartiteGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 10/28/2015.
 * @author Jangwon Yie
 */
public class BipartiteSimRankTest {
    private SimRank bipartiteSimRank;

    private static int stepNum = 10;
    private static float constant = (float)0.8;

    private Graph graph;

    private Node A;
    private Node B;
    private Node sugar;
    private Node frosting;
    private Node eggs;
    private Node flour;

    private double A_B;
    private double sugar_frosting;
    private double sugar_flour;
    private double sugar_eggs;
    private double frosting_eggs;
    private double frosting_flour;
    private double eggs_flour;

    /**
     * Here we use 20 as the stepNumber to get accurate value, but it has been experimentally known that
     * 8 is enough good as the step number in a general situation.
     */
    @Before
    public void setUp(){
        this.bipartiteSimRank = new BipartiteSimRank(stepNum, constant, SimRank.Direction.bothWay);
        this.graph = new BlueMultipartiteGraph(new BlueGraph(new Label("BipartiteSimRankTest")));
        loadSample();
        loadExpectedResult();
    }


    @Test(expected = UnsupportedOperationException.class)
    public void testBetweenDifferentPartition() throws UnsupportedOperationException {
        try {
            this.bipartiteSimRank.measureSimilarity(this.graph, this.A.getUid(), this.flour.getUid());
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testSimilarity(){
        try {
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.A.getUid(), this.B.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.A.getUid(), this.B.getUid()) - this.A_B) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.sugar.getUid(), this.frosting.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.sugar.getUid(), this.frosting.getUid()) - this.sugar_frosting) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.sugar.getUid(), this.eggs.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.sugar.getUid(), this.eggs.getUid()) - this.sugar_eggs) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.flour.getUid(), this.sugar.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.flour.getUid(), this.sugar.getUid()) - this.sugar_flour) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.frosting.getUid(), this.eggs.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.frosting.getUid(), this.eggs.getUid()) - this.frosting_eggs) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.frosting.getUid(), this.flour.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.frosting.getUid(), this.flour.getUid()) - this.frosting_flour) < 0.01);
            System.out.println(this.bipartiteSimRank.measureSimilarity(this.graph, this.eggs.getUid(), this.flour.getUid()));
            Assert.assertTrue(Math.abs(this.bipartiteSimRank.measureSimilarity(this.graph, this.eggs.getUid(), this.flour.getUid()) - this.eggs_flour) < 0.01);

        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    /**
     * this sample is appeared in the original paper of SimRank
     */
    private void loadSample(){
        try {
            Map<String, String> forPeoplepartition = Maps.newHashMap();
            forPeoplepartition.put(BlueMultipartiteGraph.PARTITION, "People");
            Map<String, String> forItemPartition = Maps.newHashMap();
            forItemPartition.put(BlueMultipartiteGraph.PARTITION, "Item");


            this.A = this.graph.addNode(forPeoplepartition);
            this.B = this.graph.addNode(forPeoplepartition);
            this.sugar = this.graph.addNode(forItemPartition);
            this.frosting = this.graph.addNode(forItemPartition);
            this.eggs = this.graph.addNode(forItemPartition);
            this.flour = this.graph.addNode(forItemPartition);

            graph.addDirectedEdge(A.getUid(), sugar.getUid(), null);
            graph.addDirectedEdge(A.getUid(), frosting.getUid(), null);
            graph.addDirectedEdge(A.getUid(), eggs.getUid(), null);

            graph.addDirectedEdge(B.getUid(), flour.getUid(), null);
            graph.addDirectedEdge(B.getUid(), frosting.getUid(), null);
            graph.addDirectedEdge(B.getUid(), eggs.getUid(), null);
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch(InvalidParamException ipe){
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    private void loadExpectedResult(){
        this.A_B = 0.547;
        this.sugar_frosting = 0.619;
        this.sugar_flour = 0.437;
        this.sugar_eggs = 0.619;
        this.frosting_eggs = 0.619;
        this.frosting_flour = 0.619;
        this.eggs_flour = 0.619;
    }
}
