package edu.asu.sefcom.athena.graph.spectral;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.Property;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 12/10/15.
 *
 * @author Jangwon Yie
 */
public class ComposedSpectralGraphTest {

    private Graph csgForSimpleUndirected;
    private Graph csgForWeightedUndirected;
    private Graph csgForWeightedDirected;

    private Node node1;
    private Node node2;
    private Node node3;
    private Node node4;
    private Node node5;
    private Node node6;
    private Node node7;
    private Node node8;
    private Node node9;

    @Before
    public void setUpBefore() {
        try {
            csgForSimpleUndirected = new BlueGraph(new Label("bsgTest"));
            csgForWeightedUndirected = new BlueGraph(new Label("bsgTest2"));
            csgForWeightedDirected = new BlueGraph(new Label("bsgTest3"));

            node1 = csgForSimpleUndirected.addNode(null);
            node2 = csgForSimpleUndirected.addNode(null);
            node3 = csgForSimpleUndirected.addNode(null);
            csgForSimpleUndirected.addUndirectedEdge(node1.getUid(), node2.getUid(), null);
            csgForSimpleUndirected.addUndirectedEdge(node1.getUid(), node3.getUid(), null);

            node4 = csgForWeightedUndirected.addNode(null);
            node5 = csgForWeightedUndirected.addNode(null);
            node6 = csgForWeightedUndirected.addNode(null);
            Map<String, String> weight_45 = Maps.newHashMap();
            weight_45.put(Property.WEIGHT.name(), "0.5");
            Map<String, String> weight_46 = Maps.newHashMap();
            weight_46.put(Property.WEIGHT.name(), "0.3");
            csgForWeightedUndirected.addUndirectedEdge(node4.getUid(), node5.getUid(), weight_45);
            csgForWeightedUndirected.addUndirectedEdge(node4.getUid(), node6.getUid(), weight_46);

            node7 = csgForWeightedDirected.addNode(null);
            node8 = csgForWeightedDirected.addNode(null);
            node9 = csgForWeightedDirected.addNode(null);
            Map<String, String> weight_78 = Maps.newHashMap();
            weight_78.put(Property.WEIGHT.name(), "0.5");
            Map<String, String> weight_79 = Maps.newHashMap();
            weight_79.put(Property.WEIGHT.name(), "0.3");
            csgForWeightedDirected.addDirectedEdge(node7.getUid(), node8.getUid(), weight_78);
            csgForWeightedDirected.addDirectedEdge(node7.getUid(), node9.getUid(), weight_79);

        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testAdjacencyMatrixException() throws UnsupportedOperationException {
        try {
            csgForSimpleUndirected.addDirectedEdge(node2.getUid(), node3.getUid(), null);
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForSimpleUndirected);
            csg.generateAdjacencyMatrix();
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (InvalidParamException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testAdjacencyMatrix1() {
        Matrix adjacency = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForSimpleUndirected);
            adjacency = csg.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isSymmetry(actual));

        ComplexNumber<Double>[] eigenvalues = adjacency.getEigenValues();
        Assert.assertTrue(null != eigenvalues && eigenvalues.length == 3);
        System.out.println("AdjacencyMatrixForSimpleUndirected : " + adjacency);
    }

    @Test
    public void testAdjacencyMatrix1_1() {
        try {
            csgForSimpleUndirected.removeNode(node3.getUid());
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForSimpleUndirected);
            Matrix adjacency = csg.generateAdjacencyMatrix();
            System.out.println(adjacency);
            double[][] actual = adjacency.getData();
            if (null == actual)
                Assert.fail();

            Assert.assertTrue(2 == actual.length);
            Assert.assertTrue(isSymmetry(actual));
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    @Test
    public void testAdjacencyMatrix2() {
        Matrix adjacency = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForWeightedUndirected);
            adjacency = csg.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isSymmetry(actual));

        ComplexNumber<Double>[] eigenvalues = adjacency.getEigenValues();
        Assert.assertTrue(null != eigenvalues && eigenvalues.length == 3);
        System.out.println("AdjacencyMatrixForWeightedUndirected : " + adjacency);
    }

    @Test
    public void testAdjacencyMatrix3() {
        Matrix adjacency = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForWeightedDirected);
            adjacency = csg.generateAdjacencyMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        double[][] actual = adjacency.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(!isSymmetry(actual));

        System.out.println("AdjacencyMatrixForWeightedDirected : " + adjacency);
    }

    @Test
    public void testDiagonalDegreeMatrix1() {
        Matrix degreeDiagonal = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForSimpleUndirected);
            degreeDiagonal = csg.generateDiagonalDegreeMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        double[][] actual = degreeDiagonal.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isDiagonal(actual));

        ComplexNumber<Double>[] eigenValues = degreeDiagonal.getEigenValues();
        ComplexNumber<Double>[] expected = new ComplexNumber[]{new ComplexNumber(2.0, 0.0), new ComplexNumber(1.0, 0.0)};

        Assert.assertTrue(Sets.difference(Sets.newHashSet(eigenValues), Sets.newHashSet(expected)).isEmpty());
        System.out.println("Diagonal : " + degreeDiagonal);
    }

    @Test
    public void testDiagonalDegreeMatrix2() {
        Matrix degreeDiagonal = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForWeightedDirected);
            degreeDiagonal = csg.generateDiagonalDegreeMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }

        double[][] actual = degreeDiagonal.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
        Assert.assertTrue(isDiagonal(actual));

        ComplexNumber<Double>[] eigenValues = degreeDiagonal.getEigenValues();
        ComplexNumber<Double>[] expected = new ComplexNumber[]{new ComplexNumber(0.8, 0.0), new ComplexNumber(0.0, 0.0)};

        Assert.assertTrue(Sets.difference(Sets.newHashSet(eigenValues), Sets.newHashSet(expected)).isEmpty());
        System.out.println("Diagonal : " + degreeDiagonal);
    }

    @Test
    public void testLaplacianMatrix() {
        Matrix laplacian = null;
        try {
            ComposedSpectralGraph csg = new ComposedSpectralGraph(csgForSimpleUndirected);
            laplacian = csg.generateLaplacianMatrix();
        } catch (UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
        double[][] actual = laplacian.getData();
        if (null == actual)
            Assert.fail();

        Assert.assertTrue(3 == actual.length);
    }

    private boolean isSymmetry(double[][] matrix) {
        boolean isSymmetry = true;
        int length = matrix.length;
        for (int i = 0; i < length; i++) {
            int cLength = matrix[i].length;
            for (int j = 0; j < cLength; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    isSymmetry = false;
                    continue;
                }
            }
        }
        return isSymmetry;
    }

    private boolean isDiagonal(double[][] matrix) {
        boolean isDiagonal = true;
        int length = matrix.length;
        for (int i = 0; i < length; i++) {
            int cLength = matrix[i].length;
            for (int j = 0; j < cLength; j++) {
                if (i != j && matrix[i][j] != 0) {
                    isDiagonal = false;
                    continue;
                }
            }
        }
        return isDiagonal;
    }
}
