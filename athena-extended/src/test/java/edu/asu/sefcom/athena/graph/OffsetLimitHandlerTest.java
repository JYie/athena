package edu.asu.sefcom.athena.graph;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Jangwon Yie on 10/10/15.
 */
public class OffsetLimitHandlerTest {

    private OffsetLimitHandler<String, String> offsetLimitHandler;

    private String[] strings;
    private int size;
    private List<String> result;

    @Before
    public void setUpBefore(){
        this.size = 200;
        this.strings = new String[this.size];
        for(int i=0;i<this.size;i++)
            strings[i] = new Integer(i).toString();
        this.result = Lists.newArrayList();
        this.offsetLimitHandler = new OffsetLimitHandler<String, String>() {
            @Override
            public String transform(String s) {
                return s;
            }
        };
    }

    @Test
    public void testLimit(){
        int expectedLimit = 20;
        this.offsetLimitHandler.handle(Arrays.asList(strings).iterator(), this.result, 10, expectedLimit);
        Assert.assertTrue(null != result && result.size() == expectedLimit);
    }

    @Test
    public void testOffsetException(){
        int offset = - 3;
        this.offsetLimitHandler.handle(Arrays.asList(strings).iterator(), this.result, offset, 1);
        Assert.assertTrue(this.result.size() == 0);

        offset = this.size;
        this.offsetLimitHandler.handle(Arrays.asList(strings).iterator(), this.result, offset, 1);
        Assert.assertTrue(this.result.size() == 0);
    }


    @Test
    public void testCompleteness(){
        int expectedLimit = 10;
        int start = 0;
        int repeat = this.size/expectedLimit;

        for(int j=0;j<repeat;j++){
            int offset = start + j*expectedLimit;
            this.offsetLimitHandler.handle(Arrays.asList(strings).iterator(), this.result, offset, expectedLimit);
        }

        Assert.assertTrue(this.result.size() == this.size);
    }


}
