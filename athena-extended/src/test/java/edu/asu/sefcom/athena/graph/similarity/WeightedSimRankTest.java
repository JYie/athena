package edu.asu.sefcom.athena.graph.similarity;

import com.google.common.collect.Maps;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.graph.MultiPartiteGraph;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueGraph;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.graph.element.Node;
import edu.asu.sefcom.athena.graph.element.Property;
import edu.asu.sefcom.athena.graph.blueprints.BlueBaseGraph;
import edu.asu.sefcom.athena.graph.blueprints.BlueMultipartiteGraph;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by Jangwon Yie on 10/28/15.
 * @author Jangwon Yie
 */
public class WeightedSimRankTest {

    private WeightedSimRank weightedSimRank;

    private static int stepNum = 7;
    private static float constant = (float)0.8;

    private MultiPartiteGraph graph;

    private Node camera;
    private Node digitalCamera;
    private Node HP;
    private Node BestBuy;


    @Before
    public void setUp(){
        this.weightedSimRank = new WeightedSimRank(new BipartiteSimRank(stepNum, constant, SimRank.Direction.Undirected)) ;
        this.graph = new BlueMultipartiteGraph(new BlueGraph(new Label("WeightedSimRankTest")));
        loadSample();
    }

    @Test
    public void testSimilarity(){
        try {
            double actualSimrank = this.weightedSimRank.calculateSimrankSimilarity(this.graph, this.camera.getUid(), this.digitalCamera.getUid());
            System.out.println(actualSimrank);
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }

    /**
     * this sample is appeared in the paper "Simrank++: Query Rewritting through Link Anaylysis of the Click Graph"
     */
    private void loadSample(){
        try {
            Map<String, String> forCameraPartition = Maps.newHashMap();
            forCameraPartition.put(BlueMultipartiteGraph.PARTITION, "Camera");
            Map<String, String> forSitePartition = Maps.newHashMap();
            forSitePartition.put(BlueMultipartiteGraph.PARTITION, "Site");

            this.camera = this.graph.addNode(forCameraPartition);
            this.digitalCamera = this.graph.addNode(forCameraPartition);
            this.HP = this.graph.addNode(forSitePartition);
            this.BestBuy = this.graph.addNode(forSitePartition);

            Map<String, String> camera_HP = Maps.newHashMap();
            camera_HP.put(Property.WEIGHT.name(),"100");
            Map<String, String> camera_BB = Maps.newHashMap();
            camera_BB.put(Property.WEIGHT.name(),"1");
            Map<String, String> digita_HP = Maps.newHashMap();
            digita_HP.put(Property.WEIGHT.name(),"100");
            Map<String, String> digita_BB = Maps.newHashMap();
            digita_BB.put(Property.WEIGHT.name(),"1");

            graph.addUndirectedEdge(camera.getUid(), HP.getUid(), camera_HP);
            graph.addUndirectedEdge(camera.getUid(), BestBuy.getUid(), camera_BB);
            graph.addUndirectedEdge(digitalCamera.getUid(), HP.getUid(), digita_HP);
            graph.addUndirectedEdge(digitalCamera.getUid(), BestBuy.getUid(), digita_BB);
        } catch (UnavailabeEdgeException e) {
            Assert.fail();
        } catch (edu.asu.sefcom.athena.UnsupportedOperationException e) {
            Assert.fail();
        } catch (InvalidParamException ipe){
            Assert.fail();
        } catch (StorageException e) {
            Assert.fail();
        }
    }
}
