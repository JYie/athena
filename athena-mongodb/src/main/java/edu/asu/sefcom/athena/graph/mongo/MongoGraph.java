package edu.asu.sefcom.athena.graph.mongo;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.mongodb.*;
import edu.asu.sefcom.athena.InvalidParamException;
import edu.asu.sefcom.athena.StorageException;
import edu.asu.sefcom.athena.UnsupportedOperationException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphType;
import edu.asu.sefcom.athena.graph.UnavailabeEdgeException;
import edu.asu.sefcom.athena.graph.blueprints.BlueEdge;
import edu.asu.sefcom.athena.graph.blueprints.BlueNode;
import edu.asu.sefcom.athena.graph.blueprints.IDGenerator;
import edu.asu.sefcom.athena.graph.element.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jangwon Yie on 11/30/2015.
 *
 * @author Jangwon Yie
 */
public class MongoGraph implements Graph {

    private static final String AthenaHeader = "Athena_";

    private static final String ELEMENT_ID_KEY;
    private static final String ORIENTATION_KEY;
    private static final String ORIENTATION_VALUE;
    private static final String ORIENTATION_VALUES;
    private static final String NEIGHBOR_KEY;
    private static final String NEIGHBOR_VALUE;
    private static final String NEIGHBOR_VALUES;
    private static final String EDGE_TYPE;
    private static final String DIRECTED;
    private static final String UNDIRECTED;
    private static final String HYPER;
    private static final String DE_Head;
    private static final String DE_Tail;
    private static final String UDE1;
    private static final String UDE2;

    static {
        ELEMENT_ID_KEY = AthenaHeader + "ElementIdentifier";
        ORIENTATION_KEY = AthenaHeader + "OrientationKeyIdentifier";
        ORIENTATION_VALUE = AthenaHeader + "OrientationValueIdentifier";
        ORIENTATION_VALUES = AthenaHeader + "OrientationValuesIdentifier";
        NEIGHBOR_KEY = AthenaHeader + "NeighborKeyIdentifier";
        NEIGHBOR_VALUE = AthenaHeader + "NeighborValueIdentifier";
        NEIGHBOR_VALUES = AthenaHeader + "NeighborValuesIdentifier";
        EDGE_TYPE = AthenaHeader + "EdgeType";
        DIRECTED = AthenaHeader + "Directed";
        UNDIRECTED = AthenaHeader + "Undirected";
        HYPER = AthenaHeader + "Hyper";
        DE_Head = AthenaHeader + "DirectedEdgeHead";
        DE_Tail = AthenaHeader + "DirectedEdgeTail";
        UDE1 = AthenaHeader + "UndirectedEdgeNode1";
        UDE2 = AthenaHeader + "UndirectedEdgeNode2";
    }

    private static final String ORIENTATION_HEAD = "AthenaHead";

    private DB db;
    private Label label;
    private PropertyRule rule;

    private GraphType graphType;

    public MongoGraph(DB db, Label label, PropertyRule rule) {
        this.db = db;
        this.label = label;
        this.rule = rule;
        this.graphType = null;
    }

    public MongoGraph(DB db, Label label) {
        this(db, label, null);
    }

    @Override
    public Label getLabel() {
        return this.label;
    }

    @Override
    public Node addNode(Map<String, String> properties) throws InvalidParamException, UnsupportedOperationException, StorageException {
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        String id = writeElement(nodes, properties);
        Node node = new BlueNode(id);
        if (null == properties)
            return node;

        Set<String> keys = properties.keySet();
        for (String key : keys) {
            node.setProperty(key, properties.get(key));
        }

        return node;
    }

    @Override
    public void removeNode(String id) throws UnsupportedOperationException, StorageException {
        if (null == id)
            return;
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        removeElementById(nodes, id);

        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        Set<String> toBeRemoved = Sets.newHashSet();
        Iterator<Edge> incidentUndirectedEdges = getIncidentEdges(id, Direction.SIMPLEUNDIRECT, null, null);
        if (null != incidentUndirectedEdges) {
            while (incidentUndirectedEdges.hasNext())
                toBeRemoved.add(incidentUndirectedEdges.next().getUid());
        }
        Iterator<Edge> incidentDirectedEdges = getIncidentEdges(id, Direction.SIMPLEBOTH, null, null);
        if (null != incidentDirectedEdges) {
            while (incidentDirectedEdges.hasNext())
                toBeRemoved.add(incidentDirectedEdges.next().getUid());
        }
        Iterator<Edge> incidentHyperEdges = getIncidentEdges(id, Direction.HYPERANY, null, null);
        if (null != incidentHyperEdges) {
            while (incidentHyperEdges.hasNext())
                toBeRemoved.add(incidentHyperEdges.next().getUid());
        }

        removeElementsById(edges, toBeRemoved);
    }

    @Override
    public Edge addDirectedEdge(String inNode, String outNode, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException {
        if (null == inNode || null == outNode)
            return null;
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        if (!isExist(nodes, inNode))
            throw new UnavailabeEdgeException(inNode + "does not exist");
        if (!isExist(nodes, outNode))
            throw new UnavailabeEdgeException(outNode + "does not exist");

        DBCollection forwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
        DBCollection backwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleBackWard);
        checkPropertyRule(properties);

        if (null == forwards || null == backwards)
            throw new InvalidParamException("Invalid Label");

        writeOrientation(forwards, inNode, outNode);
        writeOrientation(backwards, outNode, inNode);

        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        if (null == properties)
            properties = Maps.newHashMap();
        properties.put(EDGE_TYPE, DIRECTED);
        properties.put(DE_Head, inNode);
        properties.put(DE_Tail, outNode);
        String edgeID = writeElement(edges, properties);
        initializeType();
        return new BlueEdge(edgeID, EdgeType.SimpleDirected, new BlueNode(inNode), new BlueNode(outNode));
    }

    @Override
    public Edge addUndirectedEdge(String node1, String node2, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException, StorageException {
        if (null == node1 || null == node2)
            return null;
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        if (!isExist(nodes, node1))
            throw new UnavailabeEdgeException(node1 + "does not exist");
        if (!isExist(nodes, node2))
            throw new UnavailabeEdgeException(node2 + "does not exist");

        DBCollection simpleUndirects = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
        checkPropertyRule(properties);

        if (null == simpleUndirects)
            throw new InvalidParamException("Invalid Label");

        writeNeighbor(simpleUndirects, node1, node2);
        writeNeighbor(simpleUndirects, node2, node1);

        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        if (null == properties)
            properties = Maps.newHashMap();
        properties.put(EDGE_TYPE, UNDIRECTED);
        properties.put(UDE1, node1);
        properties.put(UDE2, node2);
        String edgeID = writeElement(edges, properties);
        initializeType();
        return new BlueEdge(edgeID, EdgeType.SimpleUndirected, new BlueNode(node1), new BlueNode(node2));
    }

    @Override
    public Edge addHyperEdge(Set<Object> nodes, Map<String, String> properties) throws InvalidParamException, UnavailabeEdgeException, UnsupportedOperationException {
        return null;
    }

    @Override
    public void removeEdge(String id) throws UnsupportedOperationException, StorageException {
        if (null == id)
            return;
        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        DBObject edge = extractElementById(edges, id);
        if (null == edge)
            return;

        String type = (String) edge.get(EDGE_TYPE);
        if (null == type || type.isEmpty())
            return;

        try {
            if (type.compareTo(UNDIRECTED) == 0) {
                DBCollection neighbors = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
                String node1 = (String) edge.get(UDE1);
                String node2 = (String) edge.get(UDE2);
                if (null == neighbors || null == node1 || null == node2)
                    return;
                removeNeighbor(neighbors, node1, node2);
                removeNeighbor(neighbors, node2, node1);
            } else if (type.compareTo(DIRECTED) == 0) {
                DBCollection forwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
                DBCollection backwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleBackWard);
                String head = (String) edge.get(DE_Head);
                String tail = (String) edge.get(DE_Tail);
                if (null == forwards || null == backwards || null == head || null == tail)
                    return;
                removeOrientation(forwards, head, tail);
                removeOrientation(backwards, tail, head);
            } else
                return;
        } finally {
            removeElementById(edges, id);
            initializeType();
        }

    }

    @Override
    public Edge getEdge(String id) throws StorageException {
        if (null == id)
            return null;
        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        DBObject edge = extractElementById(edges, id);
        if (null == edge)
            return null;
        String type = (String) edge.get(EDGE_TYPE);
        if (null == type || type.isEmpty())
            return null;

        if (type.compareTo(UNDIRECTED) == 0) {
            DBCollection neighbors = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
            String node1 = (String) edge.get(UDE1);
            String node2 = (String) edge.get(UDE2);
            if (null == neighbors || null == node1 || null == node2)
                return null;
            try {
                Edge result = new BlueEdge(id, EdgeType.SimpleUndirected, new BlueNode(node1), new BlueNode(node2));
                fillProperties(result, edge);
                return result;
            } catch (InvalidParamException e) {
                assert false;
                return null;
            }
        } else if (type.compareTo(DIRECTED) == 0) {
            DBCollection forwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
            DBCollection backwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleBackWard);
            String head = (String) edge.get(DE_Head);
            String tail = (String) edge.get(DE_Tail);
            if (null == forwards || null == backwards || null == head || null == tail)
                return null;
            try {
                Edge result = new BlueEdge(id, EdgeType.SimpleDirected, new BlueNode(head), new BlueNode(tail));
                fillProperties(result, edge);
                return result;
            } catch (InvalidParamException e) {
                assert false;
                return null;
            }
        } else
            return null;
    }

    @Override
    public Iterator<Node> getNodes(String key, String value) throws StorageException {
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        Map<String, String> condition = Maps.newHashMap();
        if (null != key && null != value)
            condition.put(key, value);
        DBCursor cursor = searchElements(nodes, condition);
        if (null == cursor)
            return null;
        Set<Node> result = Sets.newHashSet();
        while (cursor.hasNext()) {
            DBObject node = cursor.next();
            String id = (String) node.get(ELEMENT_ID_KEY);
            if (null == id)
                continue;
            Node blueNode = getNode(id);
            result.add(blueNode);
        }
        return result.iterator();
    }

    @Override
    public Node getNode(String id) throws StorageException {
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        DBObject node = extractElementById(nodes, id);
        if (null == node)
            return null;

        BlueNode blueNode = new BlueNode(id);
        fillProperties(blueNode, node);
        return blueNode;
    }

    @Override
    public Iterator<Edge> getEdges(String key, String value) throws StorageException {
        DBCollection edges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        Map<String, String> condition = Maps.newHashMap();
        if (null != key && null != value)
            condition.put(key, value);
        DBCursor cursor = searchElements(edges, condition);
        if (null == cursor)
            return null;
        Set<Edge> result = Sets.newHashSet();
        while (cursor.hasNext()) {
            DBObject node = cursor.next();
            String id = (String) node.get(ELEMENT_ID_KEY);
            if (null == id)
                continue;
            Edge blueEdge = getEdge(id);
            result.add(blueEdge);
        }
        return result.iterator();
    }

    @Override
    public Iterator<Node> getNeighbors(String nodeId, Direction direction, String key, String value) throws StorageException {
        if (null == nodeId || nodeId.isEmpty())
            return null;

        if (null == direction)
            return null;

        Set<Node> result = Sets.newHashSet();
        switch (direction) {
            case SIMPLEIN:
                putInNeighbors(result, nodeId);
                break;
            case SIMPLEOUT:
                putOutNeighbors(result, nodeId);
                break;
            case SIMPLEUNDIRECT:
                putNeighbors(result, nodeId);
                break;
            case SIMPLEBOTH:
                putInNeighbors(result, nodeId);
                putOutNeighbors(result, nodeId);
                break;
        }

        if (null != key && null != value) {
            Set<? extends Element> filtered = filterCondition(result, key, value);
            if (null == filtered)
                return null;
            return ((Set<Node>) filtered).iterator();
        }
        return result.iterator();
    }

    @Override
    public Iterator<Edge> getIncidentEdges(String nodeId, Direction direction, String key, String value) throws StorageException {
        if (null == nodeId || nodeId.isEmpty())
            return null;

        if (null == direction)
            return null;

        Set<Edge> result = Sets.newHashSet();
        switch (direction) {
            case SIMPLEIN:
                putInIncidentEdges(result, nodeId);
                break;
            case SIMPLEOUT:
                putOutIncidentEdges(result, nodeId);
                break;
            case SIMPLEUNDIRECT:
                putUndirectedIncidentEdges(result, nodeId);
                break;
            case SIMPLEBOTH:
                putInIncidentEdges(result, nodeId);
                putOutIncidentEdges(result, nodeId);
                break;
        }

        if (null != key && null != value) {
            Set<Edge> filtered = (Set<Edge>)filterCondition(result, key, value);
            if (null == filtered)
                return null;
            return filtered.iterator();
        }

        return result.iterator();
    }

    @Override
    public GraphType getGraphType() {
        if (null == this.graphType)
            checkType();
        return this.graphType;
    }


    String writeElement(DBCollection elements, Map<String, String> properties) throws StorageException, InvalidParamException {
        checkPropertyRule(properties);
        String id = IDGenerator.generateID(properties);

        DBObject object = new BasicDBObject();
        object.put(ELEMENT_ID_KEY, id);
        if (null == properties) {
            elements.insert(object, WriteConcern.SAFE);
            return id;
        }

        Set<String> keys = properties.keySet();
        if (null == keys) {
            elements.insert(object, WriteConcern.SAFE);
            return id;
        }

        for(String key:keys) {
            object.put(key, properties.get(key));
        }
        elements.insert(object, WriteConcern.SAFE);
        return id;
    }

    void fillProperties(Element element, DBObject object) {
        Set<String> keys = object.keySet();
        if (null == keys)
            return;
        for (String key : keys) {
            Object value = object.get(key);
            if (null == value)
                continue;
            if (value instanceof String)
                element.setProperty(key, (String) value);
            else
                element.setProperty(key, value.toString());
        }
    }

    void removeElementById(DBCollection elements, String id) throws StorageException {
        DBObject condition = new BasicDBObject();
        condition.put(ELEMENT_ID_KEY, id);
        try {
            elements.remove(condition);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    void removeElementsById(DBCollection elements, Set<String> ids) throws StorageException {

        if (null == ids || ids.size() == 0)
            return;

        List<DBObject> conditions = Lists.newArrayList();
        for (String id : ids) {
            DBObject condition = new BasicDBObject();
            condition.put(ELEMENT_ID_KEY, id);
            conditions.add(condition);
        }
        DBObject condition = new BasicDBObject();
        condition.put("$or", conditions);

        try {
            elements.remove(condition);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    DBObject extractElementById(DBCollection elements, String id) throws StorageException {
        DBObject condition = new BasicDBObject();
        condition.put(ELEMENT_ID_KEY, id);
        try {
            return elements.findOne(condition);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    DBObject searchElement(DBCollection elements, Map<String, String> condition) throws StorageException {
        if (null != condition && !condition.isEmpty()) {
            DBObject forCondition = new BasicDBObject();
            forCondition.putAll(condition);
            return elements.findOne(forCondition);
        } else
            return elements.findOne();
    }

    DBCursor searchElements(DBCollection elements, Map<String, String> condition) throws StorageException {
        if (null != condition && !condition.isEmpty()) {
            DBObject forCondition = new BasicDBObject();
            forCondition.putAll(condition);
            return elements.find(forCondition);
        } else
            return elements.find();
    }

    private Set<? extends Element> filterCondition(Set<? extends Element> set, String key, String value) {
        Set<Element> filtered = Sets.newHashSet();
        for (Element element : set) {
            String actual = (String) element.getProperty(key);
            if (null == actual)
                continue;
            if (value.compareTo(actual) == 0)
                filtered.add(element);
        }
        return filtered;
    }

    private void putOutIncidentEdges(Set<Edge> edges, String head) throws StorageException {
        DBCollection forwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
        BasicDBList list = extractOrientations(forwards, head);
        if (null == list)
            return;

        int size = list.size();
        DBCollection edgeCollection = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        for (int i = 0; i < size; i++) {
            DBObject neighborObj = (DBObject) list.get(i);
            String tail = (String) neighborObj.get(ORIENTATION_VALUE);

            Map<String, String> condition = Maps.newHashMap();
            condition.put(DE_Head, head);
            condition.put(DE_Tail, tail);
            DBObject edgeObject = searchElement(edgeCollection, condition);
            String edgeId = (String) edgeObject.get(ELEMENT_ID_KEY);
            edges.add(getEdge(edgeId));
        }
    }

    private void putInIncidentEdges(Set<Edge> edges, String tail) throws StorageException {
        DBCollection backwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleBackWard);
        BasicDBList list = extractOrientations(backwards, tail);
        if (null == list)
            return;

        int size = list.size();
        DBCollection edgeCollection = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        for (int i = 0; i < size; i++) {
            DBObject neighborObj = (DBObject) list.get(i);
            String head = (String) neighborObj.get(ORIENTATION_VALUE);

            Map<String, String> condition = Maps.newHashMap();
            condition.put(DE_Head, head);
            condition.put(DE_Tail, tail);
            DBObject edgeObject = searchElement(edgeCollection, condition);
            String edgeId = (String) edgeObject.get(ELEMENT_ID_KEY);
            edges.add(getEdge(edgeId));
        }
    }

    private void putUndirectedIncidentEdges(Set<Edge> edges, String node) throws StorageException {
        DBCollection undirected = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
        BasicDBList list = extractNeighbors(undirected, node);
        if (null == list)
            return;

        int size = list.size();
        DBCollection edgeCollection = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Edge);
        for (int i = 0; i < size; i++) {
            DBObject neighborObj = (DBObject) list.get(i);
            String neighbor = (String) neighborObj.get(NEIGHBOR_VALUE);

            DBObject condition1 = new BasicDBObject();
            condition1.put(UDE1, node);
            condition1.put(UDE2, neighbor);
            DBObject condition2 = new BasicDBObject();
            condition2.put(UDE2, node);
            condition2.put(UDE1, neighbor);
            List<DBObject> conditions = Lists.newArrayList();
            conditions.add(condition1);
            conditions.add(condition2);
            DBObject query = new BasicDBObject();
            query.put("$or", conditions);
            DBObject edgeObject = edgeCollection.findOne(query);
            if (null == edgeObject)
                continue;
            String edgeId = (String) edgeObject.get(ELEMENT_ID_KEY);
            edges.add(getEdge(edgeId));
        }
    }

    private void putOrientations(DBCollection collection, Set<Node> orientations, String nodeId) throws StorageException {
        BasicDBList list = extractOrientations(collection, nodeId);
        if (null == list)
            return;

        int size = list.size();
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        for (int i = 0; i < size; i++) {
            BasicDBObject neighborObj = (BasicDBObject) list.get(i);
            String neighbor = (String) neighborObj.get(ORIENTATION_VALUE);
            Node node = new BlueNode(neighbor);
            fillProperties(node, extractElementById(nodes, neighbor));
            orientations.add(node);
        }
    }

    private void putNeighbors(DBCollection collection, Set<Node> neighbors, String nodeId) throws StorageException {
        BasicDBList list = extractNeighbors(collection, nodeId);
        if (null == list)
            return;

        int size = list.size();
        DBCollection nodes = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.Node);
        for (int i = 0; i < size; i++) {
            BasicDBObject neighborObj = (BasicDBObject) list.get(i);
            String neighbor = (String) neighborObj.get(NEIGHBOR_VALUE);
            Node node = new BlueNode(neighbor);
            fillProperties(node, extractElementById(nodes, neighbor));
            neighbors.add(node);
        }
    }

    private void putOutNeighbors(Set<Node> neighbors, String nodeId) throws StorageException {
        DBCollection forwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
        putOrientations(forwards, neighbors, nodeId);
    }

    private void putInNeighbors(Set<Node> neighbors, String nodeId) throws StorageException {
        DBCollection backwards = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleBackWard);
        putOrientations(backwards, neighbors, nodeId);
    }

    private void putNeighbors(Set<Node> neighbors, String nodeId) throws StorageException {
        DBCollection undirected = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
        putNeighbors(undirected, neighbors, nodeId);
    }

    void writeOrientation(final DBCollection orientations, final String key, final String value) throws StorageException {
        try {
            DBObject forCurrent = new BasicDBObject(ORIENTATION_KEY, key);
            DBObject current = orientations.findOne(forCurrent);
            if (null == current) {
                BasicDBList newList = new BasicDBList();
                newList.add(new BasicDBObject(ORIENTATION_VALUE, value));
                DBObject init = new BasicDBObject();
                init.put(ORIENTATION_KEY, key);
                init.put(ORIENTATION_VALUES, newList);
                orientations.insert(init, WriteConcern.SAFE);
                return;
            }

            BasicDBList origin = (BasicDBList) current.get(ORIENTATION_VALUES);
            if (null == origin)
                origin = new BasicDBList();

            DBObject new_ = new BasicDBObject(ORIENTATION_VALUE, value);
            origin.add(new_);

            DBObject updated = new BasicDBObject();
            updated.put(ORIENTATION_KEY, key);
            updated.put(ORIENTATION_VALUES, origin);
            orientations.update(forCurrent, updated);
//            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(ORIENTATION_VALUES, origin));
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    BasicDBList extractOrientations(final DBCollection orientations, final String id) throws StorageException {
        DBObject forOrientations = new BasicDBObject(ORIENTATION_KEY, id);
        DBObject orientationList = orientations.findOne(forOrientations);
        if (null == orientationList)
            return null;
        return (BasicDBList) orientationList.get(ORIENTATION_VALUES);
    }

    void writeNeighbor(final DBCollection neighbors, final String key, final String value) throws StorageException {
        try {
            DBObject forCurrent = new BasicDBObject(NEIGHBOR_KEY, key);
            DBObject current = neighbors.findOne(forCurrent);
            if (null == current) {
                BasicDBList newList = new BasicDBList();
                newList.add(new BasicDBObject(NEIGHBOR_VALUE, value));
                DBObject init = new BasicDBObject();
                init.put(NEIGHBOR_KEY, key);
                init.put(NEIGHBOR_VALUES, newList);
                neighbors.insert(init, WriteConcern.SAFE);
                return;
            }

            BasicDBList origin = (BasicDBList) current.get(NEIGHBOR_VALUES);
            if (null == origin)
                origin = new BasicDBList();

            DBObject new_ = new BasicDBObject(NEIGHBOR_VALUE, value);
            origin.add(new_);

            DBObject updated = new BasicDBObject();
            updated.put(NEIGHBOR_KEY, key);
            updated.put(NEIGHBOR_VALUES, origin);
            //DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(NEIGHBOR_VALUES, origin));
            neighbors.update(forCurrent, updated);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    BasicDBList extractNeighbors(final DBCollection neighbors, final String id) throws StorageException {
        DBObject forNeighbors = new BasicDBObject(NEIGHBOR_KEY, id);
        DBObject neighborsList = neighbors.findOne(forNeighbors);
        if (null == neighborsList)
            return null;
        return (BasicDBList) neighborsList.get(NEIGHBOR_VALUES);
    }

    void removeNeighbor(final DBCollection neighbors, final String key, final String value) throws StorageException {
        try {
            DBObject forCurrent = new BasicDBObject(NEIGHBOR_KEY, key);
            DBObject current = neighbors.findOne(forCurrent);
            if (null == current)
                return;

            BasicDBList origin = (BasicDBList) current.get(NEIGHBOR_VALUES);
            if (null == origin)
                return;

            DBObject forRemoved = new BasicDBObject(NEIGHBOR_VALUE, value);
            origin.remove(forRemoved);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(NEIGHBOR_VALUES, origin));
            ;
            neighbors.update(forCurrent, updated);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    void removeOrientation(final DBCollection orientations, final String key, final String value) throws StorageException {
        try {
            DBObject forCurrent = new BasicDBObject(ORIENTATION_KEY, key);
            DBObject current = orientations.findOne(forCurrent);
            if (null == current)
                return;

            BasicDBList origin = (BasicDBList) current.get(ORIENTATION_VALUES);
            if (null == origin)
                return;

            DBObject forRemoved = new BasicDBObject(ORIENTATION_VALUE, value);
            origin.remove(forRemoved);

            DBObject updated = new BasicDBObject().append("$set", new BasicDBObject().append(ORIENTATION_VALUES, origin));
            ;
            orientations.update(forCurrent, updated);
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }
    }

    private void checkType() {
        boolean hasDirectedEdge = false;
        boolean hasUndirectedEdge = false;
        DBCollection directedEdges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleForward);
        DBCollection undirectedEdges = MongoCollectionManager.getCollection(this.db, this.label.getLabel(), MongoCollectionManager.CollectionType.SimpleUndirected);
        if (null != directedEdges) {
            DBCursor cursor = directedEdges.find();
            if (null != cursor) {
                while (cursor.hasNext()) {
                    hasDirectedEdge = true;
                    break;
                }
            }
        }
        if (null != undirectedEdges) {
            DBCursor cursor = undirectedEdges.find();
            if (null != cursor) {
                while (cursor.hasNext()) {
                    hasUndirectedEdge = true;
                    break;
                }
            }
        }

        if (hasDirectedEdge) {
            if (hasUndirectedEdge)
                this.graphType = GraphType.MIXED;
            else
                this.graphType = GraphType.DIRECTED;
            return;
        } else {
            if (hasUndirectedEdge)
                this.graphType = GraphType.UNDIRECTED;
            return;
        }
    }

    private void initializeType() {
        this.graphType = null;
    }

    private void checkPropertyRule(Map<String, String> properties) throws InvalidParamException {
        if (null == this.rule || null == properties || properties.isEmpty())
            return;
        String reason = this.rule.isAdmitted(properties);
        if (null == reason || reason.isEmpty())
            return;
        throw new InvalidParamException(reason);
    }

    private static boolean isExist(final DBCollection elements, final String elementId) throws StorageException {
        DBObject search = new BasicDBObject().append(ELEMENT_ID_KEY, elementId);
        try {
            DBObject result = elements.findOne(search);
            if (null != result)
                return true;
            else
                return false;
        } catch (com.mongodb.MongoException me) {
            throw new StorageException(me.getMessage());
        }

    }
}
