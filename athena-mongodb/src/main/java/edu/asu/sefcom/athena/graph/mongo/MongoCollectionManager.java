package edu.asu.sefcom.athena.graph.mongo;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import edu.asu.sefcom.athena.mongo.MongoProvider;

/**
 * Created by Jangwon Yie on 11/30/2015.
 * @author Jangwon Yie
 */
public class MongoCollectionManager {

    private static final String delim = "_";

    public static enum CollectionType{
         Node, Edge, SimpleForward, SimpleBackWard, SimpleUndirected, Hyper
    }

    public static DBCollection getCollection(DB db, String label, CollectionType type){
        return db.getCollection(generateCollectionId(label, type.name()));
    }

    private static String generateCollectionId(String label, String type){
        StringBuffer sb = new StringBuffer(label + delim + type);
        return sb.toString();
    }
}
