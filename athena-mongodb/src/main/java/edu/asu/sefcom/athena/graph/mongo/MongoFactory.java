package edu.asu.sefcom.athena.graph.mongo;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import edu.asu.sefcom.athena.InitializeException;
import edu.asu.sefcom.athena.graph.Graph;
import edu.asu.sefcom.athena.graph.GraphFactory;
import edu.asu.sefcom.athena.graph.element.Label;

import java.net.UnknownHostException;
import java.util.Properties;

/**
 * Created by Jangwon Yie on 2015. 12. 3..
 *
 * @author Jangwon Yie
 */
public class MongoFactory implements GraphFactory {

    private DB db;

    private static final String urlKey = "mongo.url";
    private static final String portKey = "mongo.port";
    private static final String dbKey = "mongo.db";

    private static final String defaultDBName = "Athena_Mongo";
    private static final String defaultUrl = "127.0.0.1";
    private static final int defaultPort = 27017;

    public MongoFactory(Properties properties) throws UnknownHostException {
        ServerAddress address = extractAddress(properties);
        String dbName = extractDBName(properties);
        this.db = generateDB(address, dbName);
    }

    @Override
    public Graph createGraph(Label label) {
        return new MongoGraph(this.db, label);
    }

    private ServerAddress extractAddress(Properties properties) throws UnknownHostException {
        String mongoUrl = properties.getProperty(urlKey);
        String mongoPort = properties.getProperty(portKey);

        String url;
        if (null == mongoUrl || mongoUrl.isEmpty())
            url = defaultUrl;
        else
            url = mongoUrl;

        int port;
        if (null == mongoPort || mongoPort.isEmpty())
            port = defaultPort;
        else {
            try {
                port = Integer.parseInt(mongoPort);
            } catch (NumberFormatException nfe) {
                port = defaultPort;
            }
        }

        try {
            return new ServerAddress(url, port);
        } catch (UnknownHostException e) {
            return new ServerAddress(defaultUrl, defaultPort);
        }
    }

    private String extractDBName(Properties properties) {
        String dbName = properties.getProperty(dbKey);
        if (null == dbName || dbName.isEmpty())
            dbName = defaultDBName;
        return dbName;
    }

    private DB generateDB(ServerAddress address, String dbName) {
        MongoClient client = new MongoClient(address);
        return client.getDB(dbName);
    }
}
