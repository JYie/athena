package edu.asu.sefcom.athena.mongo;

import com.mongodb.DB;

/**
 * Created by Jangwon Yie on 11/30/2015.
 * @author Jangwon Yie
 */
public class MongoProvider {
    private static DB database;

    public static void registerDB(DB db){
        database = db;
    }

    public static DB getDB(){
        return database;
    }
}
