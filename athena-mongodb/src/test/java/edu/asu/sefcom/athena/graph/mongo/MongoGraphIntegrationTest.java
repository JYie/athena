package edu.asu.sefcom.athena.graph.mongo;


import edu.asu.sefcom.athena.graph.BasicGraphTestSpec;
import edu.asu.sefcom.athena.graph.element.Label;
import edu.asu.sefcom.athena.mongo.MongoProvider;
import edu.asu.sefcom.athena.mongo.MongoSetupForTest;
import org.junit.Assert;

import java.net.UnknownHostException;

/**
 * Created by Jangwon Yie on 11/30/2015.
 * @author Jangwon Yie
 */
public class MongoGraphIntegrationTest extends BasicGraphTestSpec {

    @Override
    protected void setUpTarget() {
        try {
            MongoSetupForTest.setup();
            this.graph = new MongoGraph(MongoProvider.getDB(), new Label("test"));
        } catch (UnknownHostException e) {
            Assert.fail();
        }
    }

    @Override
    protected void tearDownTarget() {
        MongoSetupForTest.tearDown();
    }
}
