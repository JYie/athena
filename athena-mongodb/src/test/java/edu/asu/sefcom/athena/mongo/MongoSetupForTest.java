package edu.asu.sefcom.athena.mongo;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import java.net.UnknownHostException;
import java.util.Set;

/**
 * Created by Jangwon Yie on 11/30/2015.
 * @author Jangwon Yie
 */
public class MongoSetupForTest {
    private static String url = "127.0.0.1";
    private static int port = 27017;

    private static DB db;

    public static final String id = "athena_mongo_test";

    public static void setup() throws UnknownHostException {

        ServerAddress address = new ServerAddress(url,port);

        MongoClient client = new MongoClient(address);
        db = client.getDB(id);
        Set<String> colls = db.getCollectionNames();

        if(null != colls && colls.size() > 0)
            db.dropDatabase();

        MongoProvider.registerDB(db);
    }

    public static void tearDown(){
        db.dropDatabase();
    }
}
